<?php
require_once "../controlador/empleado/empleado.controlador.php";
require_once "../modelo/empleados.modelo.php";

class AjaxEmpleado
{
    public $idEmpleado;

    public function ajaxMostrarEmpleado()
    {
        $item = "emple_nrodocumento";
        $valor = $this->idEmpleado;
        $respuesta = ControladorEmpleado::ctrMostrarEmpleado($item, $valor);
        echo json_encode($respuesta);
    }
}

if (isset($_POST["idEmpleado"])) {
    
    $mostrarEmpleado = new AjaxEmpleado();
    $mostrarEmpleado->idEmpleado = $_POST["idEmpleado"];
    $mostrarEmpleado->ajaxMostrarEmpleado();
}
