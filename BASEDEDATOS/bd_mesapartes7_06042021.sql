-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-04-2021 a las 06:13:36
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_mesapartes7`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_EXTERNO` (IN `NRO_DOCUMENTO` VARCHAR(15), IN `ANIO` VARCHAR(5))  BEGIN
SELECT
documento.documento_id,
IFNULL(UPPER(documento.doc_dniremitente),''),
IFNULL(UPPER(documento.doc_nombreremitente),''),
IFNULL(UPPER(documento.doc_apepatremitente),''),
IFNULL(UPPER(documento.doc_apematremitente),''),
IFNULL(UPPER(documento.doc_celularremitente),''),
IFNULL(UPPER(documento.doc_emailremitente),''),
IFNULL(UPPER(documento.doc_direccionremitente),''),
IFNULL(UPPER(documento.doc_representacion),''),
IFNULL(UPPER(documento.doc_ruc),''),
IFNULL(UPPER(documento.doc_empresa),''),
IFNULL(UPPER(documento.tipodocumento_id),''),
IFNULL(UPPER(documento.doc_nrodocumento),''),
IFNULL(UPPER(documento.doc_folio),''),
IFNULL(UPPER(documento.doc_asunto),''),
IFNULL(UPPER(documento.doc_cuerpo),''),
IFNULL(UPPER(documento.doc_archivo),''),
IFNULL(UPPER(tipo_documento.tipodo_descripcion),''),
DATE_FORMAT(documento.doc_fecharegistro,'%Y'),
CONCAT(DATE_FORMAT(documento.doc_fecharegistro,'%d '),ELT(MONTH(documento.doc_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(documento.doc_fecharegistro,' %Y')),
CONCAT(DATE_FORMAT(documento.doc_fecharegistro,'%d '),ELT(MONTH(documento.doc_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(documento.doc_fecharegistro,' del %Y')),
DATE_FORMAT(documento.doc_fecharegistro,'%H : %i'),
documento.area_destino,
IFNULL(area.area_nombre,'') area_nombre
FROM
documento
LEFT JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area ON documento.area_destino = area.area_cod
WHERE documento.documento_id = NRO_DOCUMENTO and DATE_FORMAT(documento.doc_fecharegistro,'%Y') = ANIO;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_SEGUIMIENTO` (IN `ID_DOCUMENTO` VARCHAR(12))  BEGIN
SELECT
movimiento.movimiento_id,
IFNULL(UPPER(movimiento.area_origen_id),'') AS idarea_origen,
IFNULL(UPPER(movimiento.areadestino_id),'') AS idarea_destino,
DATE_FORMAT(movimiento.mov_fecharegistro,'%Y'),
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento.mov_fecharegistro,' %Y')),
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento.mov_fecharegistro,' del %Y')),
DATE_FORMAT(movimiento.mov_fecharegistro,'%H : %i'),
IFNULL(movimiento.mov_descripcion,''),
IFNULL(UPPER(movimiento.mov_estatus),''),
IFNULL(UPPER(origen.area_nombre),'') AS area_origen,
IFNULL(UPPER(destino.area_nombre),'') AS area_destino,
IFNULL(UPPER(usuario.usu_usuario),'') AS usuario,
movimiento.documento_id
FROM
movimiento
LEFT JOIN area AS origen ON movimiento.area_origen_id = origen.area_cod
LEFT JOIN area AS destino ON movimiento.areadestino_id = destino.area_cod
LEFT JOIN usuario ON movimiento.usuario_id = usuario.usu_id

WHERE movimiento.documento_id = ID_DOCUMENTO AND movimiento.tipo !=1 
AND (movimiento.mov_estatus != 'ACEPTADO' OR movimiento.mov_estatus != 'RECHAZADO');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_SEGUIMIENTO_ACCION` (IN `ID_MOVIMIENTO` INT)  BEGIN
SELECT
movimiento_accion.movimientoaccion_id,
movimiento_accion.movimiento_id,
movimiento_accion.moviac_descripcion,
movimiento_accion.moviac_estatus,
DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%Y'),
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' %Y')),
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' del %Y')),

DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%H : %i'),
area.area_nombre
FROM
movimiento_accion
INNER JOIN movimiento ON movimiento_accion.movimiento_id = movimiento.movimiento_id
INNER JOIN area ON movimiento.areadestino_id = area.area_cod
WHERE 
movimiento_accion.moviac_estatus != "PENDIENTE" AND
movimiento_accion.movimiento_id = ID_MOVIMIENTO;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_SEGUIMIENTO_NUEVO` (IN `BUSCAR` VARCHAR(20))  BEGIN
SELECT t.area_nombre,t.descripcion,t.anio,t.fecha_mini,t.fecha_comp,t.hora,t.accion,t.fecha,t.estado FROM (
SELECT
area.area_nombre,
movimiento.mov_descripcion as descripcion,
movimiento.mov_estatus as estado,
DATE_FORMAT(movimiento.mov_fecharegistro,'%Y') anio,
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento.mov_fecharegistro,' %Y')) fecha_mini,
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento.mov_fecharegistro,' del %Y')) fecha_comp,
DATE_FORMAT(movimiento.mov_fecharegistro,'%H : %i') hora,
'DERIVADO' as accion,
movimiento.mov_fecharegistro as fecha
FROM
movimiento
INNER JOIN area ON movimiento.areadestino_id = area.area_cod
where movimiento.documento_id = BUSCAR
union ALL
SELECT
area.area_nombre,
movimiento_accion.moviac_descripcion  as descripcion,
movimiento_accion.moviac_estatus  as estado,
DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%Y') AS anio,
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' %Y')) AS fecha_mini,
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' del %Y')) AS fecha_comp,
DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%H : %i') AS hora,
'ACCION' AS accion,
movimiento_accion.moviac_fecharegistro as fecha
FROM
movimiento_accion
INNER JOIN movimiento ON movimiento_accion.movimiento_id = movimiento.movimiento_id
INNER JOIN area ON movimiento.areadestino_id = area.area_cod
WHERE movimiento.documento_id = BUSCAR
) t 
ORDER BY t.fecha;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARFUNCIONARIO` (IN `BUSCAR` INT)  BEGIN
SELECT * FROM institucion_cargo
WHERE institucion_cargo.idinstitucion_cargo=BUSCAR AND institucion_cargo.estado='ACTIVO';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOAREA` ()  SELECT
area.area_cod,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado
FROM
area WHERE area.area_estado = 'ACTIVO'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOAREA_DERIVAR` (IN `area_id` INT)  BEGIN
SELECT
area.area_cod,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado
FROM
area
WHERE area.area_cod != area_id AND area.area_estado = 'ACTIVO';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOEMPLEADO` ()  SELECT
empleado.empleado_id,
UPPER(CONCAT_WS(' ',empleado.emple_nombre,
empleado.emple_apepat,
empleado.emple_apemat)) empleado
FROM
empleado

WHERE empleado.emple_estatus = 'ACTIVO'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOTIPODOCUMENTO` ()  SELECT
tipo_documento.tipodocumento_id,
upper(tipo_documento.tipodo_descripcion)tipodo_descripcion,
tipo_documento.tipodo_estado
FROM
tipo_documento
WHERE tipo_documento.tipodo_estado = 'ACTIVO'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOTIPODOCUMENTOAFUERA` ()  BEGIN
	SELECT
tipo_documento.tipodocumento_id,
upper(tipo_documento.tipodo_descripcion)tipodo_descripcion,
tipo_documento.tipodo_estado
FROM
tipo_documento
WHERE tipo_documento.tipodo_estado = 'ACTIVO' AND tipodocumento_id in(2,7,9,13,14);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_DASHBOARD_ADMINISTRADOR` ()  SELECT
(SELECT COUNT(*) FROM documento where doc_estatus='PENDIENTE'),
(SELECT COUNT(*) FROM documento where doc_estatus='RECHAZADO'),
(SELECT COUNT(*) FROM documento where doc_estatus='FINALIZADO')
FROM
documento
limit 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_DASHBOARD_ADMINISTRADOR_AREA` (IN `IDAREA` INT)  SELECT
(SELECT COUNT(*) FROM documento where doc_estatus='PENDIENTE'  and area_id=IDAREA),
(SELECT COUNT(*) FROM documento where doc_estatus='RECHAZADO' and area_id=IDAREA),
(SELECT COUNT(*) FROM documento where doc_estatus='FINALIZADO' and area_id=IDAREA)
FROM
documento
limit 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITARCLAVE` (IN `idusuario` INT, IN `clave` VARCHAR(255))  BEGIN
UPDATE usuario SET
usuario.usu_contra = clave
WHERE usuario.usu_id = idusuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITARCUENTA` (IN `usuario` VARCHAR(50), IN `actual` VARCHAR(250), IN `nueva` VARCHAR(250))  BEGIN
UPDATE usuario SET
usuario.usu_contra= nueva
WHERE usuario.usu_usuario = BINARY usuario and usuario.usu_contra = BINARY actual;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITARFOTO` (IN `idtrabajador` INT, IN `archivo` VARCHAR(255))  UPDATE empleado SET
empl_fotoperfil = archivo
WHERE empleado_id = idtrabajador$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITAR_EMPLEADO` (IN `ID` INT, IN `NRODOCUMENTO` VARCHAR(11), IN `NOMBRE` VARCHAR(150), IN `APEPAT` VARCHAR(100), IN `APEMAT` VARCHAR(100), IN `FECHANACIMIENTO` VARCHAR(20), IN `CELULAR` CHAR(9), IN `EMAIL` VARCHAR(250), IN `DIRECCION` VARCHAR(255), IN `ESTADO` VARCHAR(20))  BEGIN
DECLARE idtrabajador INT;
set @idtrabajador:=(select ifnull(empleado.empleado_id,0) FROM empleado where emple_nrodocumento=NRODOCUMENTO);
IF (ID = @idtrabajador) THEN
	UPDATE empleado SET
		emple_nombre = NOMBRE,
		emple_apepat = APEPAT,
		emple_apemat = APEMAT,
		emple_email  = EMAIL,
		emple_fechanacimiento = FECHANACIMIENTO,
		emple_nrodocumento = NRODOCUMENTO,
		emple_movil = CELULAR,
		emple_direccion = DIRECCION,
		emple_estatus = ESTADO
	WHERE empleado_id = ID;
	SELECT 1;
ELSE
set @idtrabajador:=(select COUNT(*) FROM empleado where emple_nrodocumento=NRODOCUMENTO);
	IF @idtrabajador = 0 THEN
		UPDATE empleado SET
			emple_nombre = NOMBRE,
			emple_apepat = APEPAT,
			emple_apemat = APEMAT,
			emple_email  = EMAIL,
			emple_fechanacimiento = FECHANACIMIENTO,
			emple_nrodocumento = NRODOCUMENTO,
			emple_movil = CELULAR,
			emple_direccion = DIRECCION,
			emple_estatus = ESTADO
		WHERE empleado_id = ID;
		SELECT 1;
	ELSE
		SELECT 2;
	END IF;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITAR_USUARIO` (IN `id_usuario` INT, IN `cmb_area` INT)  BEGIN
UPDATE usuario SET
area_id = cmb_area
WHERE usu_id = id_usuario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTARAREA_USUARIO` (IN `id` INT)  SELECT
area.area_cod,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado
FROM
area
WHERE area.area_estado = 'ACTIVO' AND area.area_cod != id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_AREA` ()  SELECT
area.area_cod,
area.area_nombre,
DATE(area.area_fecha_registro) as area_fecha_registro,
area.area_estado
FROM
area$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_ADMIN` (IN `id_area` VARCHAR(11), IN `estado` VARCHAR(20))  SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento.doc_estatus,
IFNULL(origen.area_nombre,'EXTERIOR') as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod
WHERE documento.area_origen LIKE id_area AND documento.doc_estatus LIKE estado
GROUP BY documento.documento_id
ORDER BY documento.documento_id desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_PDF` (IN `documento_id` VARCHAR(11))  BEGIN
SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
documento.institucion_funcionario,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento.doc_estatus,
origen.area_nombre as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod
WHERE documento.documento_id=documento_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_PDF_ALL` ()  BEGIN
SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
documento.institucion_funcionario,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento.doc_estatus,
origen.area_nombre as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_SECRE` (IN `id_area` VARCHAR(11), IN `estado` VARCHAR(20))  SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
UPPER(tipo_documento.tipodo_descripcion)tipodo_descripcion,
documento.doc_estatus,
IFNULL(origen.area_nombre,'EXTERIOR') as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.mov_estatus,
TIMESTAMPDIFF(DAY, DATE_FORMAT(movimiento.mov_fecharegistro,'%Y-%m-%d'), CURDATE()) cant_dias,
movimiento.areadestino_id,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod

WHERE movimiento.areadestino_id LIKE id_area AND movimiento.mov_estatus LIKE estado
GROUP BY documento.documento_id
ORDER BY documento.doc_fecharegistro DESC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_SEGUIMIENTO` (IN `ID` VARCHAR(12))  SELECT
DATE_FORMAT(movimiento.mov_fecharegistro,'%d/%m/%Y') AS fecharegistro,
movimiento.mov_descripcion,
movimiento.documento_id,
area.area_nombre,
IFNULL(movimiento.mov_archivo,'') mov_archivo,
movimiento.mov_descripcion_original
FROM
movimiento
LEFT JOIN area ON  movimiento.areadestino_id = area.area_cod
WHERE
movimiento.documento_id = ID
ORDER BY 
movimiento.mov_fecharegistro DESC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_EMPLEADO` ()  SELECT
empleado.empleado_id,
CONCAT_WS(' ',emple_nombre,emple_apepat,emple_apemat) as empleado,
empleado.emple_nombre,
empleado.emple_apepat,
empleado.emple_apemat,
empleado.emple_feccreacion,
empleado.emple_fechanacimiento,
DATE_FORMAT(emple_fechanacimiento,'%d/%m/%Y') AS fnacimiento, 
empleado.emple_nrodocumento,
empleado.emple_movil,
empleado.emple_email,
empleado.emple_estatus,
empleado.emple_direccion
FROM
empleado$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_INSTI_CARGO` ()  BEGIN
SELECT institucion_cargo.idinstitucion_cargo,
		institucion_cargo.nombres,
        institucion_cargo.apellidos,
        institucion_cargo.cargo,
        institucion_cargo.institucion,
        institucion_cargo.titulo,
        institucion_cargo.estado
FROM institucion_cargo;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_TIPODOCUMENTO` ()  SELECT
tipo_documento.tipodocumento_id,
tipo_documento.tipodo_descripcion,
tipo_documento.tipodo_estado
FROM
tipo_documento$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_USUARIO` ()  SELECT
usuario.usu_id,
usuario.usu_usuario,
UPPER(empleado.emple_nombre) emple_nombre,
UPPER(empleado.emple_apepat) emple_apepat,
UPPER(empleado.emple_apemat) emple_apemat,
UPPER(CONCAT_WS(' ',empleado.emple_nombre,empleado.emple_apepat,empleado.emple_apemat)) empleado,
IFNULL(area.area_nombre,'NO DEFINIDO')area_nombre,
UPPER(usuario.usu_rol)usu_rol,
usuario.usu_estatus,
area.area_cod
FROM
usuario
INNER JOIN empleado ON usuario.empleado_id = empleado.empleado_id
LEFT JOIN area ON usuario.area_id = area.area_cod$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_AREA` (IN `IDAREA` INT, IN `NOMBRE` VARCHAR(50), IN `ESTATUS` VARCHAR(10))  BEGIN
DECLARE NOMBREACTUAL VARCHAR(50);
DECLARE CANTIDAD INT;
SET @NOMBREACTUAL:=(SELECT area_nombre FROM area where area_cod=IDAREA);
SET @CANTIDAD:=(SELECT COUNT(*) FROM area where area_nombre=NOMBRE);
IF @NOMBREACTUAL = NOMBRE THEN
	UPDATE area set
	area_estado=ESTATUS
	where area_cod=IDAREA;
	SELECT 1;
ELSE
	if @CANTIDAD = 0 THEN 
		UPDATE area set
		area_nombre=NOMBRE,
		area_estado=ESTATUS
		where area_cod=IDAREA;
		SELECT 1;
	ELSE
		SELECT 2;
	END if;

END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_ESTATUS_USUARIO` (IN `IDUSUARIO` INT, IN `ESTATUS` VARCHAR(10))  update usuario set usu_estatus=ESTATUS
where usu_id=IDUSUARIO$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_INSTI_CARGO` (IN `IDINSTITUCION_CARGO` INT, IN `NOMBRE` VARCHAR(45), IN `APELLIDOS` VARCHAR(45), IN `CARGO` VARCHAR(100), IN `INSTITUCION` VARCHAR(100), IN `TITULO` VARCHAR(45), IN `ESTATUS` VARCHAR(10))  BEGIN
DECLARE INSTI VARCHAR(100);
DECLARE CANTIDAD INT;
SET @INSTI:=(SELECT institucion_cargo.institucion FROM institucion_cargo where institucion_cargo.idinstitucion_cargo=IDINSTITUCION_CARGO);
SET @CANTIDAD:=(SELECT COUNT(*) FROM institucion_cargo where institucion_cargo.institucion=INSTITUCION);
IF @INSTI = INSTITUCION THEN
	UPDATE institucion_cargo set
	institucion_cargo.nombres=NOMBRE,
	institucion_cargo.apellidos=APELLIDOS,
	institucion_cargo.cargo=CARGO,
	institucion_cargo.titulo=TITULO,
	institucion_cargo.estado=ESTATUS
	where institucion_cargo.idinstitucion_cargo=IDINSTITUCION_CARGO;
	SELECT 1;
ELSE
	if @CANTIDAD = 0 THEN 
		UPDATE institucion_cargo set
			institucion_cargo.nombres=NOMBRE,
			institucion_cargo.apellidos=APELLIDOS,
			institucion_cargo.cargo=CARGO,
			institucion_cargo.titulo=TITULO,
			institucion_cargo.institucion=INSTITUCION,
			institucion_cargo.estado=ESTATUS
		where institucion_cargo.idinstitucion_cargo=IDINSTITUCION_CARGO;
		SELECT 1;
	ELSE
		SELECT 2;
	END if;

END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_TIPODOCUMENTO` (IN `IDTIPODOCUMENTO` INT, IN `NOMBRE` VARCHAR(50), IN `ESTATUS` VARCHAR(10))  BEGIN
DECLARE NOMBREACTUAL VARCHAR(50);
DECLARE CANTIDAD INT;
SET @NOMBREACTUAL:=(SELECT tipodo_descripcion FROM tipo_documento where tipodocumento_id=IDTIPODOCUMENTO);
SET @CANTIDAD:=(SELECT COUNT(*) FROM tipo_documento where tipodo_descripcion=NOMBRE);
IF @NOMBREACTUAL = NOMBRE THEN
	UPDATE tipo_documento set
	tipodo_estado=ESTATUS
	where tipodocumento_id=IDTIPODOCUMENTO;
	SELECT 1;
ELSE
	if @CANTIDAD = 0 THEN 
		UPDATE tipo_documento set
		tipodo_descripcion=NOMBRE,
		tipodo_estado=ESTATUS
		where tipodocumento_id=IDTIPODOCUMENTO;
		SELECT 1;
	ELSE
		SELECT 2;
	END if;

END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_OBTENER_AREA_ID` (IN `AREA_ID` INT)  BEGIN
SELECT area.area_cod,
		 area.area_estado
FROM area 
		 WHERE area.area_cod=AREA_ID and
		 		 area.area_estado='ACTIVO';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_OBTENER_NEXPE_POR_TIPODOCUMENTO` (IN `tipodocumento` VARCHAR(11))  BEGIN
 SELECT tipodocumento_id,doc_nrodocumento FROM DOCUMENTO
 WHERE tipodocumento_id=tipodocumento ORDER BY documento_id desc;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_ACEPTAR_RECHAZAR` (IN `txt_idmovimiento` INT, IN `txt_iddocumento` CHAR(15), IN `txt_asunto` VARCHAR(255), IN `txt_tipo` VARCHAR(50))  BEGIN
SET time_zone = '-5:00';
INSERT INTO movimiento_accion (movimiento_id,moviac_descripcion,moviac_estatus,moviac_fecharegistro) 
                        VALUES (txt_idmovimiento,txt_asunto,txt_tipo,NOW());
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_ACEPTAR_RECHAZAR2` (IN `txt_idmovimiento` INT, IN `txt_iddocumento` CHAR(15), IN `txt_asunto` VARCHAR(255), IN `txt_tipo` VARCHAR(50))  BEGIN
IF txt_tipo = 'ACEPTAR' THEN
	UPDATE movimiento SET
		movimiento.mov_estatus = 'ACEPTADO'
	WHERE movimiento.movimiento_id = txt_idmovimiento;
ELSE
	UPDATE movimiento SET
		movimiento.mov_estatus = 'RECHAZADO'
	WHERE movimiento.movimiento_id = txt_idmovimiento;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_AREA` (IN `NOMBRE` VARCHAR(50))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM area where area_nombre=NOMBRE);
IF @CANTIDAD=0 THEN
	INSERT INTO area(area_nombre,area_fecha_registro,area_estado) VALUES(NOMBRE,CURRENT_DATE(),'ACTIVO');
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DERIVAR_FINALIZAR` (IN `txt_iddocumento` CHAR(12), IN `txt_idareaactual` INT, IN `txt_idareadestino` INT, IN `txt_descripcion` VARCHAR(255), IN `txt_estado` VARCHAR(20), IN `txt_idusuario` INT, IN `txt_idmovimiento` INT, IN `txt_archivo` VARCHAR(225))  BEGIN
SET time_zone = '-5:00';
IF txt_estado = 'DERIVADO' THEN
	INSERT INTO  movimiento (documento_id,   area_origen_id,  areadestino_id,mov_fecharegistro,mov_descripcion,mov_estatus,usuario_id,tipo) 
                   VALUES (txt_iddocumento,txt_idareaactual,txt_idareadestino,NOW(),txt_descripcion,'PENDIENTE',txt_idusuario ,'');
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_archivo=txt_archivo,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
ELSE
INSERT INTO movimiento_accion (movimiento_id,moviac_descripcion,moviac_estatus,moviac_fecharegistro) 
                        VALUES (txt_idmovimiento,txt_descripcion,'FINALIZADO',NOW());
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_archivo = txt_archivo,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DERIVAR_FINALIZAR_CON_ARCHIVO` (IN `txt_iddocumento` CHAR(12), IN `txt_idareaactual` INT, IN `txt_idareadestino` INT, IN `txt_descripcion` VARCHAR(255), IN `txt_estado` VARCHAR(20), IN `txt_idusuario` INT, IN `txt_idmovimiento` INT, IN `txt_archivo` VARCHAR(255))  BEGIN
SET time_zone = '-5:00';
IF txt_estado = 'DERIVADO' THEN
	INSERT INTO  movimiento (documento_id,   area_origen_id,  areadestino_id,mov_fecharegistro,mov_descripcion,mov_estatus,usuario_id,tipo) 
                   VALUES (txt_iddocumento,txt_idareaactual,txt_idareadestino,NOW(),txt_descripcion,'PENDIENTE',txt_idusuario ,'');
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_archivo = txt_archivo,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
ELSE
INSERT INTO movimiento_accion (movimiento_id,moviac_descripcion,moviac_estatus,moviac_fecharegistro) 
                        VALUES (txt_idmovimiento,txt_descripcion,'FINALIZADO',NOW());
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DOCUMENTO` (IN `txtdni` CHAR(8), IN `txtnombre` VARCHAR(150), IN `txtapepat` VARCHAR(100), IN `txtapemat` VARCHAR(100), IN `txtcelular` CHAR(9), IN `txtemail` VARCHAR(150), IN `txt_direccion` VARCHAR(255), IN `txt_ruc` CHAR(12), IN `txt_empresa` VARCHAR(255), IN `cmb_tipodocumento` INT, IN `txt_nrodocumentos` VARCHAR(15), IN `txt_folios` INT, IN `txt_asunto` VARCHAR(255), IN `txt_archivo` VARCHAR(255), IN `txt_representacion` VARCHAR(255))  BEGIN
DECLARE nrodocumento INT;
DECLARE cantidad INT;
DECLARE cod CHAR(12);
SET time_zone = '-5:00';
SET @cantidad :=(SELECT count(*) FROM documento );
IF @cantidad >= 1 AND @cantidad <= 8  THEN
SET @cod :=(SELECT CONCAT('D000000',(@cantidad+1)));
ELSEIF @cantidad >=9 AND @cantidad <=98 THEN
SET @cod :=(SELECT CONCAT('D00000',(@cantidad+1)));
ELSEIF @cantidad >=99 AND @cantidad <=998 THEN
SET @cod :=(SELECT CONCAT('D0000',(@cantidad+1)));
ELSEIF @cantidad >=999 AND @cantidad <=9998 THEN
SET @cod :=(SELECT CONCAT('D000',(@cantidad+1)));
ELSEIF @cantidad >=9999 AND @cantidad <=99998 THEN
SET @cod :=(SELECT CONCAT('D00',(@cantidad+1)));
ELSEIF @cantidad >=99999 AND @cantidad <=999998 THEN
SET @cod :=(SELECT CONCAT('D0',(@cantidad+1)));
ELSEIF @cantidad >=999999 THEN
SET @cod :=(SELECT CONCAT('D',(@cantidad+1)));
ELSE
SET @cod :=(SELECT CONCAT('D0000001'));
END IF;
set @nrodocumento:=(select COUNT(*) FROM documento where documento.doc_nrodocumento=txt_nrodocumentos AND documento.tipodocumento_id=cmb_tipodocumento);
IF @nrodocumento = 0 THEN
	INSERT INTO documento (documento_id,doc_dniremitente,doc_nombreremitente,doc_apepatremitente,doc_apematremitente,doc_celularremitente,
												 doc_emailremitente,doc_direccionremitente,doc_representacion,doc_ruc,doc_empresa,
												 tipodocumento_id,doc_nrodocumento,doc_folio,doc_asunto,doc_archivo,area_id,area_destino) 
								 VALUES (@cod,txtdni,txtnombre,txtapepat,txtapemat,txtcelular,
												 txtemail,txt_direccion,txt_representacion,txt_ruc,txt_empresa,
												 cmb_tipodocumento,(@cantidad+1),txt_folios,txt_asunto,txt_archivo,1,1);
	SELECT @cod;
ELSE
	SELECT 2;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DOCUMENTO_INTERNO` (IN `txtdni` CHAR(8), IN `txtnombre` VARCHAR(150), IN `txtapepat` VARCHAR(100), IN `txtapemat` VARCHAR(100), IN `txtcelular` CHAR(9), IN `txtemail` VARCHAR(150), IN `txt_direccion` VARCHAR(255), IN `txt_ruc` CHAR(12), IN `txt_empresa` VARCHAR(255), IN `cmb_tipodocumento` INT, IN `txt_nrodocumentos` CHAR(15), IN `txt_folios` INT, IN `txt_asunto` VARCHAR(255), IN `txt_cuerpo` LONGTEXT, IN `txt_archivo` VARCHAR(255), IN `txt_representacion` VARCHAR(255), IN `cmb_procedenciadocumento` INT, IN `area_destino` INT, IN `institucion_fun` TEXT)  BEGIN
DECLARE nrodocumento INT;
DECLARE cantidad INT;
DECLARE cod CHAR(12);
SET time_zone = '-5:00';
SET @cantidad :=(SELECT count(*) FROM documento );
IF @cantidad >= 1 AND @cantidad <= 8  THEN
SET @cod :=(SELECT CONCAT('D000000',(@cantidad+1)));
ELSEIF @cantidad >=9 AND @cantidad <=98 THEN
SET @cod :=(SELECT CONCAT('D00000',(@cantidad+1)));
ELSEIF @cantidad >=99 AND @cantidad <=998 THEN
SET @cod :=(SELECT CONCAT('D0000',(@cantidad+1)));
ELSEIF @cantidad >=999 AND @cantidad <=9998 THEN
SET @cod :=(SELECT CONCAT('D000',(@cantidad+1)));
ELSEIF @cantidad >=9999 AND @cantidad <=99998 THEN
SET @cod :=(SELECT CONCAT('D00',(@cantidad+1)));
ELSEIF @cantidad >=99999 AND @cantidad <=999998 THEN
SET @cod :=(SELECT CONCAT('D0',(@cantidad+1)));
ELSEIF @cantidad >=999999 THEN
SET @cod :=(SELECT CONCAT('D',(@cantidad+1)));
ELSE
SET @cod :=(SELECT CONCAT('D0000001'));
END IF;
set @nrodocumento:=(select COUNT(*) FROM documento where documento.doc_nrodocumento=txt_nrodocumentos AND documento.tipodocumento_id=cmb_tipodocumento);
IF @nrodocumento = 0 THEN
	INSERT INTO documento (documento_id,doc_dniremitente,doc_nombreremitente,doc_apepatremitente,doc_apematremitente,doc_celularremitente,
												 doc_emailremitente,doc_direccionremitente,doc_representacion,doc_ruc,doc_empresa,
												 tipodocumento_id,doc_nrodocumento,doc_folio,doc_asunto,doc_cuerpo,doc_archivo,area_origen,area_id,area_destino,institucion_funcionario) 
								 VALUES (@cod,txtdni,txtnombre,txtapepat,txtapemat,txtcelular,
												 txtemail,txt_direccion,txt_representacion,txt_ruc,txt_empresa,
												 cmb_tipodocumento,txt_nrodocumentos,txt_folios,txt_asunto,txt_cuerpo,txt_archivo,cmb_procedenciadocumento,area_destino,area_destino,institucion_fun);
	SELECT @cod;
ELSE
	SELECT 2;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_EMPLEADO` (IN `NOMBRE` VARCHAR(150), IN `APEPAT` VARCHAR(100), IN `APEMAT` VARCHAR(100), IN `FECHANACIMIENTO` VARCHAR(20), IN `NRODOCUMENTO` VARCHAR(11), IN `MOVIL` CHAR(9), IN `DIRECCION` VARCHAR(255), IN `EMAIL` VARCHAR(250))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) from  empleado where emple_nrodocumento=NRODOCUMENTO);
IF @CANTIDAD = 0 THEN
	INSERT INTO empleado(emple_nombre,emple_apepat,emple_apemat,emple_feccreacion,emple_fechanacimiento,emple_nrodocumento,emple_movil,emple_email,emple_estatus,emple_direccion)
	VALUES(NOMBRE,APEPAT,APEMAT,CURDATE(),FECHANACIMIENTO,NRODOCUMENTO,MOVIL,EMAIL,'ACTIVO',DIRECCION);
	SELECT 1;
ELSE
	SELECT 2;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_INSTI_CARGO` (IN `NOMBRE` VARCHAR(45), IN `APELLIDOS` VARCHAR(250), IN `CARGO` VARCHAR(100), IN `INSTITUCION` VARCHAR(100), IN `TITULO` VARCHAR(45))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM institucion_cargo where institucion_cargo.institucion=INSTITUCION);
IF @CANTIDAD=0 THEN
	INSERT INTO institucion_cargo(nombres,apellidos,cargo,institucion,titulo,estado) VALUES(NOMBRE,APELLIDOS,CARGO,INSTITUCION,TITULO,'ACTIVO');
	SELECT 1;
ELSE
	SELECT 2;
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_TIPODOCUMENTO` (IN `NOMBRE` VARCHAR(250))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM tipo_documento where tipo_documento.tipodo_descripcion=NOMBRE);
IF @CANTIDAD=0 THEN
	INSERT INTO tipo_documento(tipodo_descripcion,tipodo_estado) VALUES(NOMBRE,'ACTIVO');
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_USUARIO` (IN `usuario` VARCHAR(150), IN `clave` VARCHAR(255), IN `empleado` INT, IN `area` INT, IN `rol` VARCHAR(30))  BEGIN
	DECLARE cant_usuario INT;
	DECLARE cant_usuario2 INT;
	SET time_zone = '-5:00';
	set @cant_usuario:=(select count(*) FROM usuario where usuario.empleado_id=empleado AND usuario.area_id=area);
	IF @cant_usuario = 0 THEN
		set @cant_usuario2:=(select count(*) FROM usuario where usuario.usu_usuario = usuario);
		IF @cant_usuario2 = 0 THEN
			INSERT INTO usuario (usu_usuario,usu_contra,usu_feccreacion,empleado_id,usu_estatus,area_id,usu_rol) 
									 VALUES (usuario,clave,CURDATE(),empleado,'ACTIVO',area,rol);
			SELECT 1;
		ELSE
			SELECT 3;
		END IF;
	ELSE
		SELECT 2;
	END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_VERIFICARUSUARIO` (IN `USUARIO` VARCHAR(150))  SELECT
usuario.usu_id,
usuario.usu_usuario,
usuario.usu_contra,
usuario.usu_feccreacion,
usuario.usu_fecupdate,
usuario.empleado_id,
usuario.usu_observacion,
usuario.usu_estatus,
IFNULL(usuario.area_id,'') area_id,
usuario.usu_rol,
UPPER(empleado.emple_nombre) emple_nombre,
UPPER(empleado.emple_apepat) emple_apepat,
UPPER(empleado.emple_apemat) emple_apemat,
empleado.emple_feccreacion,
DATE_FORMAT(empleado.emple_fechanacimiento,'%d/%m/%Y')emple_fechanacimiento ,
empleado.emple_nrodocumento,
empleado.emple_movil,
empleado.emple_email as emple_email,
empleado.emple_estatus,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado,
UPPER(usuario.usu_usuario) as usuario,
UPPER(empleado.emple_direccion) emple_direccion,
empleado.empl_fotoperfil
FROM
usuario
INNER JOIN empleado ON usuario.empleado_id = empleado.empleado_id
LEFT JOIN area ON usuario.area_id = area.area_cod

WHERE usuario.usu_usuario = BINARY USUARIO$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `area_cod` int(11) NOT NULL COMMENT 'Codigo auto-incrementado del movimiento del area',
  `area_nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'nombre del area',
  `area_fecha_registro` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'fecha del registro del movimiento',
  `area_estado` enum('ACTIVO','INACTIVO') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'estado del area'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Entidad Area';

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`area_cod`, `area_nombre`, `area_fecha_registro`, `area_estado`) VALUES
(1, 'MESA DE PARTES', '2018-11-21 07:54:25', 'ACTIVO'),
(2, 'RECURSOS HUMANOS', '2018-11-21 08:41:19', 'ACTIVO'),
(3, 'ADMINISTRATIVA', '2020-06-26 04:00:00', 'ACTIVO'),
(4, 'SECRETARIA ACADEMICA', '2020-06-29 06:09:12', 'ACTIVO'),
(5, 'CONTABILIDAD', '2020-07-05 04:00:00', 'ACTIVO'),
(6, 'INFORMÁTICA', '2020-07-07 04:00:00', 'ACTIVO'),
(7, 'DECANA REGIONAL', '2020-07-07 04:00:00', 'ACTIVO'),
(8, 'VICEDECANA REGIONAL', '2020-07-07 04:00:00', 'ACTIVO'),
(9, 'SECRETARIA REGIONAL ADMINISTRATIVA', '2020-07-07 04:00:00', 'ACTIVO'),
(10, 'SECRETARIA REGIONAL DE ASUNTOS INTERNOS', '2020-07-07 04:00:00', 'ACTIVO'),
(11, 'SECRETARIA REGIONAL DE ASUNTOS EXTERNOS', '2020-07-07 04:00:00', 'ACTIVO'),
(12, 'TESORERA REGIONAL', '2020-07-07 04:00:00', 'ACTIVO'),
(13, 'PRIMERA VOCAL REGIONAL', '2020-07-07 04:00:00', 'ACTIVO'),
(14, 'SEGUNDO VOCAL REGIONAL', '2020-07-07 04:00:00', 'ACTIVO'),
(15, 'DOCUMENTOS EXERNOS', '2020-07-07 04:00:00', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE `documento` (
  `documento_id` char(12) COLLATE utf8_spanish_ci NOT NULL,
  `doc_dniremitente` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `doc_nombreremitente` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `doc_apepatremitente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `doc_apematremitente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `doc_celularremitente` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `doc_emailremitente` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `doc_direccionremitente` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `doc_representacion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `doc_ruc` char(12) COLLATE utf8_spanish_ci NOT NULL,
  `doc_empresa` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tipodocumento_id` int(11) NOT NULL,
  `doc_nrodocumento` varchar(15) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `doc_folio` int(11) NOT NULL,
  `doc_asunto` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `doc_cuerpo` longtext COLLATE utf8_spanish_ci DEFAULT NULL,
  `doc_archivo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `doc_fecharegistro` datetime DEFAULT current_timestamp(),
  `area_id` int(11) DEFAULT 1,
  `doc_estatus` enum('PENDIENTE','RECHAZADO','FINALIZADO') COLLATE utf8_spanish_ci NOT NULL,
  `area_origen` int(11) NOT NULL DEFAULT 0,
  `area_destino` int(11) DEFAULT NULL,
  `institucion_funcionario` text COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `documento`
--

INSERT INTO `documento` (`documento_id`, `doc_dniremitente`, `doc_nombreremitente`, `doc_apepatremitente`, `doc_apematremitente`, `doc_celularremitente`, `doc_emailremitente`, `doc_direccionremitente`, `doc_representacion`, `doc_ruc`, `doc_empresa`, `tipodocumento_id`, `doc_nrodocumento`, `doc_folio`, `doc_asunto`, `doc_cuerpo`, `doc_archivo`, `doc_fecharegistro`, `area_id`, `doc_estatus`, `area_origen`, `area_destino`, `institucion_funcionario`) VALUES
('D0000001', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', '91702345', 'prueba@gmail.com', 'PEDRO LAOS HURTADO', 'A Nombre Propio', '', '', 14, '1', 3, 'PRUEBA ', '', 'archivo/1.docx', '2020-11-28 12:55:32', 2, 'PENDIENTE', 0, 1, NULL),
('D0000002', '76244566', 'YOSSHI ', 'CONDORI ', 'MENDIETA', '917023458', 'yosshimendieta94@gmail.com', 'PEDRO LAOS', 'A Nombre Propio', '', '', 14, '2', 3, 'PRUEBA', '', 'archivo/2.docx', '2020-11-28 12:56:54', 7, 'PENDIENTE', 0, 1, NULL),
('D0000003', '12345678', 'SOFTNET', 'SOLUTIONS', 'PE', '982255930', 'yosshimendieta94@gmail.com', 'CORAZON DE JESUS MZ B LOT 13', 'A otra Persona Natural', '', '', 14, '3', 4, 'PRUEBA', '', 'archivo/4.docx', '2020-11-28 13:32:21', 2, 'PENDIENTE', 1, 2, NULL),
('D0000004', '76244569', 'YOSSHI', 'CONDORI ', 'MENDIETA', '917023454', 'yosshimendieta94@gmail.com', 'Pedro', 'A Nombre Propio', '', '', 13, '1', 1, 'prueba', '', 'archivo/5.zip', '2020-12-01 22:38:51', 1, 'PENDIENTE', 0, 1, NULL),
('D0000005', '76244563', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'yosshimendieta94@gmail.com', 'PEDRO', 'A Nombre Propio', '', '', 1, '1', 1, 'PRUEBA', '', 'archivo/5.zip', '2020-12-01 22:42:30', 1, 'PENDIENTE', 0, 1, NULL),
('D0000006', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 2, '1', 1, 'PRUEBA', '', 'archivo/5.docx', '2020-12-03 22:16:43', 1, 'PENDIENTE', 0, 1, NULL),
('D0000007', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 7, '1', 1, 'PRUEBA', '', 'archivo/6.docx', '2020-12-03 22:23:04', 1, 'PENDIENTE', 0, 1, NULL),
('D0000008', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 9, '8', 12, 'PRUEBA', '', 'archivo/7.pdf', '2020-12-03 22:26:18', 1, 'PENDIENTE', 0, 1, NULL),
('D0000009', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 7, '2', 1, 'PRUEBA', '', 'archivo/8.docx', '2020-12-03 22:28:20', 1, 'PENDIENTE', 0, 1, NULL),
('D0000010', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 7, '3', 2, 'PRUEBA', '', 'archivo/9.docx', '2020-12-05 20:56:13', 2, 'PENDIENTE', 0, 1, NULL),
('D0000011', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 2, '2', 2, 'PRUEBA', '', 'archivo/10.docx', '2020-12-05 20:57:37', 7, 'PENDIENTE', 0, 1, NULL),
('D0000012', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 7, '4', 2, 'PRUEBA', '', 'archivo/11.docx', '2020-12-05 20:58:33', 1, 'FINALIZADO', 0, 1, NULL),
('D0000013', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 7, '5', 2, 'PRUEBA', '', 'archivo/12.docx', '2020-12-05 21:05:30', 7, 'PENDIENTE', 0, 1, NULL),
('D0000014', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 7, '6', 2, 'PRUEBA', '', 'archivo/13.docx', '2020-12-05 21:23:33', 1, 'FINALIZADO', 0, 1, NULL),
('D0000015', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'prueba de detalles', 'A Nombre Propio', '', '', 2, '3', 3, 'SOLICITO CONSTANCIA LABORAL', '', 'archivo/14.docx', '2020-12-09 23:40:23', 7, 'FINALIZADO', 0, 1, NULL),
('D0000016', '76244566', 'JACK ', 'LAZARO', 'GOMEZ', 'undefined', 'yosshimendieta94@gmail.com', 'PRUEBA', 'A Nombre Propio', '', '', 7, '7', 2, 'PRUEBA DE ARCHIVOS', '', 'archivo/17.pdf', '2020-12-10 00:02:11', 9, 'PENDIENTE', 0, 1, NULL),
('D0000017', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 2, '4', 2, 'asdasd', '', 'archivo/20.docx', '2021-01-06 11:51:33', 7, 'PENDIENTE', 1, 7, NULL),
('D0000018', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 7, '8', 2, 'HOLA', '', 'archivo/23.docx', '2021-01-06 11:58:09', 2, 'PENDIENTE', 7, 2, NULL),
('D0000019', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '9', 2, 'ESTOS ES UNA PRUEBA PARA REALIZAR LA MAQUETACION', '', 'archivo/24.docx', '2021-01-12 21:04:40', 15, 'PENDIENTE', 7, 15, NULL),
('D0000020', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '10', 2, 'ESTOS ES LA SEGUNDA PRUEBA', '', '', '2021-01-12 21:06:40', 15, 'PENDIENTE', 7, 15, NULL),
('D0000021', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '11', 1, '', '', 'archivo/26.docx', '2021-01-23 22:45:53', 1, 'PENDIENTE', 7, 1, NULL),
('D0000022', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '12', 2, '', '', 'archivo/27.pdf', '2021-01-23 23:08:23', 2, 'PENDIENTE', 7, 2, NULL),
('D0000023', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '13', 0, '', '', 'archivo/28.pdf', '2021-01-23 23:13:38', 2, 'PENDIENTE', 7, 2, NULL),
('D0000024', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '14', 2, '', '', 'archivo/29.pdf', '2021-01-23 23:22:36', 1, 'PENDIENTE', 7, 1, NULL),
('D0000025', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '15', 2, '', '', 'archivo/30.pdf', '2021-01-23 23:57:55', 2, 'PENDIENTE', 7, 2, NULL),
('D0000026', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '16', 2, '', '', 'archivo/31.pdf', '2021-01-24 00:21:26', 1, 'PENDIENTE', 7, 1, NULL),
('D0000027', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '17', 2, '', '', 'archivo/32.pdf', '2021-01-24 00:24:03', 2, 'PENDIENTE', 7, 2, NULL),
('D0000028', '76244566', 'Yosshi Salvador', 'Condori', 'Mendieta', 'undefined', 'yosshimendieta94@gmail.com', 'Prueba', 'A Nombre Propio', '', '', 9, '28', 2, 'ESTO ES UNA PRUEBA', '', 'archivo/32.pdf', '2021-01-24 00:52:10', 12, 'PENDIENTE', 0, 1, NULL),
('D0000029', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '18', 2, '', '', 'archivo/33.pdf', '2021-01-24 01:04:01', 3, 'PENDIENTE', 7, 3, NULL),
('D0000030', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '19', 2, '', '', 'archivo/34.pdf', '2021-01-24 01:10:25', 1, 'PENDIENTE', 7, 1, NULL),
('D0000031', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '20', 2, '', '', 'archivo/35.pdf', '2021-01-24 01:13:51', 1, 'PENDIENTE', 7, 1, NULL),
('D0000032', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '21', 2, '', '', 'archivo/36.pdf', '2021-01-24 01:36:43', 1, 'PENDIENTE', 7, 1, NULL),
('D0000033', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '22', 2, '<div style=\"text-align: center;\"><span style=\"font-weight: bold; text-decoration-line: underline; color: rgb(0, 0, 255);\">Esto es una prueba</span></div>', '', 'archivo/37.pdf', '2021-01-24 01:38:15', 1, 'PENDIENTE', 7, 1, NULL),
('D0000034', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '23', 2, '<div style=\"text-align: center;\"><span style=\"font-style: italic; text-decoration-line: underline; background-color: rgb(255, 0, 0);\">ESTOA ES UAN PRUEBA MIAU ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ', '', 'archivo/38.pdf', '2021-01-24 02:33:06', 1, 'PENDIENTE', 7, 1, NULL),
('D0000035', '76244566', 'YOSSHI', 'CONDORI', 'MENDIETA', 'undefined', 'yosshimendieta94@gmail.com', 'PEDRO LAOS HURTADO', 'A Nombre Propio', '', '', 7, '35', 2, 'ESTO ES UNA PRUEBA', '', 'archivo/38.pdf', '2021-01-26 19:53:33', 1, 'PENDIENTE', 0, 1, NULL),
('D0000036', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '36', 2, 'Esto es una prueba', 'Esto es una prueba de texto', 'archivo/39.pdf', '2021-01-26 20:22:00', 3, 'PENDIENTE', 7, 3, NULL),
('D0000037', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '37', 2, 'Esto es uan prueba', '&lt;div style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-weight: bold;&quot;&gt;Esto es una prueba de texto&lt;/span&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-weight: 700; color: rgb(0, 0, 255);&quot;&gt;Prueba&lt;/span&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 255); font-weight: 700;&quot;&gt;Prueba&lt;/span&gt;&lt;span style=&quot;font-weight: 700; color: rgb(0, 0, 255);&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 255); font-weight: 700;&quot;&gt;Prueba&lt;/span&gt;&lt;span style=&quot;color: rgb(0, 0, 255); font-weight: 700;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 255); font-weight: 700;&quot;&gt;Prueba&lt;/span&gt;&lt;span style=&quot;color: rgb(0, 0, 255); font-weight: 700;&quot;&gt', 'archivo/40.pdf', '2021-01-26 20:24:38', 1, 'PENDIENTE', 7, 1, NULL),
('D0000038', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '38', 2, 'Esto es una prueba', '&lt;div style=&quot;text-align: center;&quot;&gt;Esto es una prueba&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;Esto es una prueba&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;Esto es una prueba&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;Esto es una prueba&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center;&quot;&gt;Esto es una prueba&lt;br&gt;&lt;/div&gt;&lt;div style=&quot;text-align: left;&quot;&gt;&lt;span style=&quot;color: rgb(0, 255, 255);&quot;&gt;Esto es una prueba&lt;/span&gt;&lt;br&gt;&lt;/div&gt;', 'archivo/41.pdf', '2021-01-26 20:48:38', 3, 'PENDIENTE', 7, 3, NULL),
('D0000039', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 9, '29', 2, 'Esto es una prueba', '<div style=\"text-align: center;\">Esto es una prueba</div><div style=\"text-align: center;\">Esto es una prueba<br></div><div style=\"text-align: center;\">Esto es una prueba<br></div><div style=\"text-align: center;\">Esto es una prueba</div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\">Esto es una pruebaEsto es una pruebaEsto es una pruebaEsto es una pruebaEsto es una pruebaEsto es una pruebaEsto es una pruebaEsto es una pruebaEsto es una prueba<br></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><ol><ol><ol><ol><li style=\"text-align: left;\">Esto es una prueba</li><li style=\"text-align: left;\">Esto es una prueba</li><li style=\"text-align: left;\">Esto es una prueba</li><li style=\"text-align: left;\">Esto es una prueba</li><li style=\"text-align: left;\">Esto es una prueba</li></ol></ol></ol></ol></div>', 'archivo/42.pdf', '2021-01-26 20:53:24', 3, 'PENDIENTE', 7, 3, NULL),
('D0000040', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '39', 2, 'INVITACION A PARTICIPAR DEL DIPLOMADO INTERNACIONAL FORMACION DE OBSTETRAS HOLISTICAS NUEVOS ENFOQUES BASADOS EN EVIDENCIAS', '<div style=\"text-align: justify;\">Es grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del\nColegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del\nDIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS\nENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y\nasistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia\ncientífica; le cual contara con la participación de Docentes Internacionales de reconocida\ntrayectoria profesional</div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\">El costo por participante es:<br></div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\"><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">Inscripción: S/. 150.00</div><div style=\"text-align: justify;\">Mensualidad por modulo: S/. 200.00 - 10 módulos.</div><div style=\"text-al', 'archivo/44.pdf', '2021-01-26 21:36:18', 3, 'PENDIENTE', 7, 3, NULL),
('D0000041', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '40', 2, 'INVITACION A APRTIR DEL DIPLOMADO INTERNTACIONAL FORMACION DE OBSTETRAS HOLISTICAS NUEVOS ENFOQUES BASADOS EN EVIDENCIAS', '<div style=\"text-align: justify;\">Es grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del\nColegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del\nDIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS\nENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y\nasistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia\ncientífica; le cual contara con la participación de Docentes Internacionales de reconocida\ntrayectoria profesional.</div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\">El costo por participante es:&nbsp;</div><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">Inscripción: S/. 150.00&nbsp;</div><div style=\"text-align: justify;\">Mensualidad por modulo: S/ 200.00 – 10 módulos.&nbsp;</div><div style=\"text-align: justify;\">Certificación: S/ 200.00&nbsp;</div><div style=\"t', 'archivo/45.pdf', '2021-01-26 22:16:56', 3, 'PENDIENTE', 7, 3, NULL),
('D0000042', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '41', 2, 'ESTO ES UNA PRUEBA BASURA', '<div style=\"text-align: justify;\">Es grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del\nColegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del\nDIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS\nENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y\nasistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia\ncientífica; le cual contara con la participación de Docentes Internacionales de reconocida\ntrayectoria profesional.&nbsp;</div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\">El costo por participante es:&nbsp;</div><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">Inscripción: S/. 150.00&nbsp;</div><div style=\"text-align: justify;\">Mensualidad por modulo: S/ 200.00 – 10 módulos.&nbsp;</div><div style=\"text-align: justify;\"', 'archivo/46.pdf', '2021-01-28 22:25:18', 2, 'PENDIENTE', 7, 2, NULL),
('D0000043', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '42', 2, 'ESTO ES UNA PRUEBA', 'Es grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del\nColegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del\nDIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS\nENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y\nasistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia\ncientífica; le cual contara con la participación de Docentes Internacionales de reconocida\ntrayectoria profesional.\nEl costo por participante es:\nInscripción: S/. 150.00\nMensualidad por modulo: S/ 200.00 – 10 módulos.\nCertificación: S/ 200.00\nDe acuerdo a las conversaciones telefónicas y siendo importante la participación de docentes\nque forman a las futuras obstetras se ha visto importante otorgar el 40% de descuento por cada\nmódulo a las siguientes obstetras:\n PAITA HUATA Elsa Lourdes\n PALPA INGA Vilma Eneida\n ZAVALA ANTICONA Yamileth Nathaly\n ROSELL GARAY Yessenia Karina\n ', 'archivo/47.pdf', '2021-01-28 22:32:51', 2, 'PENDIENTE', 7, 2, NULL),
('D0000044', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '43', 2, 'ESTO ESTO UNA PRUEBA', '<div style=\"text-align: justify;\">Es grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del\nColegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del\nDIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS\nENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y\nasistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia\ncientífica; le cual contara con la participación de Docentes Internacionales de reconocida\ntrayectoria profesional.&nbsp;</div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\">El costo por participante es:&nbsp;</div><div style=\"text-align: justify;\"><br></div><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">Inscripción: S/. 150.00</div></blockquote><blockquote style=\"margin: 0 0 0 40px; border: none', 'archivo/48.pdf', '2021-01-28 22:38:35', 2, 'PENDIENTE', 7, 2, NULL),
('D0000045', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '44', 2, 'esto es una prueba', '<div style=\"text-align: justify;\">Es grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del Colegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del DIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS ENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y asistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia científica; le cual contara con la participación de Docentes Internacionales de reconocida trayectoria profesional.&nbsp;</div><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\">El costo por participante es:&nbsp;</div><div style=\"text-align: justify;\"><br></div><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">Inscripción: S/. 150.00</div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">&nbsp;Mensualidad por modulo: S/ 200.00 – 10 módulos.&nbsp;</div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\">Certificación: S/ 200.00</div></blockquote></blockquote><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\">&nbsp;De acuerdo a las conversaciones telefónicas y siendo importante la participación de docentes que forman a las futuras obstetras se ha visto importante otorgar el 40% de descuento por cada módulo a las siguientes obstetras:&nbsp;</div><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: bold;\">&nbsp;PAITA HUATA Elsa Lourdes&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: bold;\">&nbsp;PALPA INGA Vilma Eneida&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: bold;\">&nbsp;ZAVALA ANTICONA Yamileth Nathaly&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: bold;\">&nbsp;ROSELL GARAY Yessenia Karina&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: bold;\">&nbsp;ESPINOZA VELIZ DE CORTEZ Karina Liliana&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: bold;\">&nbsp;URETA HILARIO Cayo Walter&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-weight: bold;\">&nbsp;ROMERO ESPINOZA Jaime José&nbsp;</span></div></blockquote></blockquote><div style=\"text-align: justify;\"><br></div><div style=\"text-align: justify;\">Sin otro particular en la espera de que el documento sea de su amable atención, aprovecho la oportunidad para reiterarle la muestra de mi mayor consideración y estima personal. Atentamente</div>', 'archivo/50.pdf', '2021-01-28 22:49:56', 2, 'PENDIENTE', 7, 2, NULL),
('D0000046', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '45', 2, 'Esto es una prueba', 'Esto es una prueba', 'archivo/62.pdf', '2021-02-20 21:38:47', 15, 'PENDIENTE', 7, 15, '[{&quot;id&quot;:&quot;2&quot;},{&quot;nombre&quot;:&quot;JACK&quot;},{&quot;apellido&quot;:&quot;LAZARO GOMEZ&quot;},{&quot;cargo&quot;:&quot;JEFE ET TI&quot;},{&quot;titulo&quot;:&quot;DIRIS LIMA CENTRO&quot;},{&quot;intitucion&quot;:&quot;INGENIERO&quot;},{&quot;id&quot;:&quot;3&quot;},{&quot;nombre&quot;:&quot;VLAD&quot;},{&quot;apellido&quot;:&quot;CONDORI LIMA&quot;},{&quot;cargo&quot;:&quot;JEFE&quot;},{&quot;titulo&quot;:&quot;DIRIS LIMA NORTE&quot;},{&quot;intitucion&quot;:&quot;INGENIERO&quot;}]'),
('D0000047', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '46', 2, 'Esto es una prueba', 'Esto es una prueba', 'archivo/64.pdf', '2021-02-20 21:52:30', 15, 'PENDIENTE', 7, 15, '[{&quot;id&quot;:&quot;2&quot;},{&quot;nombre&quot;:&quot;JACK&quot;},{&quot;apellido&quot;:&quot;LAZARO GOMEZ&quot;},{&quot;cargo&quot;:&quot;JEFE ET TI&quot;},{&quot;titulo&quot;:&quot;DIRIS LIMA CENTRO&quot;},{&quot;intitucion&quot;:&quot;INGENIERO&quot;},{&quot;id&quot;:&quot;1&quot;},{&quot;nombre&quot;:&quot;YOSSHI&quot;},{&quot;apellido&quot;:&quot;CONDORI &quot;},{&quot;cargo&quot;:&quot;JEFE&quot;},{&quot;titulo&quot;:&quot;DIRIS LIMA SUR&quot;},{&quot;intitucion&quot;:&quot;INGENIERO&quot;},{&quot;id&quot;:&quot;6&quot;},{&quot;nombre&quot;:&quot;SALVADOR&quot;},{&quot;apellido&quot;:&quot;MENDIETA LAZARO&quot;},{&quot;cargo&quot;:&quot;JEFE DE TI&quot;},{&quot;titulo&quot;:&quot;MINISTERIO DE CULTURA&quot;},{&quot;intitucion&quot;:&quot;INGENIERO &quot;}]'),
('D0000048', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 7, '47', 2, 'El prueba ', 'El prueba', 'archivo/65.pdf', '2021-02-20 22:21:26', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"2\"},{\"nombre\":\"JACK\"},{\"apellido\":\"LAZARO GOMEZ\"},{\"cargo\":\"JEFE ET TI\"},{\"titulo\":\"DIRIS LIMA CENTRO\"},{\"intitucion\":\"INGENIERO\"},{\"id\":\"3\"},{\"nombre\":\"VLAD\"},{\"apellido\":\"CONDORI LIMA\"},{\"cargo\":\"JEFE\"},{\"titulo\":\"DIRIS LIMA NORTE\"},{\"intitucion\":\"INGENIERO\"},{\"id\":\"4\"},{\"nombre\":\"FULANO\"},{\"apellido\":\"PRUEBA FULANO\"},{\"cargo\":\"JEFE DE TI\"},{\"titulo\":\"MINSA\"},{\"intitucion\":\"INGENIERO\"}]'),
('D0000049', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 5, '1', 2, 'Esto es una prueba', 'Esto es una prueba', 'archivo/66.pdf', '2021-02-20 22:27:25', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"3\"},{\"nombre\":\"VLAD\"},{\"apellido\":\"CONDORI LIMA\"},{\"cargo\":\"JEFE\"},{\"titulo\":\"DIRIS LIMA NORTE\"},{\"intitucion\":\"INGENIERO\"},{\"id\":\"2\"},{\"nombre\":\"JACK\"},{\"apellido\":\"LAZARO GOMEZ\"},{\"cargo\":\"JEFE ET TI\"},{\"titulo\":\"DIRIS LIMA CENTRO\"},{\"intitucion\":\"INGENIERO\"},{\"id\":\"1\"},{\"nombre\":\"YOSSHI\"},{\"apellido\":\"CONDORI \"},{\"cargo\":\"JEFE\"},{\"titulo\":\"DIRIS LIMA SUR\"},{\"intitucion\":\"INGENIERO\"}]'),
('D0000050', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 2, '5', 2, 'Esto es una prueba', 'Esto es una prueba', 'archivo/67.pdf', '2021-02-22 22:26:22', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"DIRIS LIMA CENTRO\",\"intitucion\":\"INGENIERO\"},{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"DIRIS LIMA SUR\",\"intitucion\":\"INGENIERO\"}]'),
('D0000051', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 2, '6', 2, 'INVITACION A PARTICIPAR DEL DIPLOMADO INTERNACIONAL FORMACION DE OBSTETRAS HOLISTICAS  NUEVOS ENFOQUES BASADOS EN EVIDENCIAS', '<div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">De mi especial consideración:\nEs grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del\nColegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del\nDIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS\nENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y\nasistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia\ncientífica; le cual contara con la participación de Docentes Internacionales de reconocida\ntrayectoria profesional.&nbsp;</span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><span style=\"text-align: justify;\">El costo por participante es:&nbsp;</span><br></span><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Inscripción: S/. 150.00&nbsp;</span></div></div></blockquote><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Mensualidad por modulo: S/ 200.00 – 10 módulos.&nbsp;</span></div></div></blockquote><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Certificación: S/ 200.00&nbsp;</span></div></div></blockquote></blockquote><div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">De acuerdo a las conversaciones telefónicas y siendo importante la participación de docentes\nque forman a las futuras obstetras se ha visto importante otorgar el 40% de descuento por cada\nmódulo a las siguientes obstetras:&nbsp;</span></div></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><ul><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">PAITA HUATA Elsa Lourdes&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">PALPA INGA Vilma Eneida&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ZAVALA ANTICONA Yamileth Nathaly&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ROSELL GARAY Yessenia Karina&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ESPINOZA VELIZ DE CORTEZ Karina Liliana&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">URETA HILARIO Cayo Walter&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ROMERO ESPINOZA Jaime José&nbsp;</span></li></ul></div></blockquote></blockquote><div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Sin otro particular en la espera de que el documento sea de su amable atención, aprovecho la\noportunidad para reiterarle la muestra de mi mayor consideración y estima personal.</span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">&nbsp;Atentamente,</span></div></div>', 'archivo/68.pdf', '2021-02-22 22:45:45', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"DIRIS LIMA NORTE\",\"intitucion\":\"INGENIERO\"},{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"DIRIS LIMA SUR\",\"intitucion\":\"INGENIERO\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"DIRIS LIMA CENTRO\",\"intitucion\":\"INGENIERO\"},{\"id\":\"5\",\"nombre\":\"MENGANO\",\"apellido\":\"MENGANO PRUEBA\",\"cargo\":\"JEFE DE TI\",\"titulo\":\"DGOS\",\"intitucion\":\"INGENIERO\"}]'),
('D0000052', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 2, '7', 2, 'ESTO ES UNA PRUEBA', '<div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">De mi especial consideración: Es grato dirigirme a usted, para saludarlo muy cordialmente a nombre del consejo directivo del Colegio Regional de Obstetras III Lima Callao y a su vez hacer extensiva la invitación del DIPLOMADO INTERNACIONAL FORMACIÓN DE OBSTETRAS HOLISTICAS “NUEVOS ENFOQUES BASADOS EN EVIDENCIAS” con mención en Atención, acompañamiento y asistencia respetuosa por los obstetras con un enfoque de medicina basado en evidencia científica; le cual contara con la participación de Docentes Internacionales de reconocida trayectoria profesional.&nbsp;</span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><span style=\"text-align: justify;\">El costo por participante es:&nbsp;</span><br></span><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Inscripción: S/. 150.00&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Mensualidad por modulo: S/ 200.00 – 10 módulos.&nbsp;</span></div></blockquote><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Certificación: S/ 200.00&nbsp;</span></div></blockquote></blockquote><div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">De acuerdo a las conversaciones telefónicas y siendo importante la participación de docentes que forman a las futuras obstetras se ha visto importante otorgar el 40% de descuento por cada módulo a las siguientes obstetras:&nbsp;</span></div></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0px 0px 0px 40px; border: none; padding: 0px;\"><div style=\"text-align: justify;\"><ul><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">PAITA HUATA Elsa Lourdes&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">PALPA INGA Vilma Eneida&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ZAVALA ANTICONA Yamileth Nathaly&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ROSELL GARAY Yessenia Karina&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ESPINOZA VELIZ DE CORTEZ Karina Liliana&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">URETA HILARIO Cayo Walter&nbsp;</span></li><li><span style=\"font-weight: bold; font-size: large; font-family: tahoma, sans-serif;\">ROMERO ESPINOZA Jaime José&nbsp;</span></li></ul></div></blockquote></blockquote><div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">Sin otro particular en la espera de que el documento sea de su amable atención, aprovecho la oportunidad para reiterarle la muestra de mi mayor consideración y estima personal.</span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\"><br></span></div><div style=\"text-align: justify;\"><span style=\"font-size: large; font-family: tahoma, sans-serif;\">&nbsp;Atentamente,</span></div></div>', 'archivo/69.pdf', '2021-02-22 22:59:45', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"DIRIS LIMA SUR\",\"intitucion\":\"INGENIERO\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"DIRIS LIMA CENTRO\",\"intitucion\":\"INGENIERO\"},{\"id\":\"4\",\"nombre\":\"FULANO\",\"apellido\":\"PRUEBA FULANO\",\"cargo\":\"JEFE DE TI\",\"titulo\":\"MINSA\",\"intitucion\":\"INGENIERO\"},{\"id\":\"5\",\"nombre\":\"MENGANO\",\"apellido\":\"MENGANO PRUEBA\",\"cargo\":\"JEFE DE TI\",\"titulo\":\"DGOS\",\"intitucion\":\"INGENIERO\"}]'),
('D0000053', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 8, '1', 2, 'Esto es una prueba', 'Esto es una prueba', '', '2021-03-07 22:42:55', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"}]'),
('D0000054', '76244569', 'YOSSHI', 'CONDORI', 'MENDIETA', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'PLANETA TIERRA', 'A Nombre Propio', '', '', 5, '2', 2, 'Esto es una prueba', 'Esto es una prueba', '', '2021-03-09 22:26:13', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"},{\"id\":\"4\",\"nombre\":\"FULANO\",\"apellido\":\"PRUEBA FULANO\",\"cargo\":\"JEFE DE TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"MINSA\"}]'),
('D0000055', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '3', 2, 'Esto es una prueba', 'Esto es una prueba', '', '2021-03-09 23:11:59', 15, 'PENDIENTE', 7, 15, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000056', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 2, '8', 2, 'asdasd', 'asdasd', '', '2021-03-09 23:15:58', 1, 'PENDIENTE', 8, 1, ''),
('D0000057', '48065511', 'LADY DANIELA', 'MACHADO ', 'CRUZ', '987405505', 'ldmc.0793@gmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 2, '9', 2, 'Esto es una prueba', 'Esto es una prueba', '', '2021-03-13 00:36:35', 8, 'PENDIENTE', 1, 1, ''),
('D0000058', '76244566', 'Yosshi ', 'Condori ', 'Mendieta', '917023454', 'yosshimendieta94@gmail.com', 'Direccion', 'A Nombre Propio', '', '', 7, '58', 2, 'Asunto', NULL, 'archivo/69.pdf', '2021-03-13 20:51:50', 1, 'PENDIENTE', 0, 1, NULL),
('D0000059', '76244566', 'YOSSHI SALVADOR ', 'CONDORI ', 'MENDIETA', '917023454', 'yosshimendieta94@gmail.com', 'Direccion', 'A Nombre Propio', '', '', 7, '59', 2, 'ASUNTO', NULL, 'archivo/69.pdf', '2021-03-15 22:42:27', 7, 'FINALIZADO', 0, 1, NULL),
('D0000060', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '4', 2, 'ASUNTO', 'ASUNTO', '', '2021-03-15 22:59:17', 7, 'FINALIZADO', 7, 7, ''),
('D0000061', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '5', 2, 'Asunto', 'Cuerpo', '', '2021-03-15 23:28:52', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"},{\"id\":\"4\",\"nombre\":\"FULANO\",\"apellido\":\"PRUEBA FULANO\",\"cargo\":\"JEFE DE TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"MINSA\"}]'),
('D0000062', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '6', 15, 'Asunto', 'Cuerpo', 'archivo/70.pdf', '2021-03-15 23:31:56', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000063', '48065511', 'LADY DANIELA', 'MACHADO ', 'CRUZ', '987405505', 'ldmc.0793@gmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 2, '10', 2, 'Asunto', 'Cuerpo', 'archivo/71.pdf', '2021-03-18 20:27:52', 7, 'FINALIZADO', 1, 7, ''),
('D0000064', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 2, '11', 11, 'Asunto', 'Cuerpo', 'archivo/72.pdf', '2021-03-18 20:31:02', 1, 'PENDIENTE', 7, 1, ''),
('D0000065', '48065511', 'LADY DANIELA', 'MACHADO ', 'CRUZ', '987405505', 'ldmc.0793@gmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 2, '12', 2, 'ASUNTO', '&nbsp;CUERPO', 'archivo/73.pdf', '2021-03-18 20:54:56', 7, 'FINALIZADO', 1, 7, ''),
('D0000066', '76244566', 'YOSSHI SALVADOR', 'CONDORI', 'MENDIETA', '917023454', 'yosshimendieta94@gmail.com', 'Direccion', 'A Nombre Propio', '', '', 7, '66', 2, 'ASUNTO DESDE AFUERA', NULL, 'archivo/73.pdf', '2021-03-18 23:21:49', 3, 'PENDIENTE', 0, 1, NULL),
('D0000067', '76244566', 'YOSSHI SALVADOR', 'CONDORI', 'MENDIETA', '917023454', 'yosshimendieta94@gmail.com', 'Direccion', 'A Nombre Propio', '', '', 7, '67', 2, 'ASUNTO DESDE AFUERA', NULL, 'archivo/73.pdf', '2021-03-18 23:31:30', 7, 'FINALIZADO', 0, 1, NULL),
('D0000068', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '7', 2, 'ASUNTO', 'CUERPO', '', '2021-03-22 21:08:19', 8, 'PENDIENTE', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000069', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '8', 2, 'ASUNTO', 'CUERPO', '', '2021-03-22 21:16:13', 2, 'PENDIENTE', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000070', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '9', 2, 'ASUNTO', 'CUERPO', '', '2021-03-22 21:22:24', 15, 'PENDIENTE', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000071', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '10', 2, 'ASUNTO', 'CUERPO', '', '2021-03-22 21:55:03', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000072', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '11', 2, 'ASUNTO', 'CUERPO', '', '2021-03-22 22:30:40', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000073', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '12', 2, 'Asunto', 'Cuerpo', '', '2021-03-25 22:12:39', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"4\",\"nombre\":\"FULANO\",\"apellido\":\"PRUEBA FULANO\",\"cargo\":\"JEFE DE TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"MINSA\"}]'),
('D0000074', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '13', 2, 'ASUNTO', 'CUERPO', '', '2021-03-26 22:39:11', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000075', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '14', 2, 'ASUNTO', 'CUERPO', '', '2021-03-30 23:03:29', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"4\",\"nombre\":\"FULANO\",\"apellido\":\"PRUEBA FULANO\",\"cargo\":\"JEFE DE TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"MINSA\"}]'),
('D0000076', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '15', 2, 'ASUNTO', 'CUERPO', '', '2021-04-02 01:32:14', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000077', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '16', 2, 'ASUNTO DE PRUEBA PARA FIRMA DIGITAL', 'ESTE ES EL CUERPO DEL DOCUMENTO PARA LA FIRMA DIGITAL DEL NUEVO SISTEMA REVOLUCIONADO DEL COLEGIO DE OSTETRAS', '', '2021-04-02 23:42:35', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000078', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '17', 2, 'Asunto prueba uno', 'Cuarpo prueba 1', '', '2021-04-03 00:44:50', 7, 'FINALIZADO', 7, 1, '');
INSERT INTO `documento` (`documento_id`, `doc_dniremitente`, `doc_nombreremitente`, `doc_apepatremitente`, `doc_apematremitente`, `doc_celularremitente`, `doc_emailremitente`, `doc_direccionremitente`, `doc_representacion`, `doc_ruc`, `doc_empresa`, `tipodocumento_id`, `doc_nrodocumento`, `doc_folio`, `doc_asunto`, `doc_cuerpo`, `doc_archivo`, `doc_fecharegistro`, `area_id`, `doc_estatus`, `area_origen`, `area_destino`, `institucion_funcionario`) VALUES
('D0000079', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '18', 2, 'ASUNTO PRUEBA DOS', 'CUERPO PRUEBA 2', '', '2021-04-03 00:47:41', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000080', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '19', 2, 'ASUNTO', 'CUERPO', '', '2021-04-03 01:58:37', 1, 'PENDIENTE', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000081', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '20', 2, 'ASUNTO ', 'CUERPO&nbsp;', '', '2021-04-03 02:09:41', 1, 'PENDIENTE', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"}]'),
('D0000082', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '21', 2, 'ASUNTO', 'CUERPO PARA FIRMA SE MEJORO EL CONTEO DE LOS REGISTRO QUE ESTAN EN LA CARPETA ARCHIVO', '', '2021-04-03 02:21:40', 1, 'PENDIENTE', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"},{\"id\":\"2\",\"nombre\":\"JACK\",\"apellido\":\"LAZARO GOMEZ\",\"cargo\":\"JEFE ET TI\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA CENTRO\"},{\"id\":\"3\",\"nombre\":\"VLAD\",\"apellido\":\"CONDORI LIMA\",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA NORTE\"}]'),
('D0000083', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '22', 2, 'ASUNTO ', 'CUERPO PRUEBA 0248', '', '2021-04-03 02:48:53', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"}]'),
('D0000084', 'O9315926', 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '999346322', 'mimi_rojas@hotmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '23', 0, 'ASUNTO', 'CUERPO 0251', '', '2021-04-03 02:51:16', 7, 'FINALIZADO', 7, 7, '[{\"id\":\"1\",\"nombre\":\"YOSSHI\",\"apellido\":\"CONDORI \",\"cargo\":\"JEFE\",\"titulo\":\"INGENIERO\",\"intitucion\":\"DIRIS LIMA SUR\"}]'),
('D0000085', '48065511', 'LADY DANIELA', 'MACHADO ', 'CRUZ', '987405505', 'ldmc.0793@gmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 10, '1', 0, 'SE SOLICITA UN PERSONAL PARA EL AREA', 'POR MEDIO DE LA PRESENTE SOLICITO UN PERSONAL DE APOYO PARA NUESTRA AREA DE MESA DE PARTES', '', '2021-04-05 23:30:04', 7, 'PENDIENTE', 1, 7, ''),
('D0000086', '48065511', 'LADY DANIELA', 'MACHADO ', 'CRUZ', '987405505', 'ldmc.0793@gmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 7, '68', 1, 'SOLICITO UN PERSONAL', 'Por medio de la presente solicito un personal de apoyo para el area de mesa de partes.', '', '2021-04-05 23:31:47', 3, 'PENDIENTE', 1, 3, ''),
('D0000087', '48065511', 'LADY DANIELA', 'MACHADO ', 'CRUZ', '987405505', 'ldmc.0793@gmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 2, '13', 2, 'SOLICITO PERSONAL ', 'Por medio de la presente solicito un personal de apoyo para el area de mesa de partes virutal.', '', '2021-04-05 23:33:39', 12, 'PENDIENTE', 1, 9, ''),
('D0000088', '48065511', 'LADY DANIELA', 'MACHADO ', 'CRUZ', '987405505', 'ldmc.0793@gmail.com', 'NO DEFINIDO', 'A Nombre Propio', '', '', 5, '24', 2, 'SOLICITO PERSONAL NUEVO', 'Por medio de la presente solicito un personal para el area de mesa de ayuda', '', '2021-04-05 23:49:44', 9, 'PENDIENTE', 1, 9, '');

--
-- Disparadores `documento`
--
DELIMITER $$
CREATE TRIGGER `TRG_REGISTRAR_MOVIMIENTO_INICIAL` AFTER INSERT ON `documento` FOR EACH ROW BEGIN
IF new.area_origen!= '' AND new.area_origen!= '0' THEN
	INSERT INTO movimiento (documento_id,area_origen_id,areadestino_id,mov_fecharegistro,mov_descripcion,mov_estatus,tipo) VALUES (new.documento_id,new.area_origen,new.area_id,now(),'','PENDIENTE','1');
ELSE
	INSERT INTO movimiento (documento_id,area_origen_id,areadestino_id,mov_fecharegistro,mov_descripcion,mov_estatus,tipo) VALUES (new.documento_id,'1',new.area_id,now(),'','PENDIENTE','1');
END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `empleado_id` int(11) NOT NULL,
  `emple_nombre` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emple_apepat` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emple_apemat` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emple_feccreacion` date DEFAULT NULL,
  `emple_fechanacimiento` date DEFAULT NULL,
  `emple_nrodocumento` char(12) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emple_movil` char(9) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emple_email` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `emple_estatus` enum('ACTIVO','INACTIVO') COLLATE utf8_spanish_ci NOT NULL,
  `emple_direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empl_fotoperfil` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Fotos/admin.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`empleado_id`, `emple_nombre`, `emple_apepat`, `emple_apemat`, `emple_feccreacion`, `emple_fechanacimiento`, `emple_nrodocumento`, `emple_movil`, `emple_email`, `emple_estatus`, `emple_direccion`, `empl_fotoperfil`) VALUES
(1, 'SOFTNET', 'SOLUTIONS', 'PE', '2019-09-27', '1996-09-27', '12345678', '982255930', 'softnet.solutions.pe@gmail.com', 'ACTIVO', 'CORAZON DE JESUS MZ B LOT 13', 'Fotos/admin.PNG'),
(2, 'CESAR AUGUSTO', 'GARAY ', 'PEREZ', '0000-00-00', '0000-00-00', '6274590', '939387560', 'tantus2030@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(3, 'EVA', 'HUAMAN', 'ESTEBAN', '0000-00-00', '0000-00-00', '10438233', '979230321', 'evaluhe2014@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(4, 'LADY DANIELA', 'MACHADO ', 'CRUZ', '0000-00-00', '0000-00-00', '48065511', '987405505', 'ldmc.0793@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(5, 'EVA', 'NIETO', 'TABOADA', '0000-00-00', '0000-00-00', '10048580', '994499732', 'evanietotaboada@yahoo.es', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(6, 'FLOR', 'OCHOA', 'ARESTEGUI', '0000-00-00', '0000-00-00', 'O9150771', '999623378', 'flor_ochoa@hotmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(7, 'MARGARITA', 'RODRIGUEZ', 'TAQUIRE', '0000-00-00', '0000-00-00', '72713412', '987029579', 'mert.121990@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(8, 'ANA ROSA', 'RODRIGUEZ', 'VIGO', '0000-00-00', '0000-00-00', 'O6270540', '952656063', 'vicedecanacro3@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(9, 'ELERY', 'RUIZ', 'BOULANGGER', '0000-00-00', '0000-00-00', '41541621', '961710569', 'eleryb@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(10, 'JESSENIA', 'SERNAQUE', 'CHAPA', '0000-00-00', '0000-00-00', '46931353', '933076711', 'jesseniasernaque16@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(11, 'SISSY', 'TABOADA', 'GUARDIAN', '0000-00-00', '0000-00-00', '42450137', '971316132', 'lizettaboada@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(12, 'MARIA ELENA', 'TORO', 'QUINTO', '0000-00-00', '0000-00-00', '29701576', '987318458', 'malena_toroq@hotmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(13, 'JESSICA', 'CASTRO', 'SANTOS', '0000-00-00', '0000-00-00', '41811016', '970531384', 'jessicacastroobst2007@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(14, 'MIRIAN ELIZABETH', ' ROJAS ', 'AGUEDO', '0000-00-00', '0000-00-00', 'O9315926', '999346322', 'mimi_rojas@hotmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(15, 'MARGARITA PATRICIA ', 'VITAL', ' ANTON', '0000-00-00', '0000-00-00', '25595569', '975427556', 'marga_vitalanton@yahoo.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(16, 'MARIBEL ELISA', ' TAFUR ', 'LAZO', '0000-00-00', '0000-00-00', 'O9897171', '956015678', 'mtlazo@hotmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(17, 'HILARIO OSORIO', 'OLIVIA', ' MARIA ', '0000-00-00', '0000-00-00', '40661492', '993001033', 'obstetrahilario@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(18, 'ERIKA GINA', ' AMUNATEGUI', ' LUNA', '0000-00-00', '0000-00-00', 'O9972504', '992996164', 'erikagina@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(19, 'BRISEIDA LISBEL', ' HERRERA ', 'ERAS', '0000-00-00', '0000-00-00', 'OO328420', '932599211', 'lisbel29@hotmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(20, 'VILMA NORMA', ' MAXIMILIANO ', 'COSME', '0000-00-00', '0000-00-00', 'O7673509', '923298177', 'vinoma_04@hotmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(21, 'CARLOS MARTIN', ' PAZ ', 'ROMERO', '0000-00-00', '0000-00-00', 'O9892947', '991161167', 'martinpaz0508@gmail.com', 'ACTIVO', 'NO DEFINIDO', 'Fotos/admin.PNG'),
(22, 'YOSSHI', 'CONDORI', 'MENDIETA', '2020-12-06', '1994-03-29', '76244569', '917023454', 'YOSSHIMENDIETA94@GMAIL.COM', 'ACTIVO', 'PLANETA TIERRA', 'Fotos/admin.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucion_cargo`
--

CREATE TABLE `institucion_cargo` (
  `idinstitucion_cargo` int(11) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `institucion` varchar(100) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `estado` enum('ACTIVO','INACTIVO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `institucion_cargo`
--

INSERT INTO `institucion_cargo` (`idinstitucion_cargo`, `nombres`, `apellidos`, `cargo`, `institucion`, `titulo`, `estado`) VALUES
(1, 'YOSSHI', 'CONDORI ', 'JEFE', 'DIRIS LIMA SUR', 'INGENIERO', 'ACTIVO'),
(2, 'JACK', 'LAZARO GOMEZ', 'JEFE ET TI', 'DIRIS LIMA CENTRO', 'INGENIERO', 'ACTIVO'),
(3, 'VLAD', 'CONDORI LIMA', 'JEFE', 'DIRIS LIMA NORTE', 'INGENIERO', 'ACTIVO'),
(4, 'FULANO', 'PRUEBA FULANO', 'JEFE DE TI', 'MINSA', 'INGENIERO', 'ACTIVO'),
(5, 'MENGANO', 'MENGANO PRUEBA', 'JEFE DE TI', 'DGOS', 'INGENIERO', 'ACTIVO'),
(6, 'SALVADOR', 'MENDIETA LAZARO', 'JEFE DE TI', 'MINISTERIO DE CULTURA', 'INGENIERO ', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimiento`
--

CREATE TABLE `movimiento` (
  `movimiento_id` int(11) NOT NULL,
  `documento_id` char(12) COLLATE utf8_spanish_ci NOT NULL,
  `area_origen_id` int(11) DEFAULT NULL,
  `areadestino_id` int(11) NOT NULL,
  `mov_fecharegistro` datetime DEFAULT current_timestamp(),
  `mov_descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `mov_estatus` enum('PENDIENTE','CONFORME','INCOFORME','ACEPTADO','DERIVADO','FINALIZADO','RECHAZADO') COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mov_archivo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mov_descripcion_original` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `movimiento`
--

INSERT INTO `movimiento` (`movimiento_id`, `documento_id`, `area_origen_id`, `areadestino_id`, `mov_fecharegistro`, `mov_descripcion`, `mov_estatus`, `usuario_id`, `tipo`, `mov_archivo`, `mov_descripcion_original`) VALUES
(1, 'D0000001', 1, 1, '2020-11-28 12:55:32', '', 'DERIVADO', NULL, '1', 'archivo/3.docx', 'ATENCIÓN'),
(2, 'D0000002', 1, 1, '2020-11-28 12:56:54', '', 'DERIVADO', NULL, '1', 'archivo/16.pdf', 'PRUEBA'),
(3, 'D0000001', 1, 2, '2020-11-28 13:25:02', 'ATENCIÓN', 'PENDIENTE', 2, '', NULL, NULL),
(4, 'D0000003', 1, 2, '2020-11-28 13:32:21', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(5, 'D0000004', 1, 1, '2020-12-01 22:38:51', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(6, 'D0000005', 1, 1, '2020-12-01 22:42:30', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(7, 'D0000006', 1, 1, '2020-12-03 22:16:43', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(8, 'D0000007', 1, 1, '2020-12-03 22:23:04', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(9, 'D0000008', 1, 1, '2020-12-03 22:26:18', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(10, 'D0000009', 1, 1, '2020-12-03 22:28:20', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(11, 'D0000010', 1, 1, '2020-12-05 20:56:13', '', 'DERIVADO', NULL, '1', 'archivo/70.pdf', 'Esto es una prueba'),
(12, 'D0000011', 1, 1, '2020-12-05 20:57:37', '', 'DERIVADO', NULL, '1', NULL, 'PRUEBA'),
(13, 'D0000012', 1, 1, '2020-12-05 20:58:33', '', 'FINALIZADO', NULL, '1', NULL, 'NO SE PUDO'),
(14, 'D0000013', 1, 1, '2020-12-05 21:05:30', '', 'DERIVADO', NULL, '1', NULL, 'PARA OBSERVACION'),
(15, 'D0000014', 1, 1, '2020-12-05 21:23:33', '', 'FINALIZADO', NULL, '1', NULL, 'NO CUMPLE'),
(16, 'D0000015', 1, 1, '2020-12-09 23:40:23', '', 'DERIVADO', NULL, '1', 'archivo/15.docx', 'Derivación de prueba'),
(17, 'D0000015', 1, 7, '2020-12-09 23:51:34', 'Derivación de prueba', 'FINALIZADO', 8, '', NULL, 'PRUEBA'),
(18, 'D0000002', 1, 7, '2020-12-09 23:59:30', 'PRUEBA', 'PENDIENTE', 8, '', NULL, NULL),
(19, 'D0000016', 1, 1, '2020-12-10 00:02:11', '', 'DERIVADO', NULL, '1', 'archivo/18.docx', 'Prueba 2'),
(20, 'D0000016', 1, 7, '2020-12-10 00:04:09', 'Prueba 2', 'DERIVADO', 8, '', NULL, 'ATENCION '),
(21, 'D0000016', 7, 9, '2020-12-10 00:05:28', 'ATENCION ', 'ACEPTADO', 6, '', NULL, 'Por Corresponder'),
(22, 'D0000017', 1, 7, '2021-01-06 11:51:33', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(23, 'D0000018', 7, 2, '2021-01-06 11:58:09', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(24, 'D0000019', 7, 15, '2021-01-12 21:04:40', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(25, 'D0000020', 7, 15, '2021-01-12 21:06:40', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(26, 'D0000013', 1, 7, '2021-01-12 22:03:32', 'PARA OBSERVACION', 'PENDIENTE', 8, '', NULL, NULL),
(27, 'D0000011', 1, 7, '2021-01-14 21:54:12', 'PRUEBA', 'PENDIENTE', 8, '', NULL, NULL),
(28, 'D0000021', 7, 1, '2021-01-23 22:45:53', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(29, 'D0000022', 7, 2, '2021-01-23 23:08:23', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(30, 'D0000023', 7, 2, '2021-01-23 23:13:38', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(31, 'D0000024', 7, 1, '2021-01-23 23:22:36', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(32, 'D0000025', 7, 2, '2021-01-23 23:57:55', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(33, 'D0000026', 7, 1, '2021-01-24 00:21:26', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(34, 'D0000027', 7, 2, '2021-01-24 00:24:03', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(35, 'D0000028', 1, 1, '2021-01-24 00:52:10', '', 'DERIVADO', NULL, '1', NULL, 'ESTE DOCUMENTO '),
(36, 'D0000029', 7, 3, '2021-01-24 01:04:01', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(37, 'D0000030', 7, 1, '2021-01-24 01:10:25', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(38, 'D0000031', 7, 1, '2021-01-24 01:13:51', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(39, 'D0000032', 7, 1, '2021-01-24 01:36:43', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(40, 'D0000033', 7, 1, '2021-01-24 01:38:15', '', 'DERIVADO', NULL, '1', 'archivo/70.pdf', 'Esto es una prueba'),
(41, 'D0000034', 7, 1, '2021-01-24 02:33:06', '', 'DERIVADO', NULL, '1', NULL, 'Oficio 23'),
(42, 'D0000035', 1, 1, '2021-01-26 19:53:33', '', 'DERIVADO', NULL, '1', NULL, 'Por corresponder'),
(43, 'D0000036', 7, 3, '2021-01-26 20:22:00', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(44, 'D0000037', 7, 1, '2021-01-26 20:24:38', '', 'DERIVADO', NULL, '1', NULL, 'Por corresponder'),
(45, 'D0000038', 7, 3, '2021-01-26 20:48:38', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(46, 'D0000039', 7, 3, '2021-01-26 20:53:24', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(47, 'D0000040', 7, 3, '2021-01-26 21:36:18', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(48, 'D0000041', 7, 3, '2021-01-26 22:16:56', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(49, 'D0000042', 7, 2, '2021-01-28 22:25:18', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(50, 'D0000043', 7, 2, '2021-01-28 22:32:51', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(51, 'D0000044', 7, 2, '2021-01-28 22:38:35', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(52, 'D0000045', 7, 2, '2021-01-28 22:49:56', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(53, 'D0000037', 1, 7, '2021-02-15 21:18:52', 'Por corresponder', 'DERIVADO', 8, '', 'archivo/73.pdf', 'ASDASD'),
(54, 'D0000035', 1, 7, '2021-02-15 21:19:56', 'Por corresponder', 'DERIVADO', 8, '', 'archivo/74.pdf', 'ACEPTAR'),
(55, 'D0000034', 1, 7, '2021-02-15 21:20:47', 'Oficio 23', 'DERIVADO', 8, '', 'archivo/75.pdf', 'ACEPTAR 34'),
(56, 'D0000046', 7, 15, '2021-02-20 21:38:47', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(57, 'D0000047', 7, 15, '2021-02-20 21:52:30', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(58, 'D0000048', 7, 15, '2021-02-20 22:21:26', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(59, 'D0000049', 7, 15, '2021-02-20 22:27:25', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(60, 'D0000050', 7, 15, '2021-02-22 22:26:22', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(61, 'D0000051', 7, 15, '2021-02-22 22:45:45', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(62, 'D0000052', 7, 15, '2021-02-22 22:59:45', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(63, 'D0000053', 7, 15, '2021-03-07 22:42:55', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(64, 'D0000054', 7, 15, '2021-03-09 22:26:13', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(65, 'D0000055', 7, 15, '2021-03-09 23:11:59', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(66, 'D0000056', 8, 1, '2021-03-09 23:15:58', '', 'DERIVADO', NULL, '1', 'archivo/70.pdf', 'HOLA MUNDO'),
(67, 'D0000057', 1, 1, '2021-03-13 00:36:35', '', 'DERIVADO', NULL, '1', 'archivo/70.pdf', 'HOLA MUNDO'),
(68, 'D0000058', 1, 1, '2021-03-13 20:51:50', '', 'DERIVADO', NULL, '1', 'archivo/70.pdf', 'Esto es una prueba'),
(69, 'D0000059', 1, 1, '2021-03-15 22:42:27', '', 'DERIVADO', NULL, '1', 'archivo/70.pdf', 'HOLA MUNDO'),
(70, 'D0000060', 7, 7, '2021-03-15 22:59:17', '', 'FINALIZADO', NULL, '', NULL, 'EXPEDIENTE 60 PRUEBA'),
(71, 'D0000061', 7, 7, '2021-03-15 23:28:52', '', 'FINALIZADO', NULL, '', NULL, 'HOLA'),
(72, 'D0000062', 7, 7, '2021-03-15 23:31:56', '', 'FINALIZADO', NULL, '', NULL, 'Hola'),
(73, 'D0000028', 1, 12, '2021-03-15 23:42:49', 'ESTE DOCUMENTO ', 'PENDIENTE', 8, '', NULL, NULL),
(74, 'D0000058', 1, 7, '2021-03-16 00:36:55', 'Esto es una prueba', 'DERIVADO', 8, '', 'archivo/73.pdf', 'DERIVANDO NUEVO DE EXPEDEDIENTE 58'),
(75, 'D0000059', 1, 7, '2021-03-16 00:43:00', 'HOLA MUNDO', 'FINALIZADO', 8, '', 'archivo/73.pdf', 'Expediente 59 Finalizado'),
(76, 'D0000057', 1, 8, '2021-03-16 00:43:37', 'HOLA MUNDO', 'PENDIENTE', 8, '', NULL, NULL),
(77, 'D0000056', 1, 7, '2021-03-16 00:45:09', 'HOLA MUNDO', 'DERIVADO', 8, '', 'archivo/73.pdf', 'PRUEBA 79'),
(78, 'D0000010', 1, 2, '2021-03-18 20:23:06', 'Esto es una prueba', 'PENDIENTE', 8, '', NULL, NULL),
(79, 'D0000033', 1, 7, '2021-03-18 20:25:28', 'Esto es una prueba', 'DERIVADO', 8, '', 'archivo/73.pdf', 'PRUEBA '),
(80, 'D0000063', 1, 7, '2021-03-18 20:27:52', '', 'FINALIZADO', NULL, '', NULL, 'FINALIZAR DOCUMENTO'),
(81, 'D0000064', 7, 1, '2021-03-18 20:31:02', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(82, 'D0000065', 1, 7, '2021-03-18 20:54:56', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'APROBADO PERO POR FAVOR NO LA CAGUEN '),
(83, 'D0000065', 7, 1, '2021-03-18 20:57:07', 'APROBADO PERO POR FAVOR NO LA CAGUEN ', 'DERIVADO', 6, '', 'archivo/73.pdf', 'DOCUMENTO FIRMADO '),
(84, 'D0000066', 1, 1, '2021-03-18 23:21:49', '', 'DERIVADO', NULL, '1', 'archivo/73.pdf', 'DESCRIPCION DE DOCUMENTO EXTERNO'),
(85, 'D0000066', 1, 7, '2021-03-18 23:23:03', 'DESCRIPCION DE DOCUMENTO EXTERNO', 'DERIVADO', 8, '', 'archivo/73.pdf', 'DOCUMENTO EXTERNO'),
(86, 'D0000066', 7, 3, '2021-03-18 23:30:41', 'DOCUMENTO EXTERNO', 'PENDIENTE', 6, '', NULL, NULL),
(87, 'D0000067', 1, 1, '2021-03-18 23:31:30', '', 'DERIVADO', NULL, '1', 'archivo/73.pdf', 'DESCRIPCION DESDE EL EXTERIOR'),
(88, 'D0000067', 1, 7, '2021-03-18 23:32:16', 'DESCRIPCION DESDE EL EXTERIOR', 'FINALIZADO', 8, '', NULL, 'FINALIZAR'),
(89, 'D0000068', 7, 7, '2021-03-22 21:08:19', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'EL DOCUMENTO FUE FIRMADO CORRECTAMENTE'),
(90, 'D0000068', 7, 8, '2021-03-22 21:10:48', 'EL DOCUMENTO FUE FIRMADO CORRECTAMENTE', 'PENDIENTE', 6, '', NULL, NULL),
(91, 'D0000069', 7, 7, '2021-03-22 21:16:13', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'ESTO ES UNA PRUEBA'),
(92, 'D0000065', 1, 7, '2021-03-22 21:18:11', 'DOCUMENTO FIRMADO ', 'FINALIZADO', 8, '', 'archivo/73.pdf', 'Expediente 65 Descripcion Prueba'),
(93, 'D0000070', 7, 7, '2021-03-22 21:22:24', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'EL DOCUMENTO FUE FIRMADO CORRECTAMENTE'),
(94, 'D0000070', 7, 15, '2021-03-22 21:25:59', 'EL DOCUMENTO FUE FIRMADO CORRECTAMENTE', 'PENDIENTE', 6, '', NULL, NULL),
(95, 'D0000071', 7, 7, '2021-03-22 21:55:03', '', 'FINALIZADO', NULL, '', NULL, 'EL DOCUMENTO FUE FINALIZADO'),
(96, 'D0000072', 7, 7, '2021-03-22 22:30:40', '', 'FINALIZADO', NULL, '', NULL, 'DOCUMENTO FIRMADO '),
(97, 'D0000069', 7, 2, '2021-03-22 23:05:21', 'ESTO ES UNA PRUEBA', 'PENDIENTE', 6, '', NULL, NULL),
(98, 'D0000033', 7, 1, '2021-03-24 22:48:37', 'PRUEBA ', 'PENDIENTE', 6, '', NULL, NULL),
(99, 'D0000073', 7, 7, '2021-03-25 22:12:39', '', 'FINALIZADO', NULL, '', 'archivo/73.pdf', 'FIRMADO'),
(100, 'D0000074', 7, 7, '2021-03-26 22:39:11', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'CARTA MULTIPLE 76, 73 NUMERO ULTIMO DEL REGISTRO'),
(101, 'D0000075', 7, 7, '2021-03-30 23:03:29', '', 'FINALIZADO', NULL, '', 'archivo/73.pdf', 'DOCUMENTO FIRMADO'),
(102, 'D0000076', 7, 7, '2021-04-02 01:32:14', '', 'FINALIZADO', NULL, '', 'archivo/73.pdf', 'El documento ha sido firmado'),
(103, 'D0000074', 7, 1, '2021-04-02 23:35:30', 'CARTA MULTIPLE 76, 73 NUMERO ULTIMO DEL REGISTRO', 'DERIVADO', 6, '', 'archivo/73.pdf', 'EXPEDIENTE 74'),
(104, 'D0000077', 7, 7, '2021-04-02 23:42:35', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'DERIVACION ADJUNTADO EL EXPEDIENTE NUMERO D0000077'),
(105, 'D0000077', 7, 1, '2021-04-03 00:00:35', 'DERIVACION ADJUNTADO EL EXPEDIENTE NUMERO D0000077', 'DERIVADO', 6, '', 'archivo/73.pdf', 'Expediente 77'),
(106, 'D0000058', 7, 1, '2021-04-03 00:16:10', 'DERIVANDO NUEVO DE EXPEDEDIENTE 58', 'PENDIENTE', 6, '', NULL, NULL),
(107, 'D0000078', 7, 1, '2021-04-03 00:44:50', '', 'DERIVADO', NULL, '1', 'archivo/73.pdf', 'DERIVANDO EXPEDIENTE 79'),
(108, 'D0000079', 7, 7, '2021-04-03 00:47:41', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'DERIVANDO EXPEDIENTE 79'),
(109, 'D0000079', 7, 1, '2021-04-03 00:58:27', 'DERIVANDO EXPEDIENTE 79', 'DERIVADO', 6, '', 'archivo/73.pdf', 'Derivando nuevamente el expediente 79'),
(110, 'D0000079', 1, 7, '2021-04-03 01:01:38', 'Derivando nuevamente el expediente 79', 'FINALIZADO', 8, '', 'archivo/73.pdf', 'FINALIZANDO EL EXPEDIENTE 79'),
(111, 'D0000078', 1, 7, '2021-04-03 01:27:56', 'DERIVANDO EXPEDIENTE 79', 'FINALIZADO', 8, '', 'archivo/73.pdf', 'Expediente 79'),
(112, 'D0000077', 1, 7, '2021-04-03 01:29:39', 'Expediente 77', 'FINALIZADO', 8, '', 'archivo/73.pdf', 'Finalizar Expediente 77'),
(113, 'D0000074', 1, 7, '2021-04-03 01:34:00', 'EXPEDIENTE 74', 'FINALIZADO', 8, '', '', 'EXPEDIENTE SIN FORMATO'),
(114, 'D0000056', 7, 1, '2021-04-03 01:44:54', 'PRUEBA 79', 'PENDIENTE', 6, '', NULL, NULL),
(115, 'D0000080', 7, 7, '2021-04-03 01:58:37', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'DOCUMENTO NUEVO 80'),
(116, 'D0000080', 7, 1, '2021-04-03 02:00:42', 'DOCUMENTO NUEVO 80', 'PENDIENTE', 6, '', NULL, NULL),
(117, 'D0000081', 7, 7, '2021-04-03 02:09:41', '', 'DERIVADO', NULL, '', 'archivo/73.pdf', 'DERIVAR 81'),
(118, 'D0000081', 7, 1, '2021-04-03 02:11:31', 'DERIVAR 81', 'PENDIENTE', 6, '', NULL, NULL),
(119, 'D0000037', 7, 1, '2021-04-03 02:13:07', 'ASDASD', 'PENDIENTE', 6, '', NULL, NULL),
(120, 'D0000035', 7, 1, '2021-04-03 02:18:15', 'ACEPTAR', 'PENDIENTE', 6, '', NULL, NULL),
(121, 'D0000034', 7, 1, '2021-04-03 02:19:35', 'ACEPTAR 34', 'PENDIENTE', 6, '', NULL, NULL),
(122, 'D0000082', 7, 7, '2021-04-03 02:21:40', '', 'DERIVADO', NULL, '', 'archivo/76.pdf', 'DERIVAR DOCUMENTO 82'),
(123, 'D0000082', 7, 1, '2021-04-03 02:22:31', 'DERIVAR DOCUMENTO 82', 'PENDIENTE', 6, '', NULL, NULL),
(124, 'D0000083', 7, 7, '2021-04-03 02:48:53', '', 'FINALIZADO', NULL, '', 'archivo/77.pdf', 'DOCUMENTO FIRMADO'),
(125, 'D0000084', 7, 7, '2021-04-03 02:51:16', '', 'FINALIZADO', NULL, '', 'archivo/78.pdf', 'DOCUMENTO FIRMADO 84 '),
(126, 'D0000085', 1, 7, '2021-04-05 23:30:04', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(127, 'D0000086', 1, 3, '2021-04-05 23:31:47', '', 'PENDIENTE', NULL, '1', NULL, NULL),
(128, 'D0000087', 1, 9, '2021-04-05 23:33:39', '', 'DERIVADO', NULL, '', '', 'SE AUTORIZA EL DOCUMENTO PARA REALIZAR LA ADQUISICION DE UN NUEVO PERSONAL'),
(129, 'D0000087', 9, 12, '2021-04-05 23:41:05', 'SE AUTORIZA EL DOCUMENTO PARA REALIZAR LA ADQUISICION DE UN NUEVO PERSONAL', 'PENDIENTE', 5, '', NULL, NULL),
(130, 'D0000088', 1, 9, '2021-04-05 23:49:44', '', 'PENDIENTE', NULL, '1', NULL, NULL);

--
-- Disparadores `movimiento`
--
DELIMITER $$
CREATE TRIGGER `TRG_ACTUALIZAR_DOCUMENTO` AFTER INSERT ON `movimiento` FOR EACH ROW BEGIN
IF new.tipo!=1 THEN
UPDATE documento SET
documento.area_id = new.areadestino_id
WHERE documento.documento_id =  new.documento_id;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `TRG_ACTUALIZAR_DOCUMENTO_FINALIZAR` AFTER UPDATE ON `movimiento` FOR EACH ROW BEGIN
IF new.mov_estatus = 'FINALIZADO' THEN
    UPDATE documento SET
    	documento.doc_estatus = 'FINALIZADO'
    WHERE documento.documento_id =  new.documento_id;
END IF;
IF new.mov_estatus = 'RECHAZADO' THEN
    UPDATE documento SET
    	documento.doc_estatus = 'RECHAZADO'
    WHERE documento.documento_id =  new.documento_id;
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimiento_accion`
--

CREATE TABLE `movimiento_accion` (
  `movimientoaccion_id` int(11) NOT NULL,
  `movimiento_id` int(11) DEFAULT NULL,
  `moviac_descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT '',
  `moviac_estatus` enum('PENDIENTE','ACEPTADO','CONFORME','INCOFORME','RECHAZADO','FINALIZADO') COLLATE utf8_spanish_ci NOT NULL,
  `moviac_fecharegistro` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `movimiento_accion`
--

INSERT INTO `movimiento_accion` (`movimientoaccion_id`, `movimiento_id`, `moviac_descripcion`, `moviac_estatus`, `moviac_fecharegistro`) VALUES
(1, 17, 'Todo conforme', 'ACEPTADO', '2020-12-09 23:58:00'),
(2, 20, 'Todo conforme', 'ACEPTADO', '2020-12-10 00:04:58'),
(3, 21, 'Por Corresponder', 'ACEPTADO', '2020-12-10 00:09:29'),
(4, 15, 'NO CUMPLE', 'FINALIZADO', '2021-01-12 22:03:04'),
(5, 13, 'NO SE PUDO', 'FINALIZADO', '2021-01-12 22:04:00'),
(6, 72, 'HOlA MUNDO', 'ACEPTADO', '2021-03-16 00:32:47'),
(7, 71, 'HOLA MUNDO', 'ACEPTADO', '2021-03-16 00:34:10'),
(8, 80, 'Aprobado', 'ACEPTADO', '2021-03-18 20:28:55'),
(9, 79, 'Aprobado', 'ACEPTADO', '2021-03-18 20:29:49'),
(10, 82, 'Aprobado ', 'ACEPTADO', '2021-03-18 20:56:37'),
(11, 85, 'Todo conforme', 'ACEPTADO', '2021-03-18 23:26:54'),
(12, 88, 'ACEPTADO DOCUMENTO DEL EXTERIOR', 'ACEPTADO', '2021-03-18 23:32:40'),
(13, 89, 'APROBADO', 'ACEPTADO', '2021-03-22 21:09:06'),
(14, 91, 'ACEPTO EL DOCUMENTO EXTERNO', 'ACEPTADO', '2021-03-22 21:17:20'),
(15, 93, 'ACEPTO EL DOCUMENTO SIN LA FIRMA', 'ACEPTADO', '2021-03-22 21:22:48'),
(16, 95, 'DOCUMENTO APROBADO', 'ACEPTADO', '2021-03-22 21:55:14'),
(17, 95, 'EL DOCUMENTO FUE FINALIZADO', 'FINALIZADO', '2021-03-22 21:56:11'),
(18, 96, 'DOCUMENTO APROBADO', 'ACEPTADO', '2021-03-22 22:30:51'),
(19, 96, 'DOCUMENTO FIRMADO ', 'FINALIZADO', '2021-03-22 22:33:51'),
(20, 88, 'FINALIZAR', 'FINALIZADO', '2021-03-22 23:31:34'),
(21, 80, 'FINALIZAR DOCUMENTO', 'FINALIZADO', '2021-03-22 23:41:06'),
(22, 72, 'Hola', 'FINALIZADO', '2021-03-24 22:36:58'),
(23, 71, 'HOLA', 'FINALIZADO', '2021-03-24 22:42:15'),
(25, 17, 'PRUEBA', 'FINALIZADO', '2021-03-24 23:53:49'),
(26, 70, 'APROBADO PRUEBA', 'ACEPTADO', '2021-03-24 23:55:34'),
(27, 70, 'EXPEDIENTE 60 PRUEBA', 'FINALIZADO', '2021-03-24 23:55:53'),
(28, 92, 'Expediente 65 prueba', 'ACEPTADO', '2021-03-25 00:05:35'),
(29, 92, 'Expediente 65 Descripcion Prueba', 'FINALIZADO', '2021-03-25 00:05:56'),
(30, 75, 'ACEPTA EXPEDIENTE 59 ', 'ACEPTADO', '2021-03-25 00:06:51'),
(31, 75, 'Expediente 59 Finalizado', 'FINALIZADO', '2021-03-25 00:07:29'),
(32, 99, 'Documento Aceptado', 'ACEPTADO', '2021-03-25 22:12:53'),
(33, 99, 'FIRMADO', 'FINALIZADO', '2021-03-25 22:13:40'),
(34, 101, 'ACEPTO EL DOCUMENTO', 'ACEPTADO', '2021-03-30 23:03:46'),
(35, 101, 'DOCUMENTO FIRMADO', 'FINALIZADO', '2021-03-30 23:04:42'),
(36, 102, 'Aprobar', 'ACEPTADO', '2021-04-02 01:33:01'),
(37, 102, 'El documento ha sido firmado', 'FINALIZADO', '2021-04-02 01:33:26'),
(38, 100, 'ACEPTADO DOCUMENTO 74', 'ACEPTADO', '2021-04-02 23:34:50'),
(39, 104, 'EXPEIDNETE ACEPTADO NUMERO 77', 'ACEPTADO', '2021-04-02 23:50:52'),
(40, 74, 'ACEPTA EXPEDIENTE 58', 'ACEPTADO', '2021-04-03 00:15:42'),
(41, 108, 'DOCUMENTO ACEPTADO 79 ', 'ACEPTADO', '2021-04-03 00:49:28'),
(42, 110, 'ACEPTANDO EL EXPEDIENTE 79', 'ACEPTADO', '2021-04-03 01:02:12'),
(43, 110, 'FINALIZANDO EL EXPEDIENTE 79', 'FINALIZADO', '2021-04-03 01:02:32'),
(44, 111, 'ACEPTA DOCUMENTO 78', 'ACEPTADO', '2021-04-03 01:28:12'),
(45, 111, 'Expediente 79', 'FINALIZADO', '2021-04-03 01:28:57'),
(46, 112, 'Aceptar Expediente 77', 'ACEPTADO', '2021-04-03 01:29:55'),
(47, 112, 'Finalizar Expediente 77', 'FINALIZADO', '2021-04-03 01:30:15'),
(48, 113, 'ACEPTAR EXPEIENTE 74', 'ACEPTADO', '2021-04-03 01:35:00'),
(49, 113, 'EXPEDIENTE SIN FORMATO', 'FINALIZADO', '2021-04-03 01:36:37'),
(50, 77, 'ACEPTAR DOCUMENTO 56', 'ACEPTADO', '2021-04-03 01:43:32'),
(51, 115, 'ACEPTAR DOCUMENTO 80', 'ACEPTADO', '2021-04-03 01:59:44'),
(52, 117, 'ACEPTAR A 81', 'ACEPTADO', '2021-04-03 02:10:22'),
(53, 53, 'ACEPTAR 37', 'ACEPTADO', '2021-04-03 02:12:20'),
(54, 54, 'ACEPTAR 35', 'ACEPTADO', '2021-04-03 02:17:24'),
(55, 55, 'ACEPTAR 34', 'ACEPTADO', '2021-04-03 02:19:21'),
(56, 122, 'ACEPTAR 82', 'ACEPTADO', '2021-04-03 02:21:51'),
(57, 124, 'ACEPTAR 83', 'ACEPTADO', '2021-04-03 02:49:12'),
(58, 124, 'DOCUMENTO FIRMADO', 'FINALIZADO', '2021-04-03 02:49:53'),
(59, 125, 'ACEPTAR 84', 'ACEPTADO', '2021-04-03 02:51:28'),
(60, 125, 'DOCUMENTO FIRMADO 84 ', 'FINALIZADO', '2021-04-03 02:52:08'),
(61, 128, 'TODO CORRECTO CON EL DOCUMENTO', 'ACEPTADO', '2021-04-05 23:34:20');

--
-- Disparadores `movimiento_accion`
--
DELIMITER $$
CREATE TRIGGER `TRAG_ACTUALIZAR_MOVIMIENTO` AFTER INSERT ON `movimiento_accion` FOR EACH ROW BEGIN
DECLARE destino_inicial INT;
set @destino_inicial :=(SELECT documento.area_destino FROM documento 
INNER JOIN movimiento ON movimiento.documento_id = documento.documento_id
WHERE movimiento.movimiento_id = new.movimiento_id);
IF @destino_inicial = 1 THEN
	UPDATE movimiento SET
    	movimiento.mov_estatus = new.moviac_estatus,
        movimiento.mov_descripcion_original = new.moviac_descripcion
    WHERE movimiento.movimiento_id = new.movimiento_id;
ELSE
	UPDATE movimiento SET
    	movimiento.mov_estatus = new.moviac_estatus,
        movimiento.mov_descripcion_original = new.moviac_descripcion,
        movimiento.tipo = ''
    WHERE movimiento.movimiento_id = new.movimiento_id;
END IF;
IF new.moviac_estatus = 'RECHAZADO' THEN
    SET @ID :=(SELECT movimiento.documento_id FROM movimiento WHERE movimiento.movimiento_id = new.movimiento_id);
    SET @AREA :=(SELECT movimiento.areadestino_id FROM movimiento WHERE movimiento.movimiento_id = new.movimiento_id);
    SET @USUARIO :=(SELECT movimiento.usuario_id FROM movimiento WHERE movimiento.movimiento_id = new.movimiento_id);
    INSERT INTO movimiento (documento_id,area_origen_id,  areadestino_id,mov_fecharegistro,mov_descripcion,mov_estatus,usuario_id,tipo) 
                   VALUES (@ID,@AREA,1,NOW(),'DOCUMENTO RECHAZADO','PENDIENTE',@USUARIO ,'');
 END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `tipodocumento_id` int(11) NOT NULL COMMENT 'Codigo auto-incrementado del tipo documento',
  `tipodo_descripcion` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Descripcion del  tipo documento',
  `tipodo_estado` enum('ACTIVO','INACTIVO') COLLATE utf8_spanish_ci NOT NULL COMMENT 'estado del tipo de documento'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Entidad Documento';

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`tipodocumento_id`, `tipodo_descripcion`, `tipodo_estado`) VALUES
(1, 'ORDEN DE COORDINACION', 'ACTIVO'),
(2, 'CARTA', 'ACTIVO'),
(3, 'DIRECTIVA', 'ACTIVO'),
(4, 'ANEXO', 'ACTIVO'),
(5, 'CARTA CIRCULAR', 'ACTIVO'),
(7, 'OFICIO', 'ACTIVO'),
(8, 'OFICIO MULTIPLE', 'ACTIVO'),
(9, 'INFORME', 'ACTIVO'),
(10, 'REQUERIMIENTO', 'ACTIVO'),
(11, 'MEMORANDUM', 'ACTIVO'),
(12, 'MEMORADUM MULTIPLE', 'ACTIVO'),
(13, 'INVITACION', 'ACTIVO'),
(14, 'SOLICITUD', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usu_id` int(11) NOT NULL,
  `usu_usuario` varchar(250) COLLATE utf8_spanish_ci DEFAULT '',
  `usu_contra` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_feccreacion` date DEFAULT NULL,
  `usu_fecupdate` date DEFAULT NULL,
  `empleado_id` int(11) DEFAULT NULL,
  `usu_observacion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usu_estatus` enum('ACTIVO','INACTIVO') COLLATE utf8_spanish_ci NOT NULL,
  `area_id` int(11) DEFAULT NULL,
  `usu_rol` enum('Operador (a)','Administrador') COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usu_id`, `usu_usuario`, `usu_contra`, `usu_feccreacion`, `usu_fecupdate`, `empleado_id`, `usu_observacion`, `usu_estatus`, `area_id`, `usu_rol`) VALUES
(1, 'admin00', '$2y$10$9f73EzbIt.emnMGPCLgwjOzli.P/snMNGX7kNXWrBx.jrovJ34Bq6', '2019-08-31', '2019-08-31', 1, '', 'ACTIVO', NULL, 'Administrador'),
(2, 'admin', '$2y$10$9f73EzbIt.emnMGPCLgwjOzli.P/snMNGX7kNXWrBx.jrovJ34Bq6', '2019-08-31', '2019-08-31', 1, '', 'ACTIVO', 1, 'Operador (a)'),
(3, 'rrhh', '$2y$10$9f73EzbIt.emnMGPCLgwjOzli.P/snMNGX7kNXWrBx.jrovJ34Bq6', '2019-08-31', '2019-08-31', 1, '', 'ACTIVO', 2, 'Operador (a)'),
(4, 'ycondori', '$2y$10$B5YnoHjE.gQiVqXMyUzdZOIm2Ys3r5e67Flu9fPnQuhBGe8Xf4aXa', '2020-12-06', NULL, 22, NULL, 'ACTIVO', 7, 'Administrador'),
(5, 'mtafur', '$2y$10$BoxXdgm7NVh/rrqse3a0NePZR3DMbVjIEARXlvyqsBt19GWbi57uW', '2020-12-06', NULL, 16, NULL, 'ACTIVO', 9, 'Operador (a)'),
(6, 'mrojas', '$2y$10$j91TXBvhvYIssIlu.omS5.2GfpprAVHGL4JsBCawk6lFThpzYlJni', '2020-12-09', NULL, 14, NULL, 'ACTIVO', 7, 'Operador (a)'),
(7, 'bherrera', '$2y$10$Lrx7q0f8Iy.2jGmiFAMxKOjx27/iwEhDIqSjx58STf9DO3TZR47f6', '2020-12-09', NULL, 19, NULL, 'ACTIVO', 12, 'Operador (a)'),
(8, 'lmachado', '$2y$10$YdACKMttpHhCEkcLYPyJk.lZAeR4k1aiBl4/dHvg3e7E4WZgJqyku', '2020-12-09', NULL, 4, NULL, 'ACTIVO', 1, 'Operador (a)');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`area_cod`),
  ADD UNIQUE KEY `unico` (`area_nombre`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`documento_id`),
  ADD KEY `tipodocumento_id` (`tipodocumento_id`),
  ADD KEY `fk_documento_insticargo` (`institucion_funcionario`(1024)) USING BTREE;

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`empleado_id`);

--
-- Indices de la tabla `institucion_cargo`
--
ALTER TABLE `institucion_cargo`
  ADD PRIMARY KEY (`idinstitucion_cargo`);

--
-- Indices de la tabla `movimiento`
--
ALTER TABLE `movimiento`
  ADD PRIMARY KEY (`movimiento_id`),
  ADD KEY `area_origen_id` (`area_origen_id`),
  ADD KEY `areadestino_id` (`areadestino_id`),
  ADD KEY `usuario_id` (`usuario_id`),
  ADD KEY `documento_id` (`documento_id`);

--
-- Indices de la tabla `movimiento_accion`
--
ALTER TABLE `movimiento_accion`
  ADD PRIMARY KEY (`movimientoaccion_id`),
  ADD KEY `movimiento_id` (`movimiento_id`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`tipodocumento_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usu_id`),
  ADD KEY `empleado_id` (`empleado_id`),
  ADD KEY `area_id` (`area_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `area_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo auto-incrementado del movimiento del area', AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `empleado_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `institucion_cargo`
--
ALTER TABLE `institucion_cargo`
  MODIFY `idinstitucion_cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `movimiento`
--
ALTER TABLE `movimiento`
  MODIFY `movimiento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `movimiento_accion`
--
ALTER TABLE `movimiento_accion`
  MODIFY `movimientoaccion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `tipodocumento_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo auto-incrementado del tipo documento', AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `documento`
--
ALTER TABLE `documento`
  ADD CONSTRAINT `documento_ibfk_1` FOREIGN KEY (`tipodocumento_id`) REFERENCES `tipo_documento` (`tipodocumento_id`);

--
-- Filtros para la tabla `movimiento`
--
ALTER TABLE `movimiento`
  ADD CONSTRAINT `movimiento_ibfk_1` FOREIGN KEY (`area_origen_id`) REFERENCES `area` (`area_cod`),
  ADD CONSTRAINT `movimiento_ibfk_2` FOREIGN KEY (`areadestino_id`) REFERENCES `area` (`area_cod`),
  ADD CONSTRAINT `movimiento_ibfk_3` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usu_id`),
  ADD CONSTRAINT `movimiento_ibfk_4` FOREIGN KEY (`documento_id`) REFERENCES `documento` (`documento_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `movimiento_accion`
--
ALTER TABLE `movimiento_accion`
  ADD CONSTRAINT `movimiento_accion_ibfk_1` FOREIGN KEY (`movimiento_id`) REFERENCES `movimiento` (`movimiento_id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`empleado_id`),
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_cod`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
