SELECT
documento_temp.documento_id,
documento_temp.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento_temp.doc_nombreremitente,documento_temp.doc_apepatremitente,documento_temp.doc_apematremitente)) AS empleado,
documento_temp.doc_celularremitente,
documento_temp.doc_emailremitente,
documento_temp.doc_direccionremitente,
documento_temp.doc_representacion,
documento_temp.doc_ruc,
documento_temp.doc_empresa,
documento_temp.doc_nrodocumento,
documento_temp.doc_folio,
documento_temp.doc_asunto,
documento_temp.doc_cuerpo,
documento_temp.doc_archivo,
documento_temp.institucion_funcionario,
DATE_FORMAT(documento_temp.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento_temp.area_id,
area.area_nombre,
origen.sigla,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento_temp.doc_estatus,
origen.area_nombre as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento_temp.tipodocumento_id
FROM
documento_temp
LEFT JOIN area ON area.area_cod = documento_temp.area_id
LEFT JOIN tipo_documento ON documento_temp.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento_temp.area_origen
LEFT JOIN movimiento ON  movimiento.documento_id = documento_temp.documento_id
LEFT JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod
WHERE documento_temp.tipodocumento_id ='9' and documento_temp.doc_nrodocumento ='116'
order by documento_temp.doc_fecharegistro desc limit 1;

call PA_LISTAR_DOCUMENTOS_PDF_TEMP("9","116");
/*---------------------------------------------------------------------------------------
	CALCULAR EL NUMERO MAXIMO POR OFICINA, TIPO DOCUMENTO
**/
 SELECT tipodocumento_id,doc_nrodocumento FROM DOCUMENTO
 WHERE tipodocumento_id=tipodocumento ORDER BY documento_id desc;


select documento_id,area_origen,tipodocumento_id,doc_nrodocumento,doc_fecharegistro from documento
where area_origen =7 and tipodocumento_id =2 and extract(year from doc_fecharegistro)=2022

select area_origen,tipodocumento_id,max(cast(doc_nrodocumento AS int)) from documento
	where area_origen =7 
		and tipodocumento_id =7
		and extract(year from doc_fecharegistro)=2022
		order by cast(doc_nrodocumento AS int) desc

call PA_OBTENER_NEXPE_POR_TIPODOCUMENTO(7,7)

select * from documento order by documento_id desc
select * from area;
select * from tipo_documento;
SELECT MAX(cobhabilidad) as cobhabilidad FROM $tabla


call PA_LISTAR_DOCUMENTOS_SEGUIMIENTO('D0000184')

call PA_LISTAR_DOCUMENTO_PRINT_HOJADERUTA('D0000184')
