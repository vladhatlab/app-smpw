-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-03-2022 a las 07:44:01
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_mesapartes7`
--

DELIMITER 
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_EXTERNO` (IN `NRO_DOCUMENTO` VARCHAR(15), IN `ANIO` VARCHAR(5))  BEGIN
SELECT
documento.documento_id,
IFNULL(UPPER(documento.doc_dniremitente),''),
IFNULL(UPPER(documento.doc_nombreremitente),''),
IFNULL(UPPER(documento.doc_apepatremitente),''),
IFNULL(UPPER(documento.doc_apematremitente),''),
IFNULL(UPPER(documento.doc_celularremitente),''),
IFNULL(UPPER(documento.doc_emailremitente),''),
IFNULL(UPPER(documento.doc_direccionremitente),''),
IFNULL(UPPER(documento.doc_representacion),''),
IFNULL(UPPER(documento.doc_ruc),''),
IFNULL(UPPER(documento.doc_empresa),''),
IFNULL(UPPER(documento.tipodocumento_id),''),
IFNULL(UPPER(documento.doc_nrodocumento),''),
IFNULL(UPPER(documento.doc_folio),''),
IFNULL(UPPER(documento.doc_asunto),''),
IFNULL(UPPER(documento.doc_cuerpo),''),
IFNULL(UPPER(documento.doc_archivo),''),
IFNULL(UPPER(tipo_documento.tipodo_descripcion),''),
DATE_FORMAT(documento.doc_fecharegistro,'%Y'),
CONCAT(DATE_FORMAT(documento.doc_fecharegistro,'%d '),ELT(MONTH(documento.doc_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(documento.doc_fecharegistro,' %Y')),
CONCAT(DATE_FORMAT(documento.doc_fecharegistro,'%d '),ELT(MONTH(documento.doc_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(documento.doc_fecharegistro,' del %Y')),
DATE_FORMAT(documento.doc_fecharegistro,'%H : %i'),
documento.area_destino,
IFNULL(area.area_nombre,'') area_nombre,
IFNULL((select area.area_nombre from area where area.area_cod=documento.area_origen ),'') as area_origen
FROM
documento
LEFT JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area ON documento.area_destino = area.area_cod
WHERE documento.documento_id = NRO_DOCUMENTO and DATE_FORMAT(documento.doc_fecharegistro,'%Y') = ANIO;
END


CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_SEGUIMIENTO` (IN `ID_DOCUMENTO` VARCHAR(12))  BEGIN
SELECT
movimiento.movimiento_id,
IFNULL(UPPER(movimiento.area_origen_id),'') AS idarea_origen,
IFNULL(UPPER(movimiento.areadestino_id),'') AS idarea_destino,
DATE_FORMAT(movimiento.mov_fecharegistro,'%Y'),
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento.mov_fecharegistro,' %Y')),
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento.mov_fecharegistro,' del %Y')),
DATE_FORMAT(movimiento.mov_fecharegistro,'%H : %i'),
IFNULL(movimiento.mov_descripcion,''),
IFNULL(UPPER(movimiento.mov_estatus),''),
IFNULL(UPPER(origen.area_nombre),'') AS area_origen,
IFNULL(UPPER(destino.area_nombre),'') AS area_destino,
IFNULL(UPPER(usuario.usu_usuario),'') AS usuario,
movimiento.documento_id
FROM
movimiento
LEFT JOIN area AS origen ON movimiento.area_origen_id = origen.area_cod
LEFT JOIN area AS destino ON movimiento.areadestino_id = destino.area_cod
LEFT JOIN usuario ON movimiento.usuario_id = usuario.usu_id

WHERE movimiento.documento_id = ID_DOCUMENTO AND movimiento.tipo !=1 
AND (movimiento.mov_estatus != 'ACEPTADO' OR movimiento.mov_estatus != 'RECHAZADO');
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_SEGUIMIENTO_ACCION` (IN `ID_MOVIMIENTO` INT)  BEGIN
SELECT
movimiento_accion.movimientoaccion_id,
movimiento_accion.movimiento_id,
movimiento_accion.moviac_descripcion,
movimiento_accion.moviac_estatus,
DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%Y'),
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' %Y')),
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' del %Y')),

DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%H : %i'),
area.area_nombre
FROM
movimiento_accion
INNER JOIN movimiento ON movimiento_accion.movimiento_id = movimiento.movimiento_id
INNER JOIN area ON movimiento.areadestino_id = area.area_cod
WHERE 
movimiento_accion.moviac_estatus != "PENDIENTE" AND
movimiento_accion.movimiento_id = ID_MOVIMIENTO;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARDOCUMENTO_SEGUIMIENTO_NUEVO` (IN `BUSCAR` VARCHAR(20))  BEGIN
SELECT t.area_nombre,t.descripcion,t.anio,t.fecha_mini,t.fecha_comp,t.hora,t.accion,t.fecha,t.estado FROM (
SELECT
area.area_nombre,
movimiento.mov_descripcion as descripcion,
movimiento.mov_estatus as estado,
DATE_FORMAT(movimiento.mov_fecharegistro,'%Y') anio,
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento.mov_fecharegistro,' %Y')) fecha_mini,
CONCAT(DATE_FORMAT(movimiento.mov_fecharegistro,'%d '),ELT(MONTH(movimiento.mov_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento.mov_fecharegistro,' del %Y')) fecha_comp,
DATE_FORMAT(movimiento.mov_fecharegistro,'%H : %i') hora,
'DERIVADO' as accion,
movimiento.mov_fecharegistro as fecha
FROM
movimiento
INNER JOIN area ON movimiento.areadestino_id = area.area_cod
where movimiento.documento_id = BUSCAR
union ALL
SELECT
area.area_nombre,
movimiento_accion.moviac_descripcion  as descripcion,
movimiento_accion.moviac_estatus  as estado,
DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%Y') AS anio,
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' %Y')) AS fecha_mini,
CONCAT(DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%d '),ELT(MONTH(movimiento_accion.moviac_fecharegistro), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"),DATE_FORMAT(movimiento_accion.moviac_fecharegistro,' del %Y')) AS fecha_comp,
DATE_FORMAT(movimiento_accion.moviac_fecharegistro,'%H : %i') AS hora,
'ACCION' AS accion,
movimiento_accion.moviac_fecharegistro as fecha
FROM
movimiento_accion
INNER JOIN movimiento ON movimiento_accion.movimiento_id = movimiento.movimiento_id
INNER JOIN area ON movimiento.areadestino_id = area.area_cod
WHERE movimiento.documento_id = BUSCAR
) t 
ORDER BY t.fecha;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_BUSCARFUNCIONARIO` (IN `BUSCAR` INT)  BEGIN
SELECT * FROM institucion_cargo
WHERE institucion_cargo.idinstitucion_cargo=BUSCAR AND institucion_cargo.estado='ACTIVO';
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOAREA` ()  SELECT
area.area_cod,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado
FROM
area WHERE area.area_estado = 'ACTIVO'

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOAREA_DERIVAR` (IN `area_id` INT)  BEGIN
SELECT
area.area_cod,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado
FROM
area
WHERE area.area_cod != area_id AND area.area_estado = 'ACTIVO';
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOEMPLEADO` ()  SELECT
empleado.empleado_id,
UPPER(CONCAT_WS(' ',empleado.emple_nombre,
empleado.emple_apepat,
empleado.emple_apemat)) empleado
FROM
empleado

WHERE empleado.emple_estatus = 'ACTIVO'

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOTIPODOCUMENTO` ()  SELECT
tipo_documento.tipodocumento_id,
upper(tipo_documento.tipodo_descripcion)tipodo_descripcion,
tipo_documento.tipodo_estado
FROM
tipo_documento
WHERE tipo_documento.tipodo_estado = 'ACTIVO'

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_COMBOTIPODOCUMENTOAFUERA` ()  BEGIN
	SELECT
tipo_documento.tipodocumento_id,
upper(tipo_documento.tipodo_descripcion)tipodo_descripcion,
tipo_documento.tipodo_estado
FROM
tipo_documento
WHERE tipo_documento.tipodo_estado = 'ACTIVO' AND tipodocumento_id in(2,7,9,13,14);
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_DASHBOARD_ADMINISTRADOR` ()  SELECT
(SELECT COUNT(*) FROM documento where doc_estatus='PENDIENTE'),
(SELECT COUNT(*) FROM documento where doc_estatus='RECHAZADO'),
(SELECT COUNT(*) FROM documento where doc_estatus='FINALIZADO')
FROM
documento
limit 1

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_DASHBOARD_ADMINISTRADOR_AREA` (IN `IDAREA` INT)  SELECT
(SELECT COUNT(*) FROM documento where doc_estatus='PENDIENTE'  and area_id=IDAREA),
(SELECT COUNT(*) FROM documento where doc_estatus='RECHAZADO' and area_id=IDAREA),
(SELECT COUNT(*) FROM documento where doc_estatus='FINALIZADO' and area_id=IDAREA)
FROM
documento
limit 1

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITARCLAVE` (IN `idusuario` INT, IN `clave` VARCHAR(255))  BEGIN
UPDATE usuario SET
usuario.usu_contra = clave
WHERE usuario.usu_id = idusuario;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITARCUENTA` (IN `usuario` VARCHAR(50), IN `actual` VARCHAR(250), IN `nueva` VARCHAR(250))  BEGIN
UPDATE usuario SET
usuario.usu_contra= nueva
WHERE usuario.usu_usuario = BINARY usuario and usuario.usu_contra = BINARY actual;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITARFOTO` (IN `idtrabajador` INT, IN `archivo` VARCHAR(255))  UPDATE empleado SET
empl_fotoperfil = archivo
WHERE empleado_id = idtrabajador

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITAR_EMPLEADO` (IN `ID` INT, IN `NRODOCUMENTO` VARCHAR(11), IN `NOMBRE` VARCHAR(150), IN `APEPAT` VARCHAR(100), IN `APEMAT` VARCHAR(100), IN `FECHANACIMIENTO` VARCHAR(20), IN `CELULAR` CHAR(9), IN `EMAIL` VARCHAR(250), IN `DIRECCION` VARCHAR(255), IN `ESTADO` VARCHAR(20))  BEGIN
DECLARE idtrabajador INT;
set @idtrabajador:=(select ifnull(empleado.empleado_id,0) FROM empleado where emple_nrodocumento=NRODOCUMENTO);
IF (ID = @idtrabajador) THEN
	UPDATE empleado SET
		emple_nombre = NOMBRE,
		emple_apepat = APEPAT,
		emple_apemat = APEMAT,
		emple_email  = EMAIL,
		emple_fechanacimiento = FECHANACIMIENTO,
		emple_nrodocumento = NRODOCUMENTO,
		emple_movil = CELULAR,
		emple_direccion = DIRECCION,
		emple_estatus = ESTADO
	WHERE empleado_id = ID;
	SELECT 1;
ELSE
set @idtrabajador:=(select COUNT(*) FROM empleado where emple_nrodocumento=NRODOCUMENTO);
	IF @idtrabajador = 0 THEN
		UPDATE empleado SET
			emple_nombre = NOMBRE,
			emple_apepat = APEPAT,
			emple_apemat = APEMAT,
			emple_email  = EMAIL,
			emple_fechanacimiento = FECHANACIMIENTO,
			emple_nrodocumento = NRODOCUMENTO,
			emple_movil = CELULAR,
			emple_direccion = DIRECCION,
			emple_estatus = ESTADO
		WHERE empleado_id = ID;
		SELECT 1;
	ELSE
		SELECT 2;
	END IF;
END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_EDITAR_USUARIO` (IN `id_usuario` INT, IN `cmb_area` INT)  BEGIN
UPDATE usuario SET
area_id = cmb_area
WHERE usu_id = id_usuario;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTARAREA_USUARIO` (IN `id` INT)  SELECT
area.area_cod,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado
FROM
area
WHERE area.area_estado = 'ACTIVO' AND area.area_cod != id

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_AREA` ()  SELECT
area.area_cod,
area.area_nombre,
DATE(area.area_fecha_registro) as area_fecha_registro,
area.area_estado
FROM
area

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_ADMIN` (IN `id_area` VARCHAR(11), IN `estado` VARCHAR(20))  SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento.doc_estatus,
IFNULL(origen.area_nombre,'EXTERIOR') as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod
WHERE documento.area_origen LIKE id_area AND documento.doc_estatus LIKE estado
GROUP BY documento.documento_id
ORDER BY documento.documento_id desc

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_PDF` (IN `documento_id` VARCHAR(11))  BEGIN
SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
documento.institucion_funcionario,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
origen.sigla,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento.doc_estatus,
origen.area_nombre as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod
WHERE documento.documento_id=documento_id;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_PDF_ALL` ()  BEGIN
SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
documento.institucion_funcionario,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
origen.sigla,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento.doc_estatus,
origen.area_nombre as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_PDF_TEMP` (IN `tipodocumento_id` VARCHAR(11), IN `num_documento` VARCHAR(11))  BEGIN
SELECT
documento_temp.documento_id,
documento_temp.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento_temp.doc_nombreremitente,documento_temp.doc_apepatremitente,documento_temp.doc_apematremitente)) AS empleado,
documento_temp.doc_celularremitente,
documento_temp.doc_emailremitente,
documento_temp.doc_direccionremitente,
documento_temp.doc_representacion,
documento_temp.doc_ruc,
documento_temp.doc_empresa,
documento_temp.doc_nrodocumento,
documento_temp.doc_folio,
documento_temp.doc_asunto,
documento_temp.doc_cuerpo,
documento_temp.doc_archivo,
documento_temp.institucion_funcionario,
DATE_FORMAT(documento_temp.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento_temp.area_id,
area.area_nombre,
origen.sigla,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento_temp.doc_estatus,
origen.area_nombre as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento_temp.tipodocumento_id
FROM
documento_temp
LEFT JOIN area ON area.area_cod = documento_temp.area_id
LEFT JOIN tipo_documento ON documento_temp.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento_temp.area_origen
LEFT JOIN movimiento ON  movimiento.documento_id = documento_temp.documento_id
LEFT JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod
WHERE documento_temp.tipodocumento_id =tipodocumento_id and documento_temp.doc_nrodocumento =num_documento
order by documento_temp.doc_fecharegistro desc limit 1;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_SECRE` (IN `id_area` VARCHAR(11), IN `estado` VARCHAR(20))  SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
UPPER(tipo_documento.tipodo_descripcion)tipodo_descripcion,
documento.doc_estatus,
IFNULL(origen.area_nombre,'EXTERIOR') as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.mov_estatus,
TIMESTAMPDIFF(DAY, DATE_FORMAT(movimiento.mov_fecharegistro,'%Y-%m-%d'), CURDATE()) cant_dias,
movimiento.areadestino_id,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod

WHERE movimiento.areadestino_id LIKE id_area AND movimiento.mov_estatus LIKE estado
GROUP BY documento.documento_id
ORDER BY documento.doc_fecharegistro DESC

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTOS_SEGUIMIENTO` (IN `ID` VARCHAR(12))  SELECT
DATE_FORMAT(movimiento.mov_fecharegistro,'%d/%m/%Y') AS fecharegistro,
movimiento.mov_descripcion,
movimiento.documento_id,
area.area_nombre,
IFNULL(movimiento.mov_archivo,'') mov_archivo,
movimiento.mov_descripcion_original
FROM
movimiento
LEFT JOIN area ON  movimiento.areadestino_id = area.area_cod
WHERE
movimiento.documento_id = ID
ORDER BY 
movimiento.mov_fecharegistro DESC

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_DOCUMENTO_PRINT_HOJADERUTA` (IN `ndocumento` VARCHAR(30))  begin
	SELECT
documento.documento_id,
documento.doc_dniremitente,
UPPER(CONCAT_WS(' ',documento.doc_nombreremitente,documento.doc_apepatremitente,documento.doc_apematremitente)) AS empleado,
documento.doc_celularremitente,
documento.doc_emailremitente,
documento.doc_direccionremitente,
documento.doc_representacion,
documento.doc_ruc,
documento.doc_empresa,
documento.doc_nrodocumento,
documento.doc_folio,
documento.doc_asunto,
documento.doc_cuerpo,
documento.doc_archivo,
DATE_FORMAT(documento.doc_fecharegistro,'%d/%m/%Y') AS doc_fecharegistro,
documento.area_id,
area.area_nombre,
UPPER(tipo_documento.tipodo_descripcion) tipodo_descripcion,
documento.doc_estatus,
IFNULL(origen.area_nombre,'EXTERIOR') as origen_nombre,
origen2.area_nombre as origen_nombre2,
movimiento.movimiento_id,
documento.tipodocumento_id
FROM
documento
INNER JOIN area ON area.area_cod = documento.area_id
INNER JOIN tipo_documento ON documento.tipodocumento_id = tipo_documento.tipodocumento_id
LEFT JOIN area AS origen ON origen.area_cod = documento.area_origen
INNER JOIN movimiento ON  movimiento.documento_id = documento.documento_id
INNER JOIN area AS origen2 ON movimiento.area_origen_id = origen2.area_cod
WHERE documento.documento_id=ndocumento
GROUP BY documento.documento_id
ORDER BY documento.documento_id desc;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_EMPLEADO` ()  SELECT
empleado.empleado_id,
CONCAT_WS(' ',emple_nombre,emple_apepat,emple_apemat) as empleado,
empleado.emple_nombre,
empleado.emple_apepat,
empleado.emple_apemat,
empleado.emple_feccreacion,
empleado.emple_fechanacimiento,
DATE_FORMAT(emple_fechanacimiento,'%d/%m/%Y') AS fnacimiento, 
empleado.emple_nrodocumento,
empleado.emple_movil,
empleado.emple_email,
empleado.emple_estatus,
empleado.emple_direccion
FROM
empleado

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_INSTI_CARGO` ()  BEGIN
SELECT institucion_cargo.idinstitucion_cargo,
		institucion_cargo.nombres,
        institucion_cargo.apellidos,
        institucion_cargo.cargo,
        institucion_cargo.institucion,
        institucion_cargo.titulo,
        institucion_cargo.estado
FROM institucion_cargo;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_TIPODOCUMENTO` ()  SELECT
tipo_documento.tipodocumento_id,
tipo_documento.tipodo_descripcion,
tipo_documento.tipodo_estado
FROM
tipo_documento

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_LISTAR_USUARIO` ()  SELECT
usuario.usu_id,
usuario.usu_usuario,
UPPER(empleado.emple_nombre) emple_nombre,
UPPER(empleado.emple_apepat) emple_apepat,
UPPER(empleado.emple_apemat) emple_apemat,
UPPER(CONCAT_WS(' ',empleado.emple_nombre,empleado.emple_apepat,empleado.emple_apemat)) empleado,
IFNULL(area.area_nombre,'NO DEFINIDO')area_nombre,
UPPER(usuario.usu_rol)usu_rol,
usuario.usu_estatus,
area.area_cod
FROM
usuario
INNER JOIN empleado ON usuario.empleado_id = empleado.empleado_id
LEFT JOIN area ON usuario.area_id = area.area_cod

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_AREA` (IN `IDAREA` INT, IN `NOMBRE` VARCHAR(50), IN `ESTATUS` VARCHAR(10))  BEGIN
DECLARE NOMBREACTUAL VARCHAR(50);
DECLARE CANTIDAD INT;
SET @NOMBREACTUAL:=(SELECT area_nombre FROM area where area_cod=IDAREA);
SET @CANTIDAD:=(SELECT COUNT(*) FROM area where area_nombre=NOMBRE);
IF @NOMBREACTUAL = NOMBRE THEN
	UPDATE area set
	area_estado=ESTATUS
	where area_cod=IDAREA;
	SELECT 1;
ELSE
	if @CANTIDAD = 0 THEN 
		UPDATE area set
		area_nombre=NOMBRE,
		area_estado=ESTATUS
		where area_cod=IDAREA;
		SELECT 1;
	ELSE
		SELECT 2;
	END if;

END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_ESTATUS_USUARIO` (IN `IDUSUARIO` INT, IN `ESTATUS` VARCHAR(10))  update usuario set usu_estatus=ESTATUS
where usu_id=IDUSUARIO

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_INSTI_CARGO` (IN `IDINSTITUCION_CARGO` INT, IN `NOMBRE` VARCHAR(45), IN `APELLIDOS` VARCHAR(45), IN `CARGO` VARCHAR(100), IN `INSTITUCION` VARCHAR(100), IN `TITULO` VARCHAR(45), IN `ESTATUS` VARCHAR(10))  BEGIN
DECLARE INSTI VARCHAR(100);
DECLARE CANTIDAD INT;
SET @INSTI:=(SELECT institucion_cargo.institucion FROM institucion_cargo where institucion_cargo.idinstitucion_cargo=IDINSTITUCION_CARGO);
SET @CANTIDAD:=(SELECT COUNT(*) FROM institucion_cargo where institucion_cargo.institucion=INSTITUCION);
IF @INSTI = INSTITUCION THEN
	UPDATE institucion_cargo set
	institucion_cargo.nombres=NOMBRE,
	institucion_cargo.apellidos=APELLIDOS,
	institucion_cargo.cargo=CARGO,
	institucion_cargo.titulo=TITULO,
	institucion_cargo.estado=ESTATUS
	where institucion_cargo.idinstitucion_cargo=IDINSTITUCION_CARGO;
	SELECT 1;
ELSE
	if @CANTIDAD = 0 THEN 
		UPDATE institucion_cargo set
			institucion_cargo.nombres=NOMBRE,
			institucion_cargo.apellidos=APELLIDOS,
			institucion_cargo.cargo=CARGO,
			institucion_cargo.titulo=TITULO,
			institucion_cargo.institucion=INSTITUCION,
			institucion_cargo.estado=ESTATUS
		where institucion_cargo.idinstitucion_cargo=IDINSTITUCION_CARGO;
		SELECT 1;
	ELSE
		SELECT 2;
	END if;

END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_MODIFICAR_TIPODOCUMENTO` (IN `IDTIPODOCUMENTO` INT, IN `NOMBRE` VARCHAR(50), IN `ESTATUS` VARCHAR(10))  BEGIN
DECLARE NOMBREACTUAL VARCHAR(50);
DECLARE CANTIDAD INT;
SET @NOMBREACTUAL:=(SELECT tipodo_descripcion FROM tipo_documento where tipodocumento_id=IDTIPODOCUMENTO);
SET @CANTIDAD:=(SELECT COUNT(*) FROM tipo_documento where tipodo_descripcion=NOMBRE);
IF @NOMBREACTUAL = NOMBRE THEN
	UPDATE tipo_documento set
	tipodo_estado=ESTATUS
	where tipodocumento_id=IDTIPODOCUMENTO;
	SELECT 1;
ELSE
	if @CANTIDAD = 0 THEN 
		UPDATE tipo_documento set
		tipodo_descripcion=NOMBRE,
		tipodo_estado=ESTATUS
		where tipodocumento_id=IDTIPODOCUMENTO;
		SELECT 1;
	ELSE
		SELECT 2;
	END if;

END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_OBTENER_AREA_ID` (IN `AREA_ID` INT)  BEGIN
SELECT area.area_cod,
		 area.area_estado
FROM area 
		 WHERE area.area_cod=AREA_ID and
		 		 area.area_estado='ACTIVO';
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_OBTENER_EMISOR_RECEPTOR` (IN `idarea` INT, IN `iddocumento` VARCHAR(255))  begin
	select d.area_destino as areadestino,
		   u.area_id as area_id,		   
		   e.empleado_id as idemplado,
		   concat(e.emple_nombre," ",e.emple_apepat," ",e.emple_apemat) as datos_completos 
	from documento d 
	inner join usuario u on u.area_id =d.area_destino
	inner join empleado e on u.empleado_id =e.empleado_id 
    where d.area_id = idarea and d.documento_id = iddocumento and e.empleado_id !=1 and u.usu_rol !="Administrador";
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_OBTENER_EMISOR_RECEPTO_TEMP` (IN `idarea` INT, IN `tipodoc` INT, IN `num_doc` INT)  begin
	select dt.area_destino as areadestino,
	   u.area_id as area_id,		   
	   e.empleado_id as idemplado,
	   concat(e.emple_nombre," ",e.emple_apepat," ",e.emple_apemat) as datos_completos 
	from documento_temp dt 
	inner join usuario u on u.area_id =dt.area_destino
	inner join empleado e on u.empleado_id =e.empleado_id 
	where dt.area_id = idarea and dt.tipodocumento_id = tipodoc and dt.doc_nrodocumento = num_doc and e.empleado_id !=1
	and u.usu_rol!="Administrador"
	order by dt.doc_fecharegistro desc limit 1;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_OBTENER_NEXPE_POR_TIPODOCUMENTO` (IN `area_id` VARCHAR(11), IN `tipodocumento` VARCHAR(11))  BEGIN
 /*SELECT tipodocumento_id,doc_nrodocumento FROM documento
 WHERE tipodocumento_id=tipodocumento ORDER BY documento_id desc;
*/

select tipodocumento_id,max(cast(doc_nrodocumento AS int)),area_origen,extract(year from doc_fecharegistro) from documento
where area_origen =area_id 
and tipodocumento_id =tipodocumento 
and extract(year from doc_fecharegistro)=2022
order by cast(doc_nrodocumento AS int) desc;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_ACEPTAR_RECHAZAR` (IN `txt_idmovimiento` INT, IN `txt_iddocumento` CHAR(15), IN `txt_asunto` VARCHAR(255), IN `txt_tipo` VARCHAR(50))  BEGIN
SET time_zone = '-5:00';
INSERT INTO movimiento_accion (movimiento_id,moviac_descripcion,moviac_estatus,moviac_fecharegistro) 
                        VALUES (txt_idmovimiento,txt_asunto,txt_tipo,NOW());
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_ACEPTAR_RECHAZAR2` (IN `txt_idmovimiento` INT, IN `txt_iddocumento` CHAR(15), IN `txt_asunto` VARCHAR(255), IN `txt_tipo` VARCHAR(50))  BEGIN
IF txt_tipo = 'ACEPTAR' THEN
	UPDATE movimiento SET
		movimiento.mov_estatus = 'ACEPTADO'
	WHERE movimiento.movimiento_id = txt_idmovimiento;
ELSE
	UPDATE movimiento SET
		movimiento.mov_estatus = 'RECHAZADO'
	WHERE movimiento.movimiento_id = txt_idmovimiento;
END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_AREA` (IN `NOMBRE` VARCHAR(50))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM area where area_nombre=NOMBRE);
IF @CANTIDAD=0 THEN
	INSERT INTO area(area_nombre,area_fecha_registro,area_estado) VALUES(NOMBRE,CURRENT_DATE(),'ACTIVO');
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DERIVAR_FINALIZAR` (IN `txt_iddocumento` CHAR(12), IN `txt_idareaactual` INT, IN `txt_idareadestino` INT, IN `txt_descripcion` VARCHAR(255), IN `txt_estado` VARCHAR(20), IN `txt_idusuario` INT, IN `txt_idmovimiento` INT, IN `txt_archivo` VARCHAR(225))  BEGIN
SET time_zone = '-5:00';
IF txt_estado = 'DERIVADO' THEN
	INSERT INTO  movimiento (documento_id,   area_origen_id,  areadestino_id,mov_fecharegistro,mov_descripcion,mov_estatus,usuario_id,tipo) 
                   VALUES (txt_iddocumento,txt_idareaactual,txt_idareadestino,NOW(),txt_descripcion,'PENDIENTE',txt_idusuario ,'');
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_archivo=txt_archivo,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
ELSE
INSERT INTO movimiento_accion (movimiento_id,moviac_descripcion,moviac_estatus,moviac_fecharegistro) 
                        VALUES (txt_idmovimiento,txt_descripcion,'FINALIZADO',NOW());
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_archivo = txt_archivo,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DERIVAR_FINALIZAR_CON_ARCHIVO` (IN `txt_iddocumento` CHAR(12), IN `txt_idareaactual` INT, IN `txt_idareadestino` INT, IN `txt_descripcion` VARCHAR(255), IN `txt_estado` VARCHAR(20), IN `txt_idusuario` INT, IN `txt_idmovimiento` INT, IN `txt_archivo` VARCHAR(255))  BEGIN
SET time_zone = '-5:00';
IF txt_estado = 'DERIVADO' THEN
	INSERT INTO  movimiento (documento_id,   area_origen_id,  areadestino_id,mov_fecharegistro,mov_descripcion,mov_estatus,usuario_id,tipo) 
                   VALUES (txt_iddocumento,txt_idareaactual,txt_idareadestino,NOW(),txt_descripcion,'PENDIENTE',txt_idusuario ,'');
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_archivo = txt_archivo,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
ELSE
INSERT INTO movimiento_accion (movimiento_id,moviac_descripcion,moviac_estatus,moviac_fecharegistro) 
                        VALUES (txt_idmovimiento,txt_descripcion,'FINALIZADO',NOW());
	UPDATE movimiento SET
		movimiento.mov_estatus = txt_estado,
		movimiento.mov_descripcion_original = txt_descripcion
	WHERE movimiento.movimiento_id = txt_idmovimiento;
END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DOCUMENTO` (IN `txtdni` CHAR(8), IN `txtnombre` VARCHAR(150), IN `txtapepat` VARCHAR(100), IN `txtapemat` VARCHAR(100), IN `txtcelular` CHAR(9), IN `txtemail` VARCHAR(150), IN `txt_direccion` VARCHAR(255), IN `txt_ruc` CHAR(12), IN `txt_empresa` VARCHAR(255), IN `cmb_tipodocumento` INT, IN `txt_nrodocumentos` VARCHAR(15), IN `txt_folios` VARCHAR(225), IN `txt_asunto` VARCHAR(255), IN `txt_archivo` VARCHAR(255), IN `txt_representacion` VARCHAR(255))  BEGIN
DECLARE nrodocumento INT;
DECLARE cantidad INT;
DECLARE cod CHAR(12);
SET time_zone = '-5:00';
SET @cantidad :=(SELECT count(*) FROM documento );
IF @cantidad >= 1 AND @cantidad <= 8  THEN
SET @cod :=(SELECT CONCAT('D000000',(@cantidad+1)));
ELSEIF @cantidad >=9 AND @cantidad <=98 THEN
SET @cod :=(SELECT CONCAT('D00000',(@cantidad+1)));
ELSEIF @cantidad >=99 AND @cantidad <=998 THEN
SET @cod :=(SELECT CONCAT('D0000',(@cantidad+1)));
ELSEIF @cantidad >=999 AND @cantidad <=9998 THEN
SET @cod :=(SELECT CONCAT('D000',(@cantidad+1)));
ELSEIF @cantidad >=9999 AND @cantidad <=99998 THEN
SET @cod :=(SELECT CONCAT('D00',(@cantidad+1)));
ELSEIF @cantidad >=99999 AND @cantidad <=999998 THEN
SET @cod :=(SELECT CONCAT('D0',(@cantidad+1)));
ELSEIF @cantidad >=999999 THEN
SET @cod :=(SELECT CONCAT('D',(@cantidad+1)));
ELSE
SET @cod :=(SELECT CONCAT('D0000001'));
END IF;
set @nrodocumento:=(select count(*) FROM documento where area_origen =cmb_procedenciadocumento and tipodocumento_id=cmb_tipodocumento and extract(year from doc_fecharegistro)=2022 and doc_nrodocumento=txt_nrodocumentos);
/*set @nrodocumento:=(select COUNT(*) FROM documento where documento.doc_nrodocumento=txt_nrodocumentos AND documento.tipodocumento_id=cmb_tipodocumento);*/
IF @nrodocumento = 0 THEN
	INSERT INTO documento (documento_id,doc_dniremitente,doc_nombreremitente,doc_apepatremitente,doc_apematremitente,doc_celularremitente,
												 doc_emailremitente,doc_direccionremitente,doc_representacion,doc_ruc,doc_empresa,
												 tipodocumento_id,doc_nrodocumento,doc_folio,doc_asunto,doc_archivo,area_id,area_destino) 
								 VALUES (@cod,txtdni,txtnombre,txtapepat,txtapemat,txtcelular,
												 txtemail,txt_direccion,txt_representacion,txt_ruc,txt_empresa,
												 cmb_tipodocumento,(@cantidad+1),txt_folios,txt_asunto,txt_archivo,1,1);
	SELECT @cod;
ELSE
	SELECT 2;
END IF;

END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DOCUMENTO_INTERNO` (IN `txtdni` CHAR(8), IN `txtnombre` VARCHAR(150), IN `txtapepat` VARCHAR(100), IN `txtapemat` VARCHAR(100), IN `txtcelular` CHAR(9), IN `txtemail` VARCHAR(150), IN `txt_direccion` VARCHAR(255), IN `txt_ruc` CHAR(12), IN `txt_empresa` VARCHAR(255), IN `cmb_tipodocumento` INT, IN `txt_nrodocumentos` CHAR(15), IN `txt_folios` VARCHAR(255), IN `txt_asunto` VARCHAR(255), IN `txt_cuerpo` LONGTEXT, IN `txt_archivo` VARCHAR(255), IN `txt_representacion` VARCHAR(255), IN `cmb_procedenciadocumento` INT, IN `area_destino` INT, IN `institucion_fun` TEXT)  BEGIN
DECLARE nrodocumento INT;
DECLARE cantidad INT;
DECLARE cod CHAR(12);
SET time_zone = '-5:00';
SET @cantidad :=(SELECT count(*) FROM documento );
IF @cantidad >= 1 AND @cantidad <= 8  THEN
SET @cod :=(SELECT CONCAT('D000000',(@cantidad+1)));
ELSEIF @cantidad >=9 AND @cantidad <=98 THEN
SET @cod :=(SELECT CONCAT('D00000',(@cantidad+1)));
ELSEIF @cantidad >=99 AND @cantidad <=998 THEN
SET @cod :=(SELECT CONCAT('D0000',(@cantidad+1)));
ELSEIF @cantidad >=999 AND @cantidad <=9998 THEN
SET @cod :=(SELECT CONCAT('D000',(@cantidad+1)));
ELSEIF @cantidad >=9999 AND @cantidad <=99998 THEN
SET @cod :=(SELECT CONCAT('D00',(@cantidad+1)));
ELSEIF @cantidad >=99999 AND @cantidad <=999998 THEN
SET @cod :=(SELECT CONCAT('D0',(@cantidad+1)));
ELSEIF @cantidad >=999999 THEN
SET @cod :=(SELECT CONCAT('D',(@cantidad+1)));
ELSE
SET @cod :=(SELECT CONCAT('D0000001'));
END IF;
set @nrodocumento:=(select count(*) FROM documento where area_origen =cmb_procedenciadocumento and tipodocumento_id=cmb_tipodocumento and extract(year from doc_fecharegistro)=2022 and doc_nrodocumento=txt_nrodocumentos);
/*set @nrodocumento:=(select COUNT(*) FROM documento where documento.doc_nrodocumento=txt_nrodocumentos AND documento.tipodocumento_id=cmb_tipodocumento);*/
IF @nrodocumento = 0 THEN
	INSERT INTO documento (documento_id,doc_dniremitente,doc_nombreremitente,doc_apepatremitente,doc_apematremitente,doc_celularremitente,
												 doc_emailremitente,doc_direccionremitente,doc_representacion,doc_ruc,doc_empresa,
												 tipodocumento_id,doc_nrodocumento,doc_folio,doc_asunto,doc_cuerpo,doc_archivo,area_origen,area_id,area_destino,institucion_funcionario) 
								 VALUES (@cod,txtdni,txtnombre,txtapepat,txtapemat,txtcelular,
												 txtemail,txt_direccion,txt_representacion,txt_ruc,txt_empresa,
												 cmb_tipodocumento,txt_nrodocumentos,txt_folios,txt_asunto,txt_cuerpo,txt_archivo,cmb_procedenciadocumento,area_destino,area_destino,institucion_fun);
	SELECT @cod;
ELSE
	SELECT 2;
END IF;

END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_DOCUMENTO_INTERNO_TEMP` (IN `txtdni` CHAR(8), IN `txtnombre` VARCHAR(150), IN `txtapepat` VARCHAR(100), IN `txtapemat` VARCHAR(100), IN `txtcelular` CHAR(9), IN `txtemail` VARCHAR(150), IN `txt_direccion` VARCHAR(255), IN `txt_ruc` CHAR(12), IN `txt_empresa` VARCHAR(255), IN `cmb_tipodocumento` INT, IN `txt_nrodocumentos` CHAR(15), IN `txt_folios` VARCHAR(255), IN `txt_asunto` VARCHAR(255), IN `txt_cuerpo` LONGTEXT, IN `txt_archivo` VARCHAR(255), IN `txt_representacion` VARCHAR(255), IN `cmb_procedenciadocumento` INT, IN `area_destino` INT, IN `institucion_fun` TEXT)  begin
DECLARE nrodocumento INT;
DECLARE cantidad INT;
DECLARE cod CHAR(12);
SET time_zone = '-5:00';
SET @cantidad :=(SELECT count(*) FROM documento );
IF @cantidad >= 1 AND @cantidad <= 8  THEN
SET @cod :=(SELECT CONCAT('D000000',(@cantidad+1)));
ELSEIF @cantidad >=9 AND @cantidad <=98 THEN
SET @cod :=(SELECT CONCAT('D00000',(@cantidad+1)));
ELSEIF @cantidad >=99 AND @cantidad <=998 THEN
SET @cod :=(SELECT CONCAT('D0000',(@cantidad+1)));
ELSEIF @cantidad >=999 AND @cantidad <=9998 THEN
SET @cod :=(SELECT CONCAT('D000',(@cantidad+1)));
ELSEIF @cantidad >=9999 AND @cantidad <=99998 THEN
SET @cod :=(SELECT CONCAT('D00',(@cantidad+1)));
ELSEIF @cantidad >=99999 AND @cantidad <=999998 THEN
SET @cod :=(SELECT CONCAT('D0',(@cantidad+1)));
ELSEIF @cantidad >=999999 THEN
SET @cod :=(SELECT CONCAT('D',(@cantidad+1)));
ELSE
SET @cod :=(SELECT CONCAT('D0000001'));
END IF;
set @nrodocumento:=(select count(*) FROM documento where area_origen =cmb_procedenciadocumento and tipodocumento_id=cmb_tipodocumento and extract(year from doc_fecharegistro)=2022 and doc_nrodocumento=txt_nrodocumentos);
/*set @nrodocumento:=(select COUNT(*) FROM documento where documento.doc_nrodocumento=txt_nrodocumentos AND documento.tipodocumento_id=cmb_tipodocumento);*/
IF @nrodocumento = 0 THEN
	INSERT INTO documento_temp (documento_id,doc_dniremitente,doc_nombreremitente,doc_apepatremitente,doc_apematremitente,doc_celularremitente,
												 doc_emailremitente,doc_direccionremitente,doc_representacion,doc_ruc,doc_empresa,
												 tipodocumento_id,doc_nrodocumento,doc_folio,doc_asunto,doc_cuerpo,doc_archivo,area_origen,area_id,area_destino,institucion_funcionario) 
								 VALUES (@cod,txtdni,txtnombre,txtapepat,txtapemat,txtcelular,
												 txtemail,txt_direccion,txt_representacion,txt_ruc,txt_empresa,
												 cmb_tipodocumento,txt_nrodocumentos,txt_folios,txt_asunto,txt_cuerpo,txt_archivo,cmb_procedenciadocumento,area_destino,area_destino,institucion_fun);
	SELECT @cod,cmb_tipodocumento,txt_nrodocumentos;
ELSE
	SELECT 2;
END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_EMPLEADO` (IN `NOMBRE` VARCHAR(150), IN `APEPAT` VARCHAR(100), IN `APEMAT` VARCHAR(100), IN `FECHANACIMIENTO` VARCHAR(20), IN `NRODOCUMENTO` VARCHAR(11), IN `MOVIL` CHAR(9), IN `DIRECCION` VARCHAR(255), IN `EMAIL` VARCHAR(250))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) from  empleado where emple_nrodocumento=NRODOCUMENTO);
IF @CANTIDAD = 0 THEN
	INSERT INTO empleado(emple_nombre,emple_apepat,emple_apemat,emple_feccreacion,emple_fechanacimiento,emple_nrodocumento,emple_movil,emple_email,emple_estatus,emple_direccion)
	VALUES(NOMBRE,APEPAT,APEMAT,CURDATE(),FECHANACIMIENTO,NRODOCUMENTO,MOVIL,EMAIL,'ACTIVO',DIRECCION);
	SELECT 1;
ELSE
	SELECT 2;
END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_INSTI_CARGO` (IN `NOMBRE` VARCHAR(45), IN `APELLIDOS` VARCHAR(250), IN `CARGO` VARCHAR(100), IN `INSTITUCION` VARCHAR(100), IN `TITULO` VARCHAR(45))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM institucion_cargo where institucion_cargo.institucion=INSTITUCION);
IF @CANTIDAD=0 THEN
	INSERT INTO institucion_cargo(nombres,apellidos,cargo,institucion,titulo,estado) VALUES(NOMBRE,APELLIDOS,CARGO,INSTITUCION,TITULO,'ACTIVO');
	SELECT 1;
ELSE
	SELECT 2;
END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_TIPODOCUMENTO` (IN `NOMBRE` VARCHAR(250))  BEGIN
DECLARE CANTIDAD INT;
SET @CANTIDAD:=(SELECT COUNT(*) FROM tipo_documento where tipo_documento.tipodo_descripcion=NOMBRE);
IF @CANTIDAD=0 THEN
	INSERT INTO tipo_documento(tipodo_descripcion,tipodo_estado) VALUES(NOMBRE,'ACTIVO');
	SELECT 1;
ELSE
	SELECT 2;
END IF;

END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_REGISTRAR_USUARIO` (IN `usuario` VARCHAR(150), IN `clave` VARCHAR(255), IN `empleado` INT, IN `area` INT, IN `rol` VARCHAR(30))  BEGIN
	DECLARE cant_usuario INT;
	DECLARE cant_usuario2 INT;
	SET time_zone = '-5:00';
	set @cant_usuario:=(select count(*) FROM usuario where usuario.empleado_id=empleado AND usuario.area_id=area);
	IF @cant_usuario = 0 THEN
		set @cant_usuario2:=(select count(*) FROM usuario where usuario.usu_usuario = usuario);
		IF @cant_usuario2 = 0 THEN
			INSERT INTO usuario (usu_usuario,usu_contra,usu_feccreacion,empleado_id,usu_estatus,area_id,usu_rol) 
									 VALUES (usuario,clave,CURDATE(),empleado,'ACTIVO',area,rol);
			SELECT 1;
		ELSE
			SELECT 3;
		END IF;
	ELSE
		SELECT 2;
	END IF;
END

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_SEARCH_TIPODOCUMENTO` (IN `iddocumento` INT)  SELECT
tipo_documento.tipodocumento_id,
tipo_documento.tipodo_descripcion
FROM
tipo_documento 
where tipo_documento.tipodo_estado !='INACTIVO' and tipo_documento.tipodocumento_id =iddocumento

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_VERIFICARUSUARIO` (IN `USUARIO` VARCHAR(150))  SELECT
usuario.usu_id,
usuario.usu_usuario,
usuario.usu_contra,
usuario.usu_feccreacion,
usuario.usu_fecupdate,
usuario.empleado_id,
usuario.usu_observacion,
usuario.usu_estatus,
IFNULL(usuario.area_id,'') area_id,
usuario.usu_rol,
UPPER(empleado.emple_nombre) emple_nombre,
UPPER(empleado.emple_apepat) emple_apepat,
UPPER(empleado.emple_apemat) emple_apemat,
empleado.emple_feccreacion,
DATE_FORMAT(empleado.emple_fechanacimiento,'%d/%m/%Y')emple_fechanacimiento ,
empleado.emple_nrodocumento,
empleado.emple_movil,
empleado.emple_email as emple_email,
empleado.emple_estatus,
UPPER(area.area_nombre) area_nombre,
area.area_fecha_registro,
area.area_estado,
UPPER(usuario.usu_usuario) as usuario,
UPPER(empleado.emple_direccion) emple_direccion,
empleado.empl_fotoperfil
FROM
usuario
INNER JOIN empleado ON usuario.empleado_id = empleado.empleado_id
LEFT JOIN area ON usuario.area_id = area.area_cod

WHERE usuario.usu_usuario = BINARY USUARIO

DELIMITER ;

