var table;

function listar_insti_cargo() {
    table = $("#tabla_insti_cargo").DataTable({
        "ordering": false,
        "pageLength": 10,
        "destroy": true,
        "async": false,
        "responsive": true,
        "autoWidth": false,
        "ajax": {
            "method": "POST",
            "url": "../controlador/institucion-cargo/controlador_insti_cargo_listar.php",
        },
        "columns": [
            { "defaultContent": "" },
            { "data": "titulo" },
            { "data": "nombres" },
            { "data": "apellidos" },
            { "data": "cargo" },
            { "data": "institucion" },
            {
                "data": "estado",
                render: function(data, type, row) {
                    if (data == 'INACTIVO') {
                        return '<span class="badge bg-danger bg-lg">' + data + '</span>';
                    } else {
                        return '<span class="badge bg-success bg-lg">' + data + '</span>';
                    }

                }
            },

            {
                "data": null,
                render: function(data, type, row) {
                    return "<button class='editar btn btn-primary  btn-sm'  type='button' ><b><i class='fas fa-edit'></i>&nbsp;Editar</b></button>&nbsp";

                }
            }

        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $($(nRow).find("td")[7]).css('text-align', 'center');
        },
        "language": idioma_espanol,
        select: true
    });
    table.on('draw.dt', function() {
        var PageInfo = $('#tabla_insti_cargo').DataTable().page.info();
        table.column(0, { page: 'current' }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

/**
 * CAPTURAR LOS DATOS DE LA TABLA PARA MOSTRARLO EN EL FORMULARIO
 */
$('#tabla_insti_cargo').on('click', '.editar', function() {
    var data = table.row($(this).parents('tr')).data(); //Detecta a que fila hago click y me captura los datos en la variable data.
    if (table.row(this).child.isShown()) { //Cuando esta en tamaño responsivo
        var data = table.row(this).data();
    }

    //Recuperacion de Datos al hacer click en el boton edit
    $('.form-control').removeClass("is-invalid").removeClass("is-valid");
    $("#modal_editar_insti_cargo").modal('show');
    $("#idinsti_cargo").val(data.idinstitucion_cargo);
    $("#txt_nombre_editar").val(data.nombres);
    $("#txt_apellido_editar").val(data.apellidos);
    $("#txt_cargo_editar").val(data.cargo);
    $("#txt_institucion_editar").val(data.institucion);
    $("#txt_titulo_editar").val(data.titulo);

    $("#cbm_estatus").val(data.estado).trigger("change");
})

function Registrar_Insti_Cargo() {
    var nombre = $("#txt_nombre").val();
    var apellidos = $("#txt_apellidos").val();
    var cargo = $("#txt_cargo").val();
    var institucion = $("#txt_institucion").val();
    var titulo = $("#txt_titulo").val();

    console.log(nombre, apellidos, cargo, institucion, titulo);

    if (nombre.length == 0) {
        ValidacionInput("txt_nombre");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    if (apellidos.length == 0) {
        ValidacionInput("txt_apellidos");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    if (cargo.length == 0) {
        ValidacionInput("txt_cargo");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    if (institucion.length == 0) {
        ValidacionInput("txt_institucion");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    if (titulo.length == 0) {
        ValidacionInput("txt_titulo");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    $.ajax({
        "url": "../controlador/institucion-cargo/controlador_insti_cargo_registro.php",
        type: 'POST',
        data: {
            nombre: nombre,
            apellidos: apellidos,
            cargo: cargo,
            institucion: institucion,
            titulo: titulo
        }
    }).done(function(resp) {
        console.log(resp);
        if (resp > 0) {
            if (resp == 1) {
                LimpiarCampos();
                listar_insti_cargo();
                Swal.fire("Mensaje De Confirmacion", "Datos guardados correctamente", "success");

            } else {
                LimpiarCampos();
                Swal.fire("Mensaje De Advertencia", "El Funcionario ya existe en nuestra base de datos", "warning");
            }
        } else {
            Swal.fire("Mensaje De Error", "Lo sentimos el registro no se pudo completar", "error");
        }
    })

}
/**
 * FUNCION PARA MODIFICAR LOS REGISTRO EN EL FORMULARIO 
 */
function Modificar_TipoDocumento() {
    var idinsti_cargo = $("#idinsti_cargo").val();
    var nombre = $("#txt_nombre_editar").val();
    var apellidos = $("#txt_apellido_editar").val();
    var cargo = $("#txt_cargo_editar").val();
    var institucion = $("#txt_institucion_editar").val();
    var titulo = $("#txt_titulo_editar").val();
    var estatus = $("#cbm_estatus").val();

    console.log(idinsti_cargo, nombre, apellidos, cargo, institucion, titulo, estatus);

    if (nombre.length == 0) {
        ValidacionInput("txt_nombre_editar");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }
    if (apellidos.length == 0) {
        ValidacionInput("txt_apellidos");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    if (cargo.length == 0) {
        ValidacionInput("txt_cargo");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    if (institucion.length == 0) {
        ValidacionInput("txt_institucion");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    if (titulo.length == 0) {
        ValidacionInput("txt_titulo");
        return Swal.fire("Mensaje de advertencia", "Tiene algunos campos vacios", "warning");
    }

    $.ajax({
        "url": "../controlador/institucion-cargo/controlador_insti_cargo_modificar.php",
        type: 'POST',
        data: {
            idinsti_cargo: idinsti_cargo,
            nombre: nombre,
            apellidos: apellidos,
            cargo: cargo,
            institucion: institucion,
            titulo: titulo,
            estatus: estatus
        }
    }).done(function(resp) {
        console.log("respuesta: ", resp);
        if (resp > 0) {
            if (resp == 1) {
                $("#modal_editar").modal('hide');
                listar_insti_cargo();
                Swal.fire("Mensaje De Confirmacion", "Datos actualizados correctamente", "success");

            } else {
                LimpiarCampos();
                Swal.fire("Mensaje De Advertencia", "El tipo de documento ya existe en nuestra base de datos", "warning");
            }
        } else {
            Swal.fire("Mensaje De Error", "Lo sentimos la actualizacion no se pudo completar", "error");
        }
    })

}

function ValidacionInput(txt_nombre) {
    Boolean($("#" + txt_nombre).val().length > 0) ? $("#" + txt_nombre).removeClass('is-invalid').addClass("is-valid") : $("#" + txt_nombre).removeClass('is-valid').addClass("is-invalid");
}

function LimpiarCampos() {
    $("#txt_nombre").val("");
    $("#txt_apellidos").val("");
    $("#txt_cargo").val("");
    $("#txt_institucion").val("");
    $("#txt_titulo").val("");


    $('.form-control').removeClass("is-invalid").removeClass("is-valid");
    $("#modal_registro_insti_cargo").modal('hide');
}

function AbrirModalRegistro_Insti_Cargo() {
    $('.form-control').removeClass("is-invalid").removeClass("is-valid");
    $("#modal_registro_insti_cargo").modal('show');

}