<?php
session_start();
if (isset($_SESSION['tiempo_sistema'])) {
    if ($_SESSION['tiempo_sistema'] < time()) {
        session_destroy();
        echo "<script>sesion();</script>";
    } else {
        $_SESSION['tiempo_sistema'] = time() + 1800;
    }
}
if (!isset($_SESSION['id_usuario_sistematramite'])) {
    echo "<script>sesion();</script>";
}
?>
<link rel="stylesheet" href="_Plantilla/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Gestor Consulta Tr&aacute;mites</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home"></i> Inicio</a></li>
                    <li class="breadcrumb-item active"> Tr&aacute;mites</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card  card-danger card-outline ">
                    <div class="card-header  d-flex p-0">
                        <h3 class="card-title p-3"><strong>Consulta Tr&aacute;mite</strong></h3>
                        <ul class="nav nav-pills ml-auto p-2">
                            <!-- <li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab">Nuevo Tr&aacute;mite</a></li> -->
                            <!-- <li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab">Consultar Tr&aacute;mite</a></li> -->
                        </ul>
                    </div>

                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">

                            <!--/*************************************
                                            PAGINA DE CONSULTA TRAMITE
                                        ***************************************/ -->
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="div_buscartramite">
                                            <div class="card card-success">
                                                <div class="card-header" style="background-color: #81172d;">
                                                    <h3 class="card-title"><b>Rastrear Tr&aacute;mite</b></h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <form onsubmit="return false" autocomplete="false">
                                                        <div class="row">
                                                            <div class="col-lg-4 form-group">
                                                                <label>Nº Documento:</label>
                                                                <input class="form-control" type="text" placeholder="Nro documento" maxlength="15" id="txtnrodocumento" onkeypress="return soloNroDocumento(event)">
                                                            </div>
                                                            <div class="col-lg-4 form-group">
                                                                <label>A&ntilde;o del documento</label>
                                                                <select id="cbm_anio" style="text-align: center;width: 100%" class="select2 select2-danger" data-dropdown-css-class="select2-danger">
                                                                    <option>2021</option>
                                                                    <option>2022</option>
                                                                    <option>2023</option>
                                                                    <option>2024</option>
                                                                    <option>2025</option>
                                                                    <option>2026</option>
                                                                    <option>2027</option>
                                                                    <option>2028</option>
                                                                    <option>2029</option>
                                                                    <option>2030</option>
                                                                    <option>2031</option>
                                                                    <option>2032</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4 fom-group">
                                                                <label>&nbsp;</label>
                                                                <button id="btn_buscar" onclick="buscar_orden_interno()" class="btn btn-block bg-gradient-danger"><i class="fa fa-search"></i></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="div_datostramite" style="display: none;">
                                            <div class="card card-orange">
                                                <div class="card-header">
                                                    <h3 class="card-title" style="color: white;"><b>Informaci&oacute;n del Tr&aacute;mite</b></h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <form onsubmit="return false" autocomplete="false">
                                                        <div class="row">
                                                            <div class="col-lg-12 ">
                                                                <div class="container-fluid">
                                                                    <div class="row mb-2">
                                                                        <div class="col-sm-10">
                                                                            <p id="nroDocumento" style="width: 100%;text-align: center;font-weight: bold;border-top: 2px solid black;background-color: #E4E4E4;font-size: 24px;"></p>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <!--                                                                                         <ol class="float-sm-right">
                                                                                            <button class="btn btn-danger btn-sm" style="width: 100%;" onclick="Generar_Reporte()"><i class="fa fa-print"></i> &nbsp;Imprimir Ticket</button>
                                                                                        </ol> -->
                                                                            <ol class="float-sm-right">
                                                                                <button class="btn btn-success btn-sm" style="width: 100%;" onclick="nueva_busqueda()"><i class="fa fa-search"></i> &nbsp;Nueva Busqueda</button>
                                                                            </ol>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="">
                                                                    <table class="table" style="width: 100%;">
                                                                        <tr>
                                                                            <td style="width: 100%;text-align: center;font-weight: bold;border-top: 2px solid black;background-color: #E4E4E4" colspan="2">
                                                                                DATOS DEL REMITENTE
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">DNI</td>
                                                                            <td style="width: 70%;"><label id="lb_dni"></label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">NOMBRES - APELLIDOS</td>
                                                                            <td style="width: 70%;" id="lb_datos"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">DIRECCI&Oacute;N</td>
                                                                            <td style="width: 70%;" id="lb_direccion"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">E-MAIL</td>
                                                                            <td style="width: 70%;" id="lb_email"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">REPRESENTACI&Oacute;N</td>
                                                                            <td style="width: 70%;" id="lb_representacion"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">AREA</td>
                                                                            <td style="width: 70%;" id="lb_area_origen"></td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="table-responsive">
                                                                    <table class="table" style="width: 100%;">
                                                                        <tr>
                                                                            <td style="width: 100%;text-align: center;font-weight: bold;border-top: 2px solid black;background-color: #E4E4E4" colspan="2">
                                                                                DATOS DEL DOCUMENTO
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">TIPO DOCUMENTO</td>
                                                                            <td style="width: 70%;" id="lb_tipodocumento"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">NRO DOCUMENTO</td>
                                                                            <td style="width: 70%;">
                                                                                <label id="lb_nrodocumento"></label>
                                                                                <label hidden id="lb_iddocumento"></label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 30%;font-weight: bold;background-color: #E4E4E4">ASUNTO</td>
                                                                            <td style="width: 70%;" rowspan="4" id="lb_asunto"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="border: 0px solid black;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="border: 0px solid black;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="border: 0px solid black;">&nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="card card-primary">
                                                    <div class="card-header">
                                                        <h3 class="card-title"><b>Seguimiento Tr&aacute;mite</b></h3>
                                                    </div>
                                                    <div class="card-body">
                                                        <form onsubmit="return false" autocomplete="false">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="timeline">
                                                                        <div id="div_historial2"></div>
                                                                        <div id="div_historial"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="js/console_tramite.js?rev=<?php echo time(); ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        bsCustomFileInput.init();
        //buscar_orden_externo();
    });
</script>
<style type="text/css">
    a,
    h3,
    h1 {
        font-weight: bold !important;
    }
</style>
<script type="text/javascript">
    $('.select2').select2();

    function soloNumerosyletrasDatapicker(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "0123456456789-";
        especiales = "";

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function soloNumerosyletras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz0123456456789:/.,\@-_";
        especiales = "8-37-39-46-58";

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function soloNumeros(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla == 8) {
            return true;
        }
        // Patron de entrada, en este caso solo acepta numeros
        patron = /[0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = "8-37-39-46";
        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function soloNroDocumento(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "abcdefghijklmnñopqrstuvwxyz0123456456789\@-_";
        especiales = "8-37-39-46-58";

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function mantenimiento() {
        Swal.fire("Mensaje de Aviso", "Opci&oacute;n en mantenimiento", "info");
    }
</script>
<style>
    .btn {
        font-weight: bold;
    }

    h1 {
        font-weight: bold;
    }

    small {
        font-weight: bold;
    }

    .select2 {
        font-weight: bold;
        text-align-last: center;
    }
</style>
<style type="text/css">
    td {
        border: 2px solid black;
        word-wrap: break-word;
        text-align: justify;
    }
</style>
<style type="text/css">
    .timeline>div>div {
        margin-bottom: 15px;
        margin-right: 10px;
        position: relative;
    }

    .timeline>div>.time-label>span {
        border-radius: 4px;
        background-color: #fff;
        display: inline-block;
        font-weight: 600;
        padding: 5px;
    }

    .timeline>div>.fa,
    .timeline>div>.fab,
    .timeline>div>.far,
    .timeline>div>.fas,
    .timeline>div>div>.fas,
    .timeline>div>.glyphicon,
    .timeline>div>.ion {
        background: #adb5bd;
        background-color: rgb(173, 181, 189);
        border-radius: 50%;
        font-size: 15px;
        height: 30px;
        left: 18px;
        line-height: 30px;
        position: absolute;
        text-align: center;
        top: 0;
        width: 30px;
    }

    .timeline>div>div>.timeline-item {
        box-shadow: 0 0 1px rgba(0, 0, 0, .125), 0 1px 3px rgba(0, 0, 0, .2);
        border-radius: .25rem;
        background: #fff;
        color: #495057;
        margin-left: 60px;
        margin-right: 15px;
        margin-top: 0;
        padding: 0;
        position: relative;
    }

    .timeline>div>div>.timeline-item>.time {
        color: #999;
        float: right;
        font-size: 12px;
        padding: 10px;
    }

    .timeline>div>div>.timeline-item>.timeline-header {
        border-bottom: 1px solid rgba(0, 0, 0, .125);
        color: #495057;
        font-size: 16px;
        line-height: 1.1;
        margin: 0;
        padding: 10px;
    }

    .timeline>div>div>.timeline-item>.timeline-body,
    .timeline>div>.timeline-item>.timeline-footer {
        padding: 10px;
    }

    .timeline>div>div>.timeline-item>.timeline-body,
    .timeline>div>.timeline-item>.timeline-footer {
        padding: 10px;
    }
</style>
<div id="div_progress">
    <div class="modal fade bs-example-modal-lg" id="modal_procesar_datos_2">
        <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 75%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-clock-o"></i> ESPERE UNOS MOMENTOS PORFAVOR: LOS DATOS SE ESTAN PROCESANDO.</h4>
                </div>
                <div class="modal-body center-block">
                    <div id="div_cadena_progress"></div>
                </div>
            </div>
        </div>
    </div>
</div>