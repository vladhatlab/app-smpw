<?php
    require '../../modelo/modelo_institucion_cargo.php';
    $MT = new Modelo_InstiCargo();//Instanciamos
    $nombre = htmlspecialchars(strtoupper($_POST['nombre']),ENT_QUOTES,'UTF-8');
    $apellidos = htmlspecialchars(strtoupper($_POST['apellidos']),ENT_QUOTES,'UTF-8');
    $cargo = htmlspecialchars(strtoupper($_POST['cargo']),ENT_QUOTES,'UTF-8');
    $institucion = htmlspecialchars(strtoupper($_POST['institucion']),ENT_QUOTES,'UTF-8');
    $titulo = htmlspecialchars(strtoupper($_POST['titulo']),ENT_QUOTES,'UTF-8');
    
    $consulta = $MT->Registrar_Insti_Cargo($nombre,$apellidos,$cargo,$institucion,$titulo);
    echo $consulta;

?>