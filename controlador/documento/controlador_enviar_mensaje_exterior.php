<?php

require "../PHPMailer/class.phpmailer.php";
require "../PHPMailer/class.smtp.php";
/* =============================================
      OBTENER EL SISTEMA OPERATIVO DE DONDE SE ENVIO EL CORREO
   ============================================= */
class ControladorMensaje
{
  /* =============================================
      OBTENER EL SISTEMA OPERATIVO DE DONDE SE ENVIO EL CORREO
   ============================================= */
  public static function getOS()
  {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
      '/windows nt 10/i'      =>  'Windows 10',
      '/windows nt 6.3/i'     =>  'Windows 8.1',
      '/windows nt 6.2/i'     =>  'Windows 8',
      '/windows nt 6.1/i'     =>  'Windows 7',
      '/windows nt 6.0/i'     =>  'Windows Vista',
      '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
      '/windows nt 5.1/i'     =>  'Windows XP',
      '/windows xp/i'         =>  'Windows XP',
      '/windows nt 5.0/i'     =>  'Windows 2000',
      '/windows me/i'         =>  'Windows ME',
      '/win98/i'              =>  'Windows 98',
      '/win95/i'              =>  'Windows 95',
      '/win16/i'              =>  'Windows 3.11',
      '/macintosh|mac os x/i' =>  'Mac OS X',
      '/mac_powerpc/i'        =>  'Mac OS 9',
      '/linux/i'              =>  'Linux',
      '/ubuntu/i'             =>  'Ubuntu',
      '/iphone/i'             =>  'iPhone',
      '/ipod/i'               =>  'iPod',
      '/ipad/i'               =>  'iPad',
      '/android/i'            =>  'Android',
      '/blackberry/i'         =>  'BlackBerry',
      '/webos/i'              =>  'Mobile'
    );

    foreach ($os_array as $regex => $value) {
      if (preg_match($regex, $user_agent)) {
        $os_platform = $value;
      }
    }

    return $os_platform;
  }


  /* =============================================
      OBTENER EL NAVEGADOR DE DONDE SE ENVIO EL CORREO
   ============================================= */
  public static function getBrowser()
  {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $browser        = "Unknown Browser";

    $browser_array = array(
      '/msie/i'      => 'Internet Explorer',
      '/firefox/i'   => 'Firefox',
      '/safari/i'    => 'Safari',
      '/chrome/i'    => 'Chrome',
      '/edge/i'      => 'Edge',
      '/opera/i'     => 'Opera',
      '/netscape/i'  => 'Netscape',
      '/maxthon/i'   => 'Maxthon',
      '/konqueror/i' => 'Konqueror',
      '/mobile/i'    => 'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value) {
      if (preg_match($regex, $user_agent)) {
        $browser = $value;
      }
    }

    return $browser;
  }
}

function smtpmailer($to, $from, $from_name, $subject, $body)
{
  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->SMTPAuth = true;

  $mail->SMTPSecure = 'ssl';
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = "465";
  $mail->Username = 'desarrollovladhat@gmail.com';
  $mail->Password = '1597531994Vlad';
  $mail->IsHTML(true);
  $mail->From = "desarrollovladhat@gmail.com";
  $mail->FromName = $from_name;
  $mail->Sender = $from;
  $mail->AddReplyTo($from, $from_name);
  $mail->Subject = $subject;
  $mail->Body = $body;
  $mail->AddAddress($to);
  if (!$mail->Send()) {
    $error = "Please try Later, Error Occured while Processing...";
    return $error;
  } else {
    $error = "Se envio correo de controlador exterior";
    return $error;
  }
}

/* =============================================
      OBTENER EL SISTEMA OPERATIVO DE DONDE SE ENVIO EL CORREO
   ============================================= */

if (isset($_POST["id_seguimiento"])) {
  $id_seguimiento = $_POST["id_seguimiento"];
  $nro_tramite    = $_POST["nro_tramite"];
  $tipo_tramite   = $_POST["tipo_tramite"];
  $txtemail       = $_POST["txtemail"];
  $txtnombre      = $_POST["txtnombre"];
}


date_default_timezone_set('America/Lima');

$template = file_get_contents('../../Vista/template.php');
$template = str_replace("{{name}}", $txtnombre, $template);
$template = str_replace("{{id_seguimiento}}", $id_seguimiento, $template);
$template = str_replace("{{action_url_2}}", 'http://pruebastdw.dirislimasur.gob.pe:8080/consulta_tramite.php', $template);
$template = str_replace("{{action_url_1}}", 'http://pruebastdw.dirislimasur.gob.pe:8080/consulta_tramite.php', $template);
$template .= str_replace("{{year}}", date('Y'), $template);
$template = str_replace("{{operating_system}}", ControladorMensaje::getBrowser(), $template);
$template = str_replace("{{browser_name}}", ControladorMensaje::getBrowser(), $template);


$to1   = $txtemail;
$from1 = 'desarrollovladhat@gmail.com';
$name1 = 'Colegio Regional de Obstetras III Lima-Callao';
$subj1 = 'Datos para el Seguimento Virtual';
$msg1 = $template;
$error = smtpmailer($to1, $from1, utf8_decode($name1), $subj1, utf8_decode($msg1));
echo $txtemail;
