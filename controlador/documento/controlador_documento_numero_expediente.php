<?php
    require '../../modelo/modelo_documento.php';
	$MC = new Modelo_documento();
    $tipo_documento= htmlspecialchars($_POST['tipo_documento'],ENT_QUOTES,'UTF-8');
	$area_id= htmlspecialchars($_POST['area_id'],ENT_QUOTES,'UTF-8');
    
	$consulta = $MC->listar_documentos_num_expediente($area_id,$tipo_documento);
	if ($consulta) {
		echo json_encode($consulta);
	}else{
		echo '{
		    "sEcho": 1,
		    "iTotalRecords": "0",
		    "iTotalDisplayRecords": "0",
		    "aaData": []
		}';
	}
