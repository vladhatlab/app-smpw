<?php

class ControladorDocumento
{
    static public function ctrMostrarDocumento($item, $valor)
    {

        $respuesta = ModeloDocumento::mdlMostrarDocumento($item, $valor);
        return $respuesta;
    }

    static public function ctrMostrarDocumento_temp($item1, $valor1, $valor2)
    {

        $respuesta = ModeloDocumento::mdlMostrarDocumento_temp($item1, $valor1, $valor2);
        return $respuesta;
    }
    static public function ctrMostrarDocumentosDerivados($valor1)
    {

        $respuesta = ModeloDocumento::mdlMostrarDocumentosDerivados($valor1);
        return $respuesta;
    }
    static public function ctrMostrarExpedienteDerivado($valor1)
    {

        $respuesta = ModeloDocumento::mdlMostrarExpedienteDerivado($valor1);
        return $respuesta;
    }
}
