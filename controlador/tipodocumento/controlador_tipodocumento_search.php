<?php
    require '../../modelo/modelo_tipodocumento.php';
    $MT = new Modelo_Tipodocumento();//Instanciamos
    $idtipodocumento = htmlspecialchars(strtoupper($_POST['idtipodocumento']),ENT_QUOTES,'UTF-8');

    
    $consulta = $MT->Search_TipoDocumento($idtipodocumento);
    if ($consulta) {
		echo json_encode($consulta);
	}else{
		echo '{
		    "sEcho": 1,
		    "iTotalRecords": "0",
		    "iTotalDisplayRecords": "0",
		    "aaData": []
		}';
	}

?>