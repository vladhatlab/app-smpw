<?php

class ApiCrudRequest
{
    public $d1, $d2;

    public function sendGet()
    {
        //datos a enviar
        $numero = $this->d1;
        $fecha = $this->d2;
        //url contra la que atacamos
        $ch = curl_init("http://app17.susalud.gob.pe:8081/webservices/ws_procesos/obtenerDatosReniecFeNacimiento?numero=$numero&feNacimiento=$fecha");
        //a true, obtendremos una respuesta de la url, en otro caso, 
        //true si es correcto, false si no lo es
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //establecemos el verbo http que queremos utilizar para la petición
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        //enviamos el array data
        /*         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); */
        //obtenemos la respuesta
        $response = curl_exec($ch);
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);
        if (!$response) {
            return false;
        } else {
            echo $response;
        }
    }
}
if (isset($_POST["d1"]) && isset($_POST["d2"])) {

    $d2 = date("d-m-Y", strtotime($_POST["d2"]));

     var_dump($d1 . " " . $d2); 

    $new = new ApiCrudRequest();
    $new->d1 = $_POST["d1"];
    $new->d2 = $d2;
    $new->sendGet();
    
} else {
    echo "no se envio el post";
}
