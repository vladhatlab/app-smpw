<?php
require_once "modelo_conexion.php";

class ModeloEmpleado
{
    static public function mdlMostrarEmpleado($tabla, $item, $valor)
    {
        if ($item != null) {
            $stmt = Conexion_PDO::conectar_pdo()->prepare("SELECT empleado_id, emple_nrodocumento,emple_nombre,emple_apepat,emple_apemat FROM $tabla WHERE $item=:$item");
            $stmt->bindParam(":" . $item, $valor, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch();
        }
        $stmt->close();
        $stmt = null;
    }
}
