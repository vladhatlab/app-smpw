<?php
require_once "modelo_conexion.php";

class ModeloDocumento
{

    static public function mdlMostrarDocumento($item, $valor)
    {
        if ($item != null) {
            $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_LISTAR_DOCUMENTOS_PDF(?)");
            $stmt->bindParam(1, $valor, PDO::PARAM_STR);
            $stmt->execute();

            return $stmt->fetch();
        } else {
            $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_LISTAR_DOCUMENTOS_PDF_ALL()");

            $stmt->execute();

            return $stmt->fetchAll();
        }

        $stmt->close();

        $stmt = null;
    }

    static public function mdlMostrarDocumento_temp($item1, $valor1, $valor2)
    {
        if ($item1 != null) {
            $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_LISTAR_DOCUMENTOS_PDF_TEMP(?,?)");

            $stmt->bindParam(1, $valor1, PDO::PARAM_STR);

            $stmt->bindParam(2, $valor2, PDO::PARAM_STR);

            $stmt->execute();

            return $stmt->fetch();
        } else {
            $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_LISTAR_DOCUMENTOS_PDF_ALL()");

            $stmt->execute();

            return $stmt->fetchAll();
        }

        $stmt->close();

        $stmt = null;
    }


    static public function mdlMostrarNombreEmisorReceptor($valor1, $valor2)
    {

        $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_OBTENER_EMISOR_RECEPTOR(?,?)");
        $stmt->bindParam(1, $valor1, PDO::PARAM_INT);
        $stmt->bindParam(2, $valor2, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch();

        $stmt->close();

        $stmt = null;
    }

    static public function mdlMostrarNombreEmisorReceptor_temp($valor1, $valor2, $valor3)
    {

        $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_OBTENER_EMISOR_RECEPTO_TEMP(?,?,?)");
        $stmt->bindParam(1, $valor1, PDO::PARAM_INT);
        $stmt->bindParam(2, $valor2, PDO::PARAM_STR);
        $stmt->bindParam(3, $valor3, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch();

        $stmt->close();

        $stmt = null;
    }

    static public function mdlMostrarDocumentosDerivados($valor1)
    {

        $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_LISTAR_DOCUMENTOS_SEGUIMIENTO(?)");
        $stmt->bindParam(1, $valor1, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll();

        $stmt->close();

        $stmt = null;
    }


    static public function mdlMostrarExpedienteDerivado($valor1)
    {

        $stmt = Conexion_PDO::conectar_pdo()->prepare("CALL PA_LISTAR_DOCUMENTO_PRINT_HOJADERUTA(?)");
        $stmt->bindParam(1, $valor1, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll();

        $stmt->close();

        $stmt = null;
    }
}