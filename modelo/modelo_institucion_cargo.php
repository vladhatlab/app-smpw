<?php
class Modelo_InstiCargo
{
	private $conexion;
	function __construct()
	{
		require_once 'modelo_conexion.php';
		$this->conexion = new conexion();
		$this->conexion->conectar();
	}


	function listar_insti_cargo()
	{
		$sql = "call PA_LISTAR_INSTI_CARGO";

		$arreglo = array();
		if ($consulta = $this->conexion->conexion->query($sql)) {

			while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
				$arreglo["data"][] = $consulta_VU;
			}
			return $arreglo;
			$this->conexion->cerrar();
		}
	}

	function combo_funcionario_destinatario()
	{
		$sql = "call PA_LISTAR_INSTI_CARGO";
		$arreglo = array();
		if ($consulta = $this->conexion->conexion->query($sql)) {
			while ($consulta_VU = mysqli_fetch_array($consulta)) {
				$arreglo[] = $consulta_VU;
			}
			return $arreglo;
			$this->conexion->cerrar();
		}
	}

	function Registrar_Insti_Cargo($nombre, $apellidos, $cargo, $institucion, $titulo)
	{
		$sql = "call PA_REGISTRAR_INSTI_CARGO('$nombre','$apellidos','$cargo','$institucion','$titulo')";
		if ($consulta = $this->conexion->conexion->query($sql)) {
			if ($row = mysqli_fetch_array($consulta)) {
				return $id = trim($row[0]); //retorna valores
			}
			$this->conexion->cerrar();
		}
	}

	function Modificar_Insti_Cargo($idinsti_cargo, $nombre, $apellidos, $cargo, $institucion, $titulo, $estatus)
	{
		$sql = "call PA_MODIFICAR_INSTI_CARGO('$idinsti_cargo','$nombre','$apellidos','$cargo','$institucion','$titulo','$estatus')";
		if ($consulta = $this->conexion->conexion->query($sql)) {
			if ($row = mysqli_fetch_array($consulta)) {
				return $id = trim($row[0]); //retorna valores
			}
			$this->conexion->cerrar();
		}
	}

	function buscar_funcionario($buscar)
	{
		$sql = "call PA_BUSCARFUNCIONARIO('$buscar')";
		$arreglo = array();
		if ($consulta = $this->conexion->conexion->query($sql)) {

			while ($consulta_VU = mysqli_fetch_array($consulta)) {

				$arreglo[] = $consulta_VU;
			}
			return $arreglo;

			$this->conexion->cerrar();
		}
	}
}
