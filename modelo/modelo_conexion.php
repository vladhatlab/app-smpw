<?php
class conexion
{
	private $servidor;
	private $usuario;
	private $contrasena;
	private $basedatos;
	public $conexion;
	public function __construct()
	{
		$this->servidor = "localhost";
		$this->usuario = "root";
		$this->contrasena = "";
		$this->basedatos = "bd_mesapartes7";

		//HOSTING
		/* 
		$this->servidor = "localhost";
		$this->usuario = "krungbpp_usutramite";
		$this->contrasena = "s9udT=s~#[))";
		$this->basedatos = "krungbpp_tramite";
		*/
	}
	function conectar()
	{
		$this->conexion = new mysqli($this->servidor, $this->usuario, $this->contrasena, $this->basedatos);
		$this->conexion->set_charset("utf8");
	}
	function cerrar()
	{
		$this->conexion->close();
	}
}

class Conexion_PDO
{

	static public function conectar_pdo()
	{
		$link = new PDO(
			"mysql:host=localhost;dbname=bd_mesapartes7",
			"root",
			""
		);
		$link->exec("set names utf8");
		return $link;
	}

	//HOSTING
	/* 	static public function conectar_pdo()
	{
		$link = new PDO(
			"mysql:host=localhost;dbname=krungbpp_tramite",
			"krungbpp_usutramite",
			"s9udT=s~#[))"
		);
		$link->exec("set names utf8");
		return $link;
	} */
}