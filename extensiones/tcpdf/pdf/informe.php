<?php

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once "../../../controlador/documento.controlador.php";
require_once "../../../modelo/documento.modelo.php";

require_once('./tcpdf_include.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{
    //Page header
    public function Header()
    {
        // Logo
        $image_file = K_PATH_IMAGES . '/oficio/header.PNG';
        $this->Image($image_file, 0, 0, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        //$this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        /*         $image_file = K_PATH_IMAGES . '/oficio/seccion.PNG';
        $this->Image($image_file, 0, 65, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false); */
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        //$this->SetY(-15);
        // Set font
        //$this->SetFont('helvetica', 'I', 8);
        // Page number
        //$this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');


        $image_file = K_PATH_IMAGES . 'oficio/footer.PNG';
        $this->Image($image_file, 0, 252, 0, 45, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}

$item = 'documento_id';
$valor = $_GET["idDocumento"];

$respuestaDocumento = ControladorDocumento::ctrMostrarDocumento($item, $valor);
/* var_dump($respuestaDocumento); */
$documento_id = json_decode($respuestaDocumento["documento_id"], true);
$doc_dniremitente = json_decode($respuestaDocumento["doc_dniremitente"], true);
$empleado = substr($respuestaDocumento["empleado"], 0);
$doc_celularremitente = substr($respuestaDocumento["doc_celularremitente"], 0);
$doc_emailremitente = substr($respuestaDocumento["doc_emailremitente"], 0);
$doc_direccionremitente = substr($respuestaDocumento["doc_direccionremitente"], 0);
$doc_representacion = substr($respuestaDocumento["doc_representacion"], 0);
$tipodocumento_id = substr($respuestaDocumento["tipodocumento_id"], 0);
$tipodo_descripcion = substr($respuestaDocumento["tipodo_descripcion"], 0);
$doc_empresa = substr($respuestaDocumento["doc_empresa"], 0);
$doc_nrodocumento = substr($respuestaDocumento["doc_nrodocumento"], 0);
$doc_folio = substr($respuestaDocumento["doc_folio"], 0);
$doc_asunto = substr($respuestaDocumento["doc_asunto"], 0);
$doc_cuerpo = $respuestaDocumento["doc_cuerpo"];
$doc_archivo = substr($respuestaDocumento["doc_archivo"], 0);
$doc_fecharegistro = json_decode($respuestaDocumento["doc_fecharegistro"], true);
$area_id = json_decode($respuestaDocumento["area_id"], true);
$area_nombre = substr($respuestaDocumento["area_nombre"], 0);
$tipodo_descripcion = json_decode($respuestaDocumento["tipodo_descripcion"], true);
$doc_estatus = json_decode($respuestaDocumento["doc_estatus"], true);
$origen_nombre = substr($respuestaDocumento["origen_nombre"], 0);
$movimiento_id = json_decode($respuestaDocumento["movimiento_id"], true);
$tipodocumento_id = json_decode($respuestaDocumento["tipodocumento_id"], true);
$sigla = substr($respuestaDocumento["sigla"], 0);

$valor1 = $area_id;
$valor2 = $respuestaDocumento["documento_id"];

/* var_dump($valor1, $valor2); */
$usuario_destino = ModeloDocumento::mdlMostrarNombreEmisorReceptor($valor1, $valor2);
/* var_dump($usuario_destino); */
$datoscompletos_destino = ucwords(strtolower($usuario_destino["datos_completos"]));
$area_destino_doc = ucwords(strtolower($area_nombre));


$origennombre = ucwords(strtolower($origen_nombre));
$empladodestino = ucwords(strtolower($empleado));

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('CROIIILC');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


$PDF_MARGIN_LEFT = 30;
$PDF_MARGIN_TOP = 48;
$PDF_MARGIN_RIGHT = 30;

$PDF_MARGIN_HEADER = 100;
$PDF_MARGIN_FOOTER = 100;
// set marginsPDF_MARGIN_FOOTER

$pdf->SetMargins($PDF_MARGIN_LEFT, $PDF_MARGIN_TOP, $PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin($PDF_MARGIN_HEADER);
$pdf->SetFooterMargin($PDF_MARGIN_FOOTER);

// set auto page breaks
$PDF_MARGIN_BOTTOM = 45;
$pdf->SetAutoPageBreak(TRUE, $PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
//$pdf->SetFont('times', 'BI', 14);

// add a page
$pdf->AddPage();

date_default_timezone_set('America/Lima');

setlocale(LC_ALL, "es_ES@euro", "es_ES", "esp");
$dia_mes = strftime("%d de %B");
$a_o = strftime('%y');


if (empty($doc_folio)) {
    $referencia = '';
} else {

    $referencia = '<tr>
                     
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        
                            <td style="font-size:12px;text-align:left;width: 100px;font-family: Arial;">REFERENCIA:</td>
                            <td style="width:430px;font-size:12px;text-align:justify;font-family: Arial;">' . $doc_folio . '</td>

                    </tr>';
}

// set some text to print
$txt = <<<EOD
<html>
    <p style="margin:0px;font-size:14px;font-weight:bold;text-decoration:underline;text-align:center;font-family: Arial;">INFORME N°
    0$doc_nrodocumento-20$a_o-$sigla-CROIIILC</p>

            <table border="0" style="line-height: 12pt;">
            <tr>
            
                <td style="font-size:14px;text-align:left;width: 100px;font-family: Arial;">A:</td>
                <td style="width:430px;font-size:14px;text-align:justify;font-family: Arial;font-weight:none;"><b>Obstra. $datoscompletos_destino<br></b>$area_destino_doc del CRO III Lima - Callao</td>

            </tr>
            <tr>
                        
                <td></td>
                <td></td>

            </tr>  
            <tr>
            
                <td style="font-size:14px;text-align:left;width: 100px;font-family: Arial;">DE:</td>
                <td style="width:430px;font-size:14px;text-align:justify;font-family: Arial;"><b>Obstra. $empladodestino</b><br>$origennombre del CRO III Lima - Callao</td>

            </tr>
            <tr>
                    
                <td></td>
                <td></td>

            </tr>            
            <tr>
            
                <td style="font-size:12px;text-align:left;width: 100px;font-family: Arial;">ASUNTO:</td>
                <td style="width:430px;font-size:12px;text-align:justify;font-family: Arial;">$doc_asunto</td>

            </tr>
            $referencia
            <tr>
                    
                <td></td>
                <td></td>

            </tr>
            <tr>
            
            <td style="font-size:12px;text-align:left;width: 100px;font-family: Arial;">FECHA:</td>
            <td style="width:430px;font-size:12px;text-align:justify;font-family: Arial;">Pueblo Libre, $dia_mes del 20$a_o</td>

            </tr>
            <tr>
                <td colspan="2">
                <p style="line-height: 8pt;border-style: double; border-width: 1px;"></p>
                <p style="line-height: 12pt;font-family: Arial;font-size:12px;">$doc_cuerpo</p>
                </td>
            </tr>        

        </table>       
</html>
EOD;

// print a block of text using Write()
//$pdf->writeHTML($txt, true, false, true, false, '');
$pdf->writeHTML($txt);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('requerimiento.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
