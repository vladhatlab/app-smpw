<?php

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once "../../../controlador/documento.controlador.php";
require_once "../../../modelo/documento.modelo.php";

require_once('./tcpdf_include.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{
    //Page header
    public function Header()
    {
        // Logo
        /* $image_file = K_PATH_IMAGES . 'header.PNG'; */

        $image_file = K_PATH_IMAGES . 'oficio/header.PNG';
        $this->Image($image_file, 0, 0, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        //$this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        /*         $image_file = K_PATH_IMAGES . 'seccion_left.PNG';
        $this->Image($image_file, 0, 72, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false); */

        $image_file = K_PATH_IMAGES . 'oficio/seccion.PNG';
        $this->Image($image_file, 0, 55, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        //$this->SetY(-15);
        // Set font
        //$this->SetFont('helvetica', 'I', 8);
        // Page number
        //$this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');


        /*        $image_file = K_PATH_IMAGES . 'footer.PNG'; */
        $image_file = K_PATH_IMAGES . 'oficio/footer.PNG';
        /*    $this->Image($image_file, 0, 271, 0, 26, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false); */
        $this->Image($image_file, 0, 252, 0, 45, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}

if (isset($_GET["tipodoc"]) && isset($_GET["ndoc"])) {
    $item1 = 'tipodocumento_id';

    $valor1 = $_GET["tipodoc"];
    $valor2 = $_GET["ndoc"];


    $respuestaDocumento = ControladorDocumento::ctrMostrarDocumento_temp($item1, $valor1, $valor2);
    /*     var_dump($respuestaDocumento); */
    $documento_id = json_decode($respuestaDocumento["documento_id"], true);
    $doc_dniremitente = json_decode($respuestaDocumento["doc_dniremitente"], true);
    $empleado = substr($respuestaDocumento["empleado"], 0);
    $doc_celularremitente = substr($respuestaDocumento["doc_celularremitente"], 0);
    $doc_emailremitente = substr($respuestaDocumento["doc_emailremitente"], 0);
    $doc_direccionremitente = substr($respuestaDocumento["doc_direccionremitente"], 0);
    $doc_representacion = substr($respuestaDocumento["doc_representacion"], 0);
    $tipodocumento_id = substr($respuestaDocumento["tipodocumento_id"], 0);
    $tipodo_descripcion = substr($respuestaDocumento["tipodo_descripcion"], 0);
    $doc_empresa = substr($respuestaDocumento["doc_empresa"], 0);
    $doc_nrodocumento = substr($respuestaDocumento["doc_nrodocumento"], 0);
    $doc_folio = substr($respuestaDocumento["doc_folio"], 0);
    $doc_asunto = substr($respuestaDocumento["doc_asunto"], 0);
    $doc_cuerpo = $respuestaDocumento["doc_cuerpo"];
    $doc_archivo = substr($respuestaDocumento["doc_archivo"], 0);
    $doc_fecharegistro = json_decode($respuestaDocumento["doc_fecharegistro"], true);
    $area_id = json_decode($respuestaDocumento["area_id"], true);
    $area_nombre = json_decode($respuestaDocumento["area_nombre"], true);
    $tipodo_descripcion = json_decode($respuestaDocumento["tipodo_descripcion"], true);
    $doc_estatus = json_decode($respuestaDocumento["doc_estatus"], true);
    $origen_nombre = json_decode($respuestaDocumento["origen_nombre"], true);
    $movimiento_id = json_decode($respuestaDocumento["movimiento_id"], true);
    $tipodocumento_id = json_decode($respuestaDocumento["tipodocumento_id"], true);
    $institucion_cargo = json_decode($respuestaDocumento["institucion_funcionario"], true);
    $sigla = substr($respuestaDocumento["sigla"], 0);


    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Yosshi Condori Mendieta');
    $pdf->SetTitle('TCPDF Example 003');
    $pdf->SetSubject('TCPDF Tutorial');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


    $PDF_MARGIN_LEFT = 52;
    $PDF_MARGIN_TOP = 51;
    $PDF_MARGIN_RIGHT = 23;


    $PDF_MARGIN_HEADER = 100;
    $PDF_MARGIN_FOOTER = 100;
    // set marginsPDF_MARGIN_FOOTER

    $pdf->SetMargins($PDF_MARGIN_LEFT, $PDF_MARGIN_TOP, $PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin($PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin($PDF_MARGIN_FOOTER);

    // set auto page breaks
    $PDF_MARGIN_BOTTOM = 65;
    $pdf->SetAutoPageBreak(TRUE, $PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once(dirname(__FILE__) . '/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // set font
    //$pdf->SetFont('times', 'BI', 14);
    if (empty($doc_folio)) {
        $referencia = '';
    } else {

        $referencia = '<tr>
                        <td></td>
                        <td></td>
                        <td></td>

                   </tr>
                    <tr>
                            <td style="width:22mm;"></td>
                            <td style="font-size:12px;text-align:left;width: 100px;font-family: Arial;">REFERENCIA:</td>
                            <td style="width:320px;font-size:12px;text-align:justify;font-family: Arial;">' . $doc_folio . '</td>

                    </tr>';
    }
    $nombreInsti = "";
    foreach ($institucion_cargo as $key => $item) {

        $nombreInsti = $item["nombre"];
        $apellidoInsti = $item["apellido"];
        $cargo = $item["cargo"];
        $titulo = $item["titulo"];
        $institucion = $item["intitucion"];

        // add a page
        $pdf->AddPage();

        date_default_timezone_set('America/Lima');

        setlocale(LC_ALL, "es_ES@euro", "es_ES", "esp");
        $dia_mes = strftime("%d de %B");
        $a_o = strftime('%y');


        //var_dump($institucion_cargo);




        // set some text to print
        $txt = <<<EOD
<html>

<body>

    <div style="line-height: 8pt;">
            <table border="0">
                
                
            <tr>

                <td colspan="2"></td>

                <td style="font-size:14px;text-align:center;width: 240px;">
                    Pueblo Libre, $dia_mes del 20$a_o
                </td>
                <td></td>

            </tr>

        </table>

        <p style="font-size:14px;font-weight:bold;text-decoration:underline;text-align:left;">CARTA CIRCULAR N°
            0$doc_nrodocumento - CDR-20$a_o-$sigla-CROIIILC</p>
<br>
        <p style="font-size:12px;font-weight:bold;">$titulo</p>

        <p style="font-size:14px;font-weight:bold;">$nombreInsti $apellidoInsti </p>

        <p style="font-size:13px;font-weight:bold;">$cargo</p>

        <p style="font-size:12px;font-weight:bold;">$institucion</p>
<br>
        <p style="font-size:12px;text-decoration:underline;font-family: Arial;">Presente.-</p>
        <br>
        <br>
        <table border="0">
            <tr>
                <td style="width:22mm;"></td>    
                <td style="font-size:12px;text-align:left;width: 100px;font-family: Arial;">ASUNTO:</td>
                <td style="width:320px;font-size:12px;text-align:justify;font-family: Arial;">$doc_asunto</td>

            </tr>
            $referencia
        </table>

         
        <br>
        <p style="line-height: 8pt;font-family: Arial;font-size:12px;">De mi mayor consideración:</p>
        <p style="line-height: 15pt;font-family: Arial;font-size:12px;">$doc_cuerpo</p>
    </div>
</body>

</html>
EOD;

        // print a block of text using Write()
        $pdf->writeHTML($txt, true, false, true, false, '');
    }



    // ---------------------------------------------------------

    //Close and output PDF document
    $pdf->Output('cartacircular.pdf', 'I');

    //============================================================+
    // END OF FILE
    //============================================================+
}
