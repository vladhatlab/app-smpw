<?php

require_once "../../../controlador/documento.controlador.php";
require_once "../../../modelo/documento.modelo.php";

require_once('./tcpdf_include.php');

class imprimirDerivacion
{
    public $nDocumento;
    public function hojaRuta()
    {
        $nDoc = $this->nDocumento;
        $respuesta = ControladorDocumento::ctrMostrarDocumentosDerivados($nDoc);

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Yosshi Condori Mendieta');
        $pdf->SetTitle('TCPDF Example 003');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


        /*
        $PDF_MARGIN_LEFT = 50;
        $PDF_MARGIN_TOP = 50;
        $PDF_MARGIN_RIGHT = 30;
        */

        $PDF_MARGIN_LEFT = 52;
        $PDF_MARGIN_TOP = 51;
        $PDF_MARGIN_RIGHT = 23;



        $PDF_MARGIN_HEADER = 100;
        $PDF_MARGIN_FOOTER = 100;
        // set marginsPDF_MARGIN_FOOTER

        $pdf->SetMargins($PDF_MARGIN_LEFT, $PDF_MARGIN_TOP, $PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin($PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin($PDF_MARGIN_FOOTER);

        // set auto page breaks
        $PDF_MARGIN_BOTTOM = 65;
        $pdf->SetAutoPageBreak(TRUE, $PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        // add a page
        $pdf->AddPage();

        date_default_timezone_set('America/Lima');

        setlocale(LC_ALL, "es_ES@euro", "es_ES", "esp");
        $dia_mes = strftime("%d de %B");
        $a_o = strftime('%y');

        $html = '
        <html>
        <body>
        
        <h1 style="text-align:center;">Hoja de ruta</h1>
        <div class="content">
        <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
        <thead>
            <tr>
                <th style="width:50px;">#</th>
                <th>Procedencias</th>
                <th>Fecha</th>
                <th>Descripcion</th>
            </tr> 
        </thead>
        <tbody>';
        foreach ($respuesta as $key => $value) {
            $html .= '<tr>
                <td style="width:50px;">' . ($key + 1) . '</td>
                <td>' . $value['area_nombre'] . '</td>
                <td>' . $value['fecharegistro'] . '</td>
                <td>' . $value['mov_descripcion'] . '</td>
            </tr>';
        }
        $html .= ' </tbody></table></div></body></html>';
        $pdf->writeHTML($html, false, false, false, false, '');
        //------------------------------------------------------------------

        //------------------------------------------------------------------

        //******************************************************************
        //SALIDA DEL ARCHIVOS
        $pdf->Output('printObstetra.pdf');
        //------------------------------------------------------------------

        //------------------------------------------------------------------

        //******************************************************************
        //SALIDA DEL ARCHIVOS
        $pdf->Output('printObstetra.pdf');
    }
}
if (isset($_GET["ndocumento"])) {
    $imprimir = new imprimirDerivacion();

    $imprimir->nDocumento = $_GET["ndocumento"];
    $imprimir->hojaRuta();
} else {
    echo "NO EXISTE";
}