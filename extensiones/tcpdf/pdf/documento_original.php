<?php

require_once "../../../controlador/documento.controlador.php";
require_once "../../../modelo/documento.modelo.php";



class imprimirDocumento
{

    public $idDocumento;

    public function traerImpresionDocumento()
    {
        //TRAEMOS LA INFORMACION DEL TICKET
        $item = 'documento_id';
        $valorTicket = $this->idDocumento;




        $respuestaDocumento = ControladorDocumento::ctrMostrarDocumento($item, $valorTicket);

        /*var_dump($respuestaDocumento); */

        $documento_id = json_decode($respuestaDocumento["documento_id"], true);
        $doc_dniremitente = json_decode($respuestaDocumento["doc_dniremitente"], true);
        $empleado = substr($respuestaDocumento["empleado"], 0);
        $doc_celularremitente = substr($respuestaDocumento["doc_celularremitente"], 0);
        $doc_emailremitente = substr($respuestaDocumento["doc_emailremitente"], 0);
        $doc_direccionremitente = substr($respuestaDocumento["doc_direccionremitente"], 0);
        $doc_representacion = substr($respuestaDocumento["doc_representacion"], 0);
        $tipodocumento_id = substr($respuestaDocumento["tipodocumento_id"], 0);
        $tipodo_descripcion = substr($respuestaDocumento["tipodo_descripcion"], 0);
        $doc_empresa = substr($respuestaDocumento["doc_empresa"], 0);
        $doc_nrodocumento = substr($respuestaDocumento["doc_nrodocumento"], 0);
        $doc_folio = substr($respuestaDocumento["doc_folio"], 0);
        $doc_asunto = substr($respuestaDocumento["doc_asunto"], 0);
        $doc_cuerpo = $respuestaDocumento["doc_cuerpo"];
        $doc_archivo = substr($respuestaDocumento["doc_archivo"], 0);
        $doc_fecharegistro = json_decode($respuestaDocumento["doc_fecharegistro"], true);
        $area_id = json_decode($respuestaDocumento["area_id"], true);
        $area_nombre = json_decode($respuestaDocumento["area_nombre"], true);
        $tipodo_descripcion = json_decode($respuestaDocumento["tipodo_descripcion"], true);
        $doc_estatus = json_decode($respuestaDocumento["doc_estatus"], true);
        $origen_nombre = json_decode($respuestaDocumento["origen_nombre"], true);
        $movimiento_id = json_decode($respuestaDocumento["movimiento_id"], true);
        $tipodocumento_id = json_decode($respuestaDocumento["tipodocumento_id"], true);



        //---------------------------------------------------------------------------------------
        require_once('./tcpdf_include.php');

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->startPageGroup();
        $pdf->AddPage();
        $pdf->setPage($pdf->getPage());

        /*         $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false); */

        // set margins
/*                 $PDF_MARGIN_LEFT = 20;
        $PDF_MARGIN_TOP = 40;
        $PDF_MARGIN_RIGHT = 20;
        $pdf->SetMargins($PDF_MARGIN_LEFT, $PDF_MARGIN_TOP, $PDF_MARGIN_RIGHT); */
        $pdf->SetMargins(0, 0, 0, true);

/*         $PDF_MARGIN_HEADER = 5;
        $PDF_MARGIN_FOOTER = 10;
        $pdf->SetHeaderMargin($PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin($PDF_MARGIN_FOOTER); */



        // set auto page breaks false
        $PDF_MARGIN_BOTTOM = 0;
        $pdf->SetAutoPageBreak(true, $PDF_MARGIN_BOTTOM);

        // add a page
        /*         $pdf->AddPage('P', 'A4'); */

        $img_file = "images/formato_carta.png";
        $pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

        $pdf->setPageMark();


        $bloque1 = <<<EOF
        <table style="font-size:8px; padding:5px 10px;" border="0">
        
        <tr>
            <td>
            </td>
            <td style="font-size:10px;font-weight:bold;font-style:italic;text-align:center">
                "Año del Bicentenaria del Perú: 200 años de Independencia"
            </td>
            <td></td>
        </tr>
    
        <tr>
    
            <td>
            </td>
        </tr>
    
        <tr>
            <td style="width:120px;"></td>
            <td ></td>
            <td style="font-size:10px;">
                Lima, 2X de Dixxxxxxx del 202x
            </td>
        </tr>
    
        <tr>
            <td style="width: 120px; height:30px;">
            </td>
            <td style="font-size:11px;font-weight:bold;text-decoration:underline;">
                CARTA N° XXX-CDR-202X/CROIIILC
            </td>
        </tr>
    
    
        <tr>
            <td>
           
            </td>
            <td style="font-size:11px;font-weight:bold;"> 
                Obsta. /Dr. /Sr. /Sra.
            </td>
    
        </tr>
    
        <tr>
            <td>
        
            </td>
            <td colspan="2" style="font-size:11px;font-weight:bold;">
                NOMBRE:
            </td>
            <td></td>
        </tr>
    
        <tr>
            <td>
            
            </td>
            <td colspan="2" style="font-size:11px;font-weight:bold;">
                CARGO:
            </td>
            <td></td>
       
        </tr>
    
        <tr>
            <td>
                
            </td>
            <td colspan="2" style="font-size:10px;text-decoration:underline;">
                Presente.-
            </td>
            <td></td>
        </tr>

        <tr>
            <td style="width:50px"></td>
            <td style="font-size:10px;text-align:right;">Asunto:</td>
            <td style="width:250px;font-size:10px;text-align:justify;">
                $doc_asunto
                
            </td>
            <td></td>    
        </tr>
    
        <tr>
            <td style="width:120px;"></td>
            <td style="font-size:10px;">
                 De mi mayor consideración
                
  
                 
                 
            </td>

        </tr>
        
        <tr>
           
            <td style="width:120px;">
            
                 
            </td>
            <td colspan="2" style="width:400px;">
                $doc_cuerpo
            </td>
            <td>

            </td>

    
        </tr>

    
    </table>

EOF;
        //$pdf->writeHTML($bloque1, false, false, false, false, '');
        $pdf->writeHTML($bloque1, true, false, true, false,'');
        $pdf->lastPage();
        //------------------------------------------------------------------

        //------------------------------------------------------------------

        //******************************************************************
        //SALIDA DEL ARCHIVOS
        $pdf->Output('printTicket.pdf');
    }
}

$ticket = new imprimirDocumento();
$ticket->idDocumento = $_GET["idDocumento"];
$ticket->traerImpresionDocumento();
