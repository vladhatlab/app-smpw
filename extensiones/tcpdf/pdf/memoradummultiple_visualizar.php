<?php

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once "../../../controlador/documento.controlador.php";
require_once "../../../modelo/documento.modelo.php";

require_once('./tcpdf_include.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{
    //Page header
    public function Header()
    {
        // Logo
        $image_file = K_PATH_IMAGES . '/oficio/header.PNG';
        $this->Image($image_file, 0, 0, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        //$this->SetFont('helvetica', 'B', 20);
        // Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        /*         $image_file = K_PATH_IMAGES . '/oficio/seccion.PNG';
        $this->Image($image_file, 0, 65, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false); */
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        //$this->SetY(-15);
        // Set font
        //$this->SetFont('helvetica', 'I', 8);
        // Page number
        //$this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');


        $image_file = K_PATH_IMAGES . 'oficio/footer.PNG';
        $this->Image($image_file, 0, 252, 0, 45, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}

if (isset($_GET["tipodoc"]) && isset($_GET["ndoc"])) {
    $item1 = 'tipodocumento_id';

    $valor1 = $_GET["tipodoc"];
    $valor2 = $_GET["ndoc"];


    $respuestaDocumento = ControladorDocumento::ctrMostrarDocumento_temp($item1, $valor1, $valor2);
    /*     var_dump($respuestaDocumento); */
    $documento_id = json_decode($respuestaDocumento["documento_id"], true);
    $doc_dniremitente = json_decode($respuestaDocumento["doc_dniremitente"], true);
    $empleado = substr($respuestaDocumento["empleado"], 0);
    $doc_celularremitente = substr($respuestaDocumento["doc_celularremitente"], 0);
    $doc_emailremitente = substr($respuestaDocumento["doc_emailremitente"], 0);
    $doc_direccionremitente = substr($respuestaDocumento["doc_direccionremitente"], 0);
    $doc_representacion = substr($respuestaDocumento["doc_representacion"], 0);
    $tipodocumento_id = substr($respuestaDocumento["tipodocumento_id"], 0);
    $tipodo_descripcion = substr($respuestaDocumento["tipodo_descripcion"], 0);
    $doc_empresa = substr($respuestaDocumento["doc_empresa"], 0);
    $doc_nrodocumento = substr($respuestaDocumento["doc_nrodocumento"], 0);
    $doc_folio = substr($respuestaDocumento["doc_folio"], 0);
    $doc_asunto = substr($respuestaDocumento["doc_asunto"], 0);
    $doc_cuerpo = $respuestaDocumento["doc_cuerpo"];
    $doc_archivo = substr($respuestaDocumento["doc_archivo"], 0);
    $doc_fecharegistro = json_decode($respuestaDocumento["doc_fecharegistro"], true);
    $area_id = json_decode($respuestaDocumento["area_id"], true);
    $area_nombre = json_decode($respuestaDocumento["area_nombre"], true);
    $tipodo_descripcion = json_decode($respuestaDocumento["tipodo_descripcion"], true);
    $doc_estatus = json_decode($respuestaDocumento["doc_estatus"], true);
    $origen_nombre = json_decode($respuestaDocumento["origen_nombre"], true);
    $movimiento_id = json_decode($respuestaDocumento["movimiento_id"], true);
    $tipodocumento_id = json_decode($respuestaDocumento["tipodocumento_id"], true);
    $institucion_cargo = json_decode($respuestaDocumento["institucion_funcionario"], true);
    $sigla = substr($respuestaDocumento["sigla"], 0);


    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Nicola Asuni');
    $pdf->SetTitle('CROIIILC');
    $pdf->SetSubject('TCPDF Tutorial');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


    $PDF_MARGIN_LEFT = 5;
    $PDF_MARGIN_TOP = 50;
    $PDF_MARGIN_RIGHT = 10;

    $PDF_MARGIN_HEADER = 100;
    $PDF_MARGIN_FOOTER = 100;
    // set marginsPDF_MARGIN_FOOTER

    $pdf->SetMargins($PDF_MARGIN_LEFT, $PDF_MARGIN_TOP, $PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin($PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin($PDF_MARGIN_FOOTER);

    // set auto page breaks
    $PDF_MARGIN_BOTTOM = 55;
    $pdf->SetAutoPageBreak(TRUE, $PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once(dirname(__FILE__) . '/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // set font
    //$pdf->SetFont('times', 'BI', 14);
    $nombreInsti = "";
    foreach ($institucion_cargo as $key => $item) {

        $nombreInsti = $item["nombre"];
        $apellidoInsti = $item["apellido"];
        $cargo = $item["cargo"];
        $titulo = $item["titulo"];
        $institucion = $item["intitucion"];


        // add a page
        $pdf->AddPage();

        date_default_timezone_set('America/Lima');

        setlocale(LC_ALL, "es_ES@euro", "es_ES", "esp");
        $dia_mes = strftime("%d de %B");
        $a_o = strftime('%y');



        // set some text to print
        $txt = <<<EOD
<html>

<table border="0">

    <tr>
        <td style="width: 75px; height:30px;"></td>
        <td colspan="2"></td>
        <td></td>
        <td style="font-size:14px;text-align:center;width: 270px;">
            Lima, $dia_mes del 20$a_o
        </td>
        <td></td>

    </tr>

    <tr>
        <td style="width: 75px; height:30px;"></td>
        <td colspan="3" style="font-size:14px;font-weight:bold;text-decoration:underline;">
            MEMORANDUM MULTIPLE N° 0$doc_nrodocumento - CDR-20$a_o-$sigla-CROIIILC
        </td>
        <td></td>
        <td style="width: 150px; height:30px;"></td>
        <td></td>
    </tr>


    <tr>
        <td style="width: 75px; height:30px;"></td>
        <td colspan="3" style="font-size:14px;font-weight:bold;">
            Obsta. /Dr. /Sr. /Sra.
        </td>
        <td></td>
        <td style="width: 150px; height:30px;"></td>
        <td></td>
    </tr>

    <tr>
        <td style="width: 75px; height:30px;"></td>
        <td colspan="3" style="font-size:14px;font-weight:bold;">
            NOMBRE:
        </td>
        <td></td>
        <td style="width: 150px; height:30px;"></td>
        <td></td>
    </tr>

    <tr>
        <td style="width: 75px; height:30px;"></td>
        <td colspan="3" style="font-size:14px;font-weight:bold;">
            CARGO:
        </td>
        <td></td>
        <td></td>
        <td></td>

    </tr>

    <tr>
        <td style="width: 75px; height:30px;"></td>
        <td colspan="3" style="font-size:14px;text-decoration:underline;">
            Presente.-
        </td>
        <td></td>
        <td></td>
        <td></td>

    </tr>
</table>

<table border="0" >

    <tr>
        <td style="width:120px;height: 60px;"></td>
        <td style="font-size:14px;text-align:left;width: 80px;">Asunto:</td>
        <td style="width:420px;font-size:12px;text-align:justify;" colspan="2">$doc_asunto</td>
        <td></td>
    </tr>

    <tr>
        <td style="width:120px;"></td>
        <td style="font-size:14px;text-align:left;" colspan="3">De mi mayor consideración</td>
        <td style="width: 120PX;"></td>
    </tr>

    <tr >

        <td style="width:120px;"></td>
        <td colspan="3" style="font-family:Arial;font-size:14px;">
            <body>
            $doc_cuerpo
            </body>
        </td>
    
    </tr>
</table>
</html>
EOD;

        // print a block of text using Write()
        //$pdf->writeHTML($txt, true, false, true, false, '');
        $pdf->writeHTML($txt);
    }



    // ---------------------------------------------------------

    //Close and output PDF document
    $pdf->Output('example_003.pdf', 'I');

    //============================================================+
    // END OF FILE
    //============================================================+
}
