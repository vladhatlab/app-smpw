<?php

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once "../../../controlador/documento.controlador.php";
require_once "../../../modelo/documento.modelo.php";

require_once('./tcpdf_include.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{
    //Page header
    public function Header()
    {
        // Logo
        /* $image_file = K_PATH_IMAGES . 'header.PNG'; */
        /* 
        $image_file = 'PRUEBA HEADER';
        $this->Image($image_file, 0, 0, 0, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false); */
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        //$this->SetY(-15);
        // Set font
        //$this->SetFont('helvetica', 'I', 8);
        // Page number
        //$this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        /*        $image_file = K_PATH_IMAGES . 'footer.PNG'; */
        /*         $image_file = 'PRUEBA FOOTER'; */
        /*    $this->Image($image_file, 0, 271, 0, 26, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false); */
        /*         $this->Image($image_file, 0, 252, 0, 45, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false); */
    }
}

if (isset($_GET["ndocumento"])) {
    $nDoc = $_GET["ndocumento"];
} else {
    echo "NO EXISTE";
}

$respuesta = ControladorDocumento::ctrMostrarDocumentosDerivados($nDoc);
$expediente = ControladorDocumento::ctrMostrarExpedienteDerivado($nDoc);

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Yosshi Condori Mendieta');
$pdf->SetTitle('TCPDF Example 003');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);


/*
$PDF_MARGIN_LEFT = 50;
$PDF_MARGIN_TOP = 50;
$PDF_MARGIN_RIGHT = 30;
*/

$PDF_MARGIN_LEFT = 30;
$PDF_MARGIN_TOP = 10;
$PDF_MARGIN_RIGHT = 20;



$PDF_MARGIN_HEADER = 100;
$PDF_MARGIN_FOOTER = 100;
// set marginsPDF_MARGIN_FOOTER

$pdf->SetMargins($PDF_MARGIN_LEFT, $PDF_MARGIN_TOP, $PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin($PDF_MARGIN_HEADER);
$pdf->SetFooterMargin($PDF_MARGIN_FOOTER);

// set auto page breaks
$PDF_MARGIN_BOTTOM = 30;
$pdf->SetAutoPageBreak(TRUE, $PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------


// add a page
$pdf->AddPage();

/*
date_default_timezone_set('America/Lima');
setlocale(LC_ALL, "es_ES@euro", "es_ES", "esp");
$dia_mes = strftime("%d de %B");
$a_o = strftime('%y'); 
*/
date_default_timezone_set('America/Bogota');

$fecha = date('Y-m-d');
$hora = date('H:i:s');

$fechaActual = $fecha . ' ' . $hora;

$html = '
<html>
<body>
<h1 style="text-align:center;">COLEGIO DE OBSTETRAS III-LIMA CALLAO</h1>
<h3 style="text-align:center;">HOJA DE DERIVACION</h3>
<h5 style="text-align:center;">TRAMITE DOCUMENTARIO</h5>
<hr>
<div>
<table style="font-size:12px;">
    <tbody>
        <tr>
            <td style="font-weight: bold;">Tipo Documento:</td>
            <td>' . $expediente[0]["tipodo_descripcion"] . '</td>
            <td style="font-weight: bold;">N° Expediente:</td>
            <td>' . $expediente[0]["documento_id"] . '</td>
        </tr>

        <tr>
            <td style="font-weight: bold;">N° Documento:</td>
            <td>' . $expediente[0]["doc_nrodocumento"] . '</td>
            <td style="font-weight: bold;">Fecha de Registro</td>
            <td>' . $expediente[0]["doc_fecharegistro"] . '</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="font-weight: bold;">Fecha del Sistema:</td>
            <td>' . $fechaActual . '</td>
        </tr>
        <br>
        <tr>
            <td style="font-weight: bold;">Intresado:</td>
            <td colspan="3">' . $expediente[0]["origen_nombre"] . '</td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Asunto:</td>
            <td colspan="3">' . $expediente[0]["doc_asunto"] . '</td>

        </tr>
    </tbody>
</table>
<hr>
</div>
<div>
<table  border="1" style="text-align:center;">
<thead>
    <tr style="font-size:12px;font-weight: bold;">
        <th style="width:50px;">#</th>
        <th style="width:190px;">PROCEDENCIA</th>
        <th>FECHA</th>
        <th colspan="2">DESCRIPCION</th>
        
    </tr> 
</thead>
<tbody>';

foreach ($respuesta as $key => $value) {

    $html .= '<tr style="font-size:10px;line-height:25px;">
        <td style="width:50px;height: 30px;">' . ($key + 1) . '</td>
        <td style="width:190px;">' . $value['area_nombre'] . '</td>
        <td>' . $value['fecharegistro'] . '</td>
        <td colspan="2">' . $value['mov_descripcion_original'] . '</td>
    </tr>';
}
$html .= ' </tbody></table></div></body></html>';;



$pdf->writeHTML($html, true, false, true, false, '');
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('carta.pdf', 'I');