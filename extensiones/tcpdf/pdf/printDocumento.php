<?php

require_once "../../../controlador/documento.controlador.php";
require_once "../../../modelo/documento.modelo.php";

class imprimirDocumento
{

    public $idDocumento;

    public function traerImpresionDocumento()
    {
        //TRAEMOS LA INFORMACION DEL TICKET
        $item = 'documento_id';
        $valorTicket = $this->idDocumento;




        $respuestaDocumento = ControladorDocumento::ctrMostrarDocumento($item, $valorTicket);

        /*         var_dump($respuestaDocumento); */

        $documento_id = json_decode($respuestaDocumento["documento_id"], true);
        $doc_dniremitente = json_decode($respuestaDocumento["doc_dniremitente"], true);
        $empleado = substr($respuestaDocumento["empleado"], 0);
        $doc_celularremitente = substr($respuestaDocumento["doc_celularremitente"], 0);
        $doc_emailremitente = substr($respuestaDocumento["doc_emailremitente"], 0);
        $doc_direccionremitente = substr($respuestaDocumento["doc_direccionremitente"], 0);
        $doc_representacion = substr($respuestaDocumento["doc_representacion"], 0);
        $tipodocumento_id = substr($respuestaDocumento["tipodocumento_id"], 0);
        $tipodo_descripcion = substr($respuestaDocumento["tipodo_descripcion"], 0);
        $doc_empresa = substr($respuestaDocumento["doc_empresa"], 0);
        $doc_nrodocumento = substr($respuestaDocumento["doc_nrodocumento"], 0);
        $doc_folio = substr($respuestaDocumento["doc_folio"], 0);
        $doc_asunto = json_decode($respuestaDocumento["doc_asunto"], true);
        $doc_archivo = substr($respuestaDocumento["doc_archivo"], 0);
        $doc_fecharegistro = json_decode($respuestaDocumento["doc_fecharegistro"], true);
        $area_id = json_decode($respuestaDocumento["area_id"], true);
        $area_nombre = json_decode($respuestaDocumento["area_nombre"], true);
        $tipodo_descripcion = json_decode($respuestaDocumento["tipodo_descripcion"], true);
        $doc_estatus = json_decode($respuestaDocumento["doc_estatus"], true);
        $origen_nombre = json_decode($respuestaDocumento["origen_nombre"], true);
        $movimiento_id = json_decode($respuestaDocumento["movimiento_id"], true);
        $tipodocumento_id = json_decode($respuestaDocumento["tipodocumento_id"], true);





        //TREAR NOMBRE DE CATEGORIA 
        /*         $item_catg = "id";
        $valor_catg = $categoria;
        $respuestaCatg = ControladorCategorias::ctrMostrarCategorias($item_catg, $valor_catg);

        $categoria_nombre = substr($respuestaCatg["categoria"], 0); */
        //---------------------------------------------------------------------------------------
        //TREAR NOMBRE DE ESTADO
        /*         $item_estado = "id";
        $valor_estado = $id_estado;
        $respuestaEstado = ControladorEstado::ctrMostrarEstado($item_estado, $valor_estado);

        $estado_nombre = substr($respuestaEstado["estado"], 0); */
        //---------------------------------------------------------------------------------------
        //TREAR NOMBRE DE ESTADO
        /*         $item_soporte = "soporte";
        $valor_soporte = $soporte;
        $respuestaSoporte = ControladorSoporte::ctrMostrarSoporte($item_soporte, $valor_soporte);

        $soporte_nombre = substr($respuestaSoporte["soporte"], 0); */
        //---------------------------------------------------------------------------------------
        require_once('./tcpdf_include.php');

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->startPageGroup();
        $pdf->AddPage();

        $bloque1 = <<<EOF
       
	<table>
		
		<tr>
            <br>
            <td style="width:10px"></td>
			<td style="width:80px"><img src="images/logo_obstetra.png"></td>

			<td style="background-color:white; width:350x">
				
				<div style="font-size:12px; text-align:right; line-height:15px;">
                COLEGIO REGIONAL DE OBSTETRAS III LIMA - CALLAO
                </div>
                <div style="font-size:10px; text-align:right; line-height:15px;">
                 SOLICITO: .......................................................
				</div>

			</td>
	

		</tr>

	</table>
        
        
        
EOF;
        $pdf->writeHTML($bloque1, false, false, false, false, '');
        //------------------------------------------------------------------
        $bloque2 = <<<EOF
<br>
<h1>Sra. Decana del Consejo Reguional III Lima - Callao</h1>       
<h1 style="text-align:center;">ETF-TECNOLOGÍA DE LA INFORMACIÓN</h1>  
   <br>
   <table style="font-size:10px; padding:5px 10px;">
                 <tr>
                    <td style="border: 1px solid #666; background-color:white; width: 270px">
                        Fecha y Hora: 
                    </td>
                    <td style="border: 1px solid #666; background-color:white; width: 270px; text-align:left">
                        Estado del Ticket: 
                    </td>
                </tr>
                
                
                <tr>
                    <td style="border: 1px solid #666; background-color:white; width: 270px">
                        Categoria:  
                        
                    </td>
                    <td style="border: 1px solid #666; background-color:white; width: 270px; text-align:left">
                        Codigo: 
                        
                    </td>
                </tr>
                 <br>             
                <tr>
                
                    <td colspan=2>
                        <strong>Datos del Usuario: </strong>
                        
                    </td>
                </tr>
                
                <tr>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 100px;text-align:center">
                        Nombre:   
                        
                    </td>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 440px;text-align:center">
                     
                        
                    </td>

                </tr>
                <tr>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 100px;text-align:center">
                        Oficina:   
                        
                    </td>   
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 440px;text-align:center">
                 
                        
                    </td>

                </tr>
                <tr>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 100px;text-align:center">
                        Area:   
                        
                    </td>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 440px;text-align:center">
               
                        
                    </td>

                </tr>
                <tr>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 100px;text-align:center">
                        Cargo:   
                        
                    </td>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 440px;text-align:center">
              
                        
                    </td>

                </tr>
                <tr>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 100px;text-align:center">
                        Cel:   
                        
                    </td>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 440px;text-align:center">
                    
                        
                    </td>

                </tr>
                <tr>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 100px;text-align:center">
                        Sede:
                        
                    </td>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 440px;text-align:center">
        
                        
                    </td>

                </tr>
                <tr>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 100px;text-align:center">
                        Piso:   
                        
                    </td>
                    <td style="font-size:12px;border: 1px solid #666; background-color:white; width: 440px;text-align:center">

                        
                    </td>

                </tr>
                 <br>
                
</table>
        
        
EOF;
        $pdf->writeHTML($bloque2, false, false, false, false, '');
        //-------------------------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------
        $bloque3 = <<<EOF
<hr>
<br>
<br>
   <table style="font-size:10px; padding:5px 10px;">
                <br>
                 <tr>
                
                    <td colspan=2>
                        <strong>Descripción: </strong>
                        
                    </td>
                </tr>
                 <tr>
                    <td style="border: 1px solid #666; background-color:white; width: 540px">

                        
                    </td>
                </tr>            
                                <br>
                 <tr>
                
                    <td colspan=2>
                        <strong>Obsercación: </strong>
                        
                    </td>
                </tr>
                 <tr>
                    <td style="border: 1px solid #666; background-color:white; width: 540px">

                        
                    </td>
                </tr> 
</table>
        
        
EOF;
        $pdf->writeHTML($bloque3, false, false, false, false, '');
        //------------------------------------------------------------------
        $bloque4 = <<<EOF
<hr>
<br>
<br>
<br>
<br>
<br>
<br>
   <table style="font-size:10px; padding:5px 10px;">
                <br>
                 <tr>
                    <td style="width: 270px;text-align:center">
                        --------------------------------------  
                    </td>
                    <td style="width: 270px;text-align:center">
                        --------------------------------------  
                    </td>
                </tr>
                <tr>
                    <td style="width: 270px;text-align:center">
                        <br>ETF-Tecnología de la Información
                    </td>
                    <td style="width: 270px;text-align:center">
                    
                    </td>
                </tr> 
 
</table>
        
        
EOF;
        $pdf->writeHTML($bloque4, false, false, false, false, '');
        //------------------------------------------------------------------

        //******************************************************************
        //SALIDA DEL ARCHIVOS
        $pdf->Output('printTicket.pdf');
    }
}

$ticket = new imprimirDocumento();
$ticket->idDocumento = $_GET["idDocumento"];
$ticket->traerImpresionDocumento();
