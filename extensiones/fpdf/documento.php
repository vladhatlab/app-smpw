<?php

require_once "../../controlador/documento.controlador.php";
require_once "../../modelo/documento.modelo.php";
require_once('./fpdf.php');
require_once('./fpdfHtml.php');

class ImprimirDocumento extends FPDF
{

    /*****************************************/

    var $B = 0;
    var $I = 0;
    var $U = 0;
    var $HREF = '';
    var $ALIGN = '';

    function WriteHTML($html)
    {
        //HTML parser
        $html = str_replace("\n", ' ', $html);
        $a = preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($a as $i => $e) {
            if ($i % 2 == 0) {
                //Text
                if ($this->HREF)
                    $this->PutLink($this->HREF, $e);
                elseif ($this->ALIGN == 'center')
                    $this->Cell(0, 5, $e, 0, 1, 'C');
                else
                    $this->Write(5, $e);
            } else {
                //Tag
                if ($e[0] == '/')
                    $this->CloseTag(strtoupper(substr($e, 1)));
                else {
                    //Extract properties
                    $a2 = explode(' ', $e);
                    $tag = strtoupper(array_shift($a2));
                    $prop = array();
                    foreach ($a2 as $v) {
                        if (preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3))
                            $prop[strtoupper($a3[1])] = $a3[2];
                    }
                    $this->OpenTag($tag, $prop);
                }
            }
        }
    }

    function OpenTag($tag, $prop)
    {
        //Opening tag
        if ($tag == 'B' || $tag == 'I' || $tag == 'U')
            $this->SetStyle($tag, true);
        if ($tag == 'A')
            $this->HREF = $prop['HREF'];
        if ($tag == 'BR')
            $this->Ln(5);
        if ($tag == 'P')
            $this->ALIGN = $prop['ALIGN'];
        if ($tag == 'HR') {
            if (!empty($prop['WIDTH']))
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin - $this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x, $y, $x + $Width, $y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag)
    {
        //Closing tag
        if ($tag == 'B' || $tag == 'I' || $tag == 'U')
            $this->SetStyle($tag, false);
        if ($tag == 'A')
            $this->HREF = '';
        if ($tag == 'P')
            $this->ALIGN = '';
    }

    function SetStyle($tag, $enable)
    {
        //Modify style and select corresponding font
        $this->$tag += ($enable ? 1 : -1);
        $style = '';
        foreach (array('B', 'I', 'U') as $s)
            if ($this->$s > 0)
                $style .= $s;
        $this->SetFont('', $style);
    }

    function PutLink($URL, $txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0, 0, 255);
        $this->SetStyle('U', true);
        $this->Write(5, $txt, $URL);
        $this->SetStyle('U', false);
        $this->SetTextColor(0);
    }


    /*****************************************/
    public $idDocumento;

    public function header()
    {
        //DARLE COLOR
        //$this->SetFillColor(253,135,39);
        //CREAR UN RECTANGULO
        //$this->Rect(0,0,220,50,'F');
        $this->SetFont('Arial', 'B', 30);
        $this->SetTextColor(255, 255, 255);
        $this->SetY(20);
        $this->Write(5, 'The Miau');
        $this->Image('images/header.PNG', 7, 0, 0, 0, 'png');
    }
    public function footer()
    {
        //DARLE COLOR
        //$this->SetFillColor(127, 34, 27);
        //CREAR UN RECTANGULO
        //$this->Rect(0, 250, 220, 50, 'F');

        //$this->SetFont('Arial', 'B', 11);
        //$this->SetTextColor(255, 255, 255);

        // $this->SetFillColor(155, 124, 46);
        //$this->Rect(0, 250, 220, 3, 'F');
        //$this->SetY(-10);
        //this->SetX(120);
        //$this->Write(5,'Av. Marical Sucre N° 1351');    
        $this->Image('images/footer.PNG', 0, 253, 0, 28, 'png');
    }
}


$item = 'documendo_id';
$valor = $_GET["idDocumento"];
$rptaDoc = ControladorDocumento::ctrMostrarDocumento($item, $valor);



$fpdf = new ImprimirDocumento('P', 'mm', 'letter', true);
$fpdf->AddPage('portrait', 'letter');
$fpdf->SetMargins(70, 100, 10);
$fpdf->SetFont('Arial', 'B', 11);
$fpdf->WriteHTML($rptaDoc["doc_cuerpo"]);
$fpdf->Output();
//$fpdf->writeHTML($rptaDoc["doc_cuerpo"], true, false, true, false,'');



//$fpdf->Output();
